package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visualgov.baytaxCollector.R;

import java.util.ArrayList;

import Model.InstallmentList;
import utils.Config;

/**
 * Created by SaraschandraaM on 29/08/15.
 */
public class InstallmentListAdapter extends RecyclerView.Adapter<InstallmentListAdapter.InstallmentListHolder> {

    Context context;
    ArrayList<InstallmentList> values;
    InstallmentClickListener installmentClickListener;

    public InstallmentListAdapter(Context context, ArrayList<InstallmentList> values){
        this.context = context;
        this.values = values;
    }

    public void setInstallmentClickListener(InstallmentClickListener installmentClickListener){
        this.installmentClickListener = installmentClickListener;
    }


    @Override
    public InstallmentListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_installment, parent, false);
        InstallmentListHolder holder = new InstallmentListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(InstallmentListHolder holder, int position) {
        InstallmentList data = values.get(position);
        if(data.getInstallmentNumber()!=null) {
            holder.installmentno.setText(data.getInstallmentNumber());
        }
        if(data.getInstallAmt() != null) {
            holder.instAmount.setText("$" + data.getInstallAmt());
        }
        if(data.getDueLiteral1() != null) {
            holder.instDueDate.setText(data.getDueLiteral1());
        }
        if(data.getTOTALSCollections() != null) {
            holder.instPaid.setText("$" + data.getTOTALSCollections());
        }
        String value = data.getLastPmDate();
        if(value !=null) {
            String[] date = value.split("\\ ");
            holder.instLastPaid.setText(date[0]);
        }
        if(data.getReceiptNo() != null && !data.getReceiptNo().equals("null")) {
            holder.instReceipt.setText(data.getReceiptNo());
            holder.payInstallment.setVisibility(View.INVISIBLE);
        }else  if(value == null && Config.isInstalmentPayment && (Config.isCreditCardAllowed || Config.isECheckAllowed)){
            holder.payInstallment.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class InstallmentListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        TextView installmentno, instDueDate, instAmount, instReceipt, instPaid, instLastPaid;
        LinearLayout payInstallment;

        public InstallmentListHolder(View itemView) {
            super(itemView);
            installmentno = (TextView) itemView.findViewById(R.id.tv_install_no);
            instDueDate = (TextView) itemView.findViewById(R.id.tv_inst_date);
            instAmount = (TextView) itemView.findViewById(R.id.tv_inst_amt);
            instReceipt = (TextView) itemView.findViewById(R.id.tv_inst_receipt);
            instPaid = (TextView) itemView.findViewById(R.id.tv_inst_paid);
            instLastPaid = (TextView) itemView.findViewById(R.id.tv_inst_lastpaid);
            payInstallment = (LinearLayout) itemView.findViewById(R.id.ll_payinstalment);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(installmentClickListener != null && payInstallment.getVisibility() == View.VISIBLE){
                installmentClickListener.onInstallmentClicked(v, getPosition());
            }
        }
    }


    public interface InstallmentClickListener{
        public void onInstallmentClicked(View view, int position);
    }
}