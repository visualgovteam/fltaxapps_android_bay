package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.DueList;
import utils.Config;

/**
 * Created by SaraschandraaM on 23/08/15.
 */
public class DueListAdapter extends RecyclerView.Adapter<DueListAdapter.DueListHolder> {

    Context context;
    ArrayList<DueList> values;

    public DueListAdapter(Context context, ArrayList<DueList> values){
        this.context = context;
        this.values = values;
    }


    @Override
    public DueListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_nonadvol, parent, false);
        DueListHolder holder = new DueListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DueListHolder holder, int position) {
        DueList data = values.get(position);
        holder.mTvDesc.setText(data.getDueDate());
        String DueAmount = data.getDueAmount();
        DueAmount = Config.formatCurrency(DueAmount);
        holder.mTvAmt.setText(DueAmount);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    class DueListHolder extends RecyclerView.ViewHolder{

        TextView mTvDesc, mTvAmt;
        public DueListHolder(View itemView) {
            super(itemView);
            mTvDesc = (TextView) itemView.findViewById(R.id.tv_rc_desc);
            mTvAmt = (TextView) itemView.findViewById(R.id.tv_rc_amt);
        }
    }
}
