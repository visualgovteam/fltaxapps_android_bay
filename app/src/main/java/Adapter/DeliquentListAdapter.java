package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.DelinquentList;
import utils.Config;

/**
 * Created by SaraschandraaM on 24/08/15.
 */
public class DeliquentListAdapter extends RecyclerView.Adapter<DeliquentListAdapter.DeliquentListHolder> {

    Context context;
    ArrayList<DelinquentList> values;
    OnDeliquentClickListener onDeliquentClickListener;

    public DeliquentListAdapter(Context context, ArrayList<DelinquentList> values){
        this.context = context;
        this.values = values;
    }

    public void setOnDeliquentClickListener(OnDeliquentClickListener onDeliquentClickListener){
        this.onDeliquentClickListener = onDeliquentClickListener;
    }

    @Override
    public DeliquentListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_delinquentlist, parent, false);
        DeliquentListHolder holder = new DeliquentListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(DeliquentListHolder holder, int position) {
        DelinquentList data = values.get(position);
        holder.mTvTaxBill.setText(data.getTaxYear()+"/"+data.getTaxRoll()+"/"+data.getTaxBill());
        String amt = data.getTaxTotal();
        amt = Config.formatCurrency(amt);
        holder.mTvTotalDue.setText(amt);
        String outstanding = data.getTaxOutstanding();
        outstanding = Config.formatCurrency(outstanding);
        holder.mTvOutstandingTax.setText(outstanding);
        String penal = data.getTaxPenalties();
        penal = Config.formatCurrency(penal);
        holder.mTvPenal.setText(penal);
        holder.mTvReference.setText(data.getTaxReference());

        if(Config.isDeliquentPayment){
            holder.mDelPaymentLayout.setVisibility(View.VISIBLE);
        }else{
            holder.mDelPaymentLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    class DeliquentListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView mTvTaxBill, mTvOutstandingTax, mTvPenal, mTvTotalDue, mTvReference;
        LinearLayout mDelPaymentLayout;
        public DeliquentListHolder(View itemView) {
            super(itemView);
            mTvTaxBill = (TextView) itemView.findViewById(R.id.tv_taxbill_year_roll);
            mTvOutstandingTax = (TextView) itemView.findViewById(R.id.tv_taxOutstanding);
            mTvPenal = (TextView) itemView.findViewById(R.id.tv_taxpenal);
            mTvTotalDue = (TextView) itemView.findViewById(R.id.tv_totalDue);
            mTvReference = (TextView) itemView.findViewById(R.id.tv_reference);
            mDelPaymentLayout = (LinearLayout) itemView.findViewById(R.id.ll_delpayinstalment);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onDeliquentClickListener != null){
                onDeliquentClickListener.onDeliquentListClicked(v, getPosition());
            }
        }
    }

    public interface OnDeliquentClickListener{
        public void onDeliquentListClicked(View view, int position);
    }
}

