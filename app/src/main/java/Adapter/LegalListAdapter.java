package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.baytaxCollector.R;

import java.util.ArrayList;

/**
 * Created by SaraschandraaM on 23/08/15.
 */
public class LegalListAdapter extends RecyclerView.Adapter<LegalListAdapter.LegalListHolder> {

    Context context;
    ArrayList<String> values;

    public LegalListAdapter(Context context, ArrayList<String> values){
        this.context = context;
        this.values = values;
    }


    @Override
    public LegalListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_legallist, parent, false);
        LegalListHolder holder = new LegalListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LegalListHolder holder, int position) {
        String data = values.get(position);
        holder.mDesc.setText(data);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class LegalListHolder extends RecyclerView.ViewHolder{

        TextView mDesc;

        public LegalListHolder(View itemView) {
            super(itemView);
            mDesc = (TextView) itemView.findViewById(R.id.tv_legdesc);
        }
    }
}
