package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.baytaxCollector.R;

import java.util.ArrayList;

import Model.Values;

/**
 * Created by SaraschandraaM on 24/08/15.
 */
public class ValuesListAdapter extends RecyclerView.Adapter<ValuesListAdapter.ValuesListHolder> {

    Context context;
    ArrayList<Values> values;

    public ValuesListAdapter(Context context, ArrayList<Values> values){
        this.context = context;
        this.values = values;
    }


    @Override
    public ValuesListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_nonadvol, parent, false);
        ValuesListHolder holder = new ValuesListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ValuesListHolder holder, int position) {
        Values data = values.get(position);
        holder.mTvDesc.setText(data.getKey());
        holder.mTvAmt.setText(data.getData());
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    class ValuesListHolder extends RecyclerView.ViewHolder{

        TextView mTvDesc, mTvAmt;
        public ValuesListHolder(View itemView) {
            super(itemView);
            mTvDesc = (TextView) itemView.findViewById(R.id.tv_rc_desc);
            mTvAmt = (TextView) itemView.findViewById(R.id.tv_rc_amt);
        }
    }
}