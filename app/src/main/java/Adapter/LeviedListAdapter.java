package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.LeviedList;
import utils.Config;

/**
 * Created by SaraschandraaM on 23/08/15.
 */
public class LeviedListAdapter extends RecyclerView.Adapter<LeviedListAdapter.LeviedListHolder> {

    Context context;
    ArrayList<LeviedList> values;

    public LeviedListAdapter(Context context, ArrayList<LeviedList> values){
        this.context = context;
        this.values = values;
    }

    @Override
    public LeviedListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_advolarem, parent, false);
        LeviedListHolder holder = new LeviedListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LeviedListHolder holder, int position) {
        LeviedList data = values.get(position);
        holder.mTaxAuth.setText(data.getLeviedDescription());
        holder.mMileage.setText(data.getLeviedAdjustment());
        String amt = data.getLeviedOriginal();
        amt = Config.formatCurrency(amt);
        holder.mTaxAmt.setText(amt);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class LeviedListHolder extends RecyclerView.ViewHolder{

        TextView mTaxAuth, mMileage, mTaxAmt;
        public LeviedListHolder(View itemView) {
            super(itemView);
            mTaxAuth = (TextView) itemView.findViewById(R.id.tv_rc_taxingAuthority);
            mMileage = (TextView) itemView.findViewById(R.id.tv_rc_MilageRate);
            mTaxAmt = (TextView) itemView.findViewById(R.id.tv_rc_taxamount);
        }
    }
}
