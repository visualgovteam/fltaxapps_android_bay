package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.AdValorem;
import utils.Config;

/**
 * Created by SaraschandraaM on 22/08/15.
 */
public class AdValoremAdapter extends RecyclerView.Adapter<AdValoremAdapter.AdValHolder> {

    Context context;
    ArrayList<AdValorem> values;
    double amount;

    public AdValoremAdapter(Context context, ArrayList<AdValorem> values) {
        this.context = context;
        this.values = values;
    }

    @Override
    public AdValHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_advolarem, parent, false);
        AdValHolder holder = new AdValHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(AdValHolder holder, int position) {

        AdValorem data = values.get(position);
        holder.mTaxAuth.setText(data.getTaxingAuthority());
        holder.mMileage.setText(data.getMillageRate());
        String amt = data.getTaxAmount();
        amt = Config.formatCurrency(amt);
        holder.mTaxAmt.setText(amt);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    class AdValHolder extends RecyclerView.ViewHolder {

        TextView mTaxAuth, mMileage, mTaxAmt;

        public AdValHolder(View itemView) {
            super(itemView);
            mTaxAuth = (TextView) itemView.findViewById(R.id.tv_rc_taxingAuthority);
            mMileage = (TextView) itemView.findViewById(R.id.tv_rc_MilageRate);
            mTaxAmt = (TextView) itemView.findViewById(R.id.tv_rc_taxamount);
        }
    }
}
