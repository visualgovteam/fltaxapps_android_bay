package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.NonAdValorem;
import utils.Config;

/**
 * Created by SaraschandraaM on 22/08/15.
 */
public class NonAdValoremAdapter extends RecyclerView.Adapter<NonAdValoremAdapter.NonAdValoremHolder> {

    Context context;
    ArrayList<NonAdValorem> values;

    public NonAdValoremAdapter(Context context, ArrayList<NonAdValorem> values){
        this.context = context;
        this.values = values;
    }


    @Override
    public NonAdValoremHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_nonadvol, parent, false);
        NonAdValoremHolder holder = new NonAdValoremHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(NonAdValoremHolder holder, int position) {
        NonAdValorem data = values.get(position);
        holder.mTvDesc.setText(data.getTaxingAuthority());
        String amt = data.getTaxAmount();
        amt = Config.formatCurrency(amt);
        holder.mTvAmt.setText(amt);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    class NonAdValoremHolder extends RecyclerView.ViewHolder{

        TextView mTvDesc, mTvAmt;
        public NonAdValoremHolder(View itemView) {
            super(itemView);
            mTvDesc = (TextView) itemView.findViewById(R.id.tv_rc_desc);
            mTvAmt = (TextView) itemView.findViewById(R.id.tv_rc_amt);
        }
    }
}
