package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.baytaxCollector.R;
import com.visualgov.baytaxCollector.SearchFragmentActivity;

import java.util.ArrayList;

import Model.ResultsList;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.SearchResultHolder> {

    Context context;
    ArrayList<ResultsList> values;
    OnSearchResultClickListener onSearchResultClickListener;


    public SearchResultAdapter(Context context, ArrayList<ResultsList> values){
        this.context = context;
        this.values = values;
    }

    @Override
    public SearchResultHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View searchResultView = LayoutInflater.from(context).inflate(R.layout.cell_searchresult, parent, false);
        SearchResultHolder holder = new SearchResultHolder(searchResultView);
        return holder;
    }

    public void setOnSearchResultClickListener(OnSearchResultClickListener searchResultClickListener){
        this.onSearchResultClickListener = searchResultClickListener;
    }

    @Override
    public void onBindViewHolder(SearchResultHolder holder, int position) {
        if(values != null){
            holder.mOwnerName.setText(values.get(position).getNAME());
            holder.mTaxBillno.setText(values.get(position).getTAXBILLNO());
            holder.mPropertyNo.setText(values.get(position).getROLLTYPE()+" "+values.get(position).getPROPERTYNO()+" "+values.get(position).getTAXYEAR() );
            if(SearchFragmentActivity.defaultInstance().isTablet){
                holder.mAddr.setText(values.get(position).getADDR1());
            }

        }
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

     class SearchResultHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView mOwnerName, mTaxBillno, mPropertyNo, mAddr;
        public SearchResultHolder(View itemView) {
            super(itemView);
            mOwnerName = (TextView) itemView.findViewById(R.id.tv_srchrsult_name);
            mTaxBillno = (TextView) itemView.findViewById(R.id.tv_srchrsult_taxbillno);
            mPropertyNo = (TextView) itemView.findViewById(R.id.tv_srchrsult_prptyno);
            if(SearchFragmentActivity.defaultInstance().isTablet){
                mAddr = (TextView) itemView.findViewById(R.id.tv_srchrsult_addr);
            }

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onSearchResultClickListener != null){
                onSearchResultClickListener.OnSearchResultClicked(v, getPosition());
            }
        }


    }

    public interface OnSearchResultClickListener {
        public void OnSearchResultClicked(View v, int position);
    }
}
