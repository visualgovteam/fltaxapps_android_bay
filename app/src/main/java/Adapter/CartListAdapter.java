package Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import com.visualgov.baytaxCollector.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

import Model.CartItems;
import utils.Config;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
public class CartListAdapter extends RecyclerView.Adapter<CartListAdapter.CartListHolder> {

    Context context;
    ArrayList<CartItems> values;

    OnCheckedChangeListener onCheckedChangeListener;
    String amount;

    public CartListAdapter(Context context, ArrayList<CartItems> values){
        this.context = context;
        this.values = values;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener){
        this.onCheckedChangeListener = onCheckedChangeListener;
    }


    @Override
    public CartListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_cartitems, parent, false);
        CartListHolder holder = new CartListHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CartListHolder holder, int position) {
        CartItems data = values.get(position);
        holder.cartPropertyNo.setText(data.getRollType()+" "+data.getTaxBillNumber()+" "+data.getTaxYear());
        amount =data.getItemAmount();
        amount = Config.formatCurrency(amount);
        holder.cartAmtPay.setText(amount);
        holder.cartChkBox.setChecked(true);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }


    class CartListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{


        TextView cartPropertyNo, cartAmtPay;
        CheckBox cartChkBox;
        public CartListHolder(View itemView) {
            super(itemView);
            cartPropertyNo = (TextView) itemView.findViewById(R.id.tv_cartdet_prptyno);
            cartAmtPay = (TextView) itemView.findViewById(R.id.tv_cartdet_amtpay);
            cartChkBox = (CheckBox) itemView.findViewById(R.id.chk_cartdet);
            cartChkBox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onCheckedChangeListener != null){
                if(v.getId() == R.id.chk_cartdet){
                    onCheckedChangeListener.onCheckedChange(v, getPosition());
                }
            }
        }
    }

    public interface OnCheckedChangeListener{
        public void onCheckedChange(View view, int position);
    }


}
