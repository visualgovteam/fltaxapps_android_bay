package Model;

import android.os.Parcel;
import android.os.Parcelable;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 23/08/15.
 */
public class Values implements Parcelable {

    String key;
    String data;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Values(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Values(Parcel in){
        key = ParcelableUtils.readString(in);
        data = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, key);
        ParcelableUtils.write(dest, data);
    }

    public static final Creator<Values> CREATOR = new Creator<Values>() {
        @Override
        public Values createFromParcel(Parcel source) {
            return new Values(source);
        }

        @Override
        public Values[] newArray(int size) {
            return new Values[size];
        }
    };
}
