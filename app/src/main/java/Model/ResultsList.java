package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 18/08/15.
 */
public class ResultsList implements Parcelable{

    String ADDR1;
    String NAME;
    String PROPERTYNO;
    String PROTESTEDQ;
    String ROLLTYPE;
    String STATUS;
    String TAXBILLNO;
    String TAXYEAR;

    public String getADDR1() {
        return ADDR1;
    }

    public void setADDR1(String ADDR1) {
        this.ADDR1 = ADDR1;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPROPERTYNO() {
        return PROPERTYNO;
    }

    public void setPROPERTYNO(String PROPERTYNO) {
        this.PROPERTYNO = PROPERTYNO;
    }

    public String getPROTESTEDQ() {
        return PROTESTEDQ;
    }

    public void setPROTESTEDQ(String PROTESTEDQ) {
        this.PROTESTEDQ = PROTESTEDQ;
    }

    public String getROLLTYPE() {
        return ROLLTYPE;
    }

    public void setROLLTYPE(String ROLLTYPE) {
        this.ROLLTYPE = ROLLTYPE;
    }

    public String getSTATUS() {
        return STATUS;
    }

    public void setSTATUS(String STATUS) {
        this.STATUS = STATUS;
    }

    public String getTAXBILLNO() {
        return TAXBILLNO;
    }

    public void setTAXBILLNO(String TAXBILLNO) {
        this.TAXBILLNO = TAXBILLNO;
    }

    public String getTAXYEAR() {
        return TAXYEAR;
    }

    public void setTAXYEAR(String TAXYEAR) {
        this.TAXYEAR = TAXYEAR;
    }

    public ResultsList(JSONObject jsonObject){
        try{
            ADDR1 = jsonObject.getString("ADDR1");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            NAME = jsonObject.getString("NAME");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            PROPERTYNO = jsonObject.getString("PROPERTYNO");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            PROTESTEDQ = jsonObject.getString("PROTESTEDQ");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            ROLLTYPE = jsonObject.getString("ROLLTYPE");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            STATUS = jsonObject.getString("STATUS");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            TAXBILLNO = jsonObject.getString("TAXBILLNO");
        }catch(JSONException e){
            e.printStackTrace();
        }

        try{
            TAXYEAR = jsonObject.getString("TAXYEAR");
        }catch(JSONException e){
            e.printStackTrace();
        }
    }



    public ResultsList(Parcel in){
        ADDR1 = ParcelableUtils.readString(in);
        NAME = ParcelableUtils.readString(in);
        PROPERTYNO = ParcelableUtils.readString(in);
        PROTESTEDQ = ParcelableUtils.readString(in);
        ROLLTYPE = ParcelableUtils.readString(in);
        STATUS = ParcelableUtils.readString(in);
        TAXBILLNO = ParcelableUtils.readString(in);
        TAXYEAR = ParcelableUtils.readString(in);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, ADDR1);
        ParcelableUtils.write(dest, NAME);
        ParcelableUtils.write(dest, PROPERTYNO);
        ParcelableUtils.write(dest, PROTESTEDQ);
        ParcelableUtils.write(dest, ROLLTYPE);
        ParcelableUtils.write(dest, STATUS);
        ParcelableUtils.write(dest, TAXBILLNO);
        ParcelableUtils.write(dest, TAXYEAR);

    }


    public static final Parcelable.Creator<ResultsList> CREATOR = new Creator<ResultsList>() {
        @Override
        public ResultsList createFromParcel(Parcel source) {
            return new ResultsList(source);
        }

        @Override
        public ResultsList[] newArray(int size) {
            return new ResultsList[size];
        }
    };
}
