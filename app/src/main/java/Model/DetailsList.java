package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class DetailsList implements Parcelable {

     ArrayList<Details> detailsList;


    public DetailsList(JSONObject jsonObject){
        detailsList = new ArrayList<>();
       parseJson(jsonObject);
    }

    public ArrayList<Details> getDetailsList() {
        return detailsList;
    }

    public void setDetailsList(ArrayList<Details> detailsList) {
        this.detailsList = detailsList;
    }

    public void parseJson(JSONObject data){
        if (data != null) {
            Iterator<String> it = data.keys();
            while (it.hasNext()) {
                String key = it.next();
                Details details = new Details();
                try {
                    if (data.get(key) instanceof JSONArray) {
                        JSONArray arry = data.getJSONArray(key);
                        int size = arry.length();
                        for (int i = 0; i < size; i++) {
                            parseJson(arry.getJSONObject(i));
                        }
                    } else if (data.get(key) instanceof JSONObject) {
                        parseJson(data.getJSONObject(key));
                    } else {
                        details.setKey(key);
                        if(data.getString(key).equals("null")){
                            details.setValue("");
                        }else {
                            details.setValue(data.getString(key));
                        }
                        detailsList.add(details);
                    }
                } catch (Throwable e) {
                    try {
                    } catch (Exception ee) {
                    }
                    e.printStackTrace();

                }
            }
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public DetailsList(Parcel in){
        detailsList = ParcelableUtils.readParcelableList(in, Details.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, detailsList, flags);
    }

    public static final Creator<DetailsList> CREATOR = new Creator<DetailsList>() {
        @Override
        public DetailsList createFromParcel(Parcel source) {
            return new DetailsList(source);
        }

        @Override
        public DetailsList[] newArray(int size) {
            return new DetailsList[size];
        }
    };
}
