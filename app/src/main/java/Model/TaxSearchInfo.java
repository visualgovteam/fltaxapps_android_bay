package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
public class TaxSearchInfo implements Parcelable {

    ResultsInfo resultsInfo;
    ArrayList<ResultsList> resultsLists;

    public ResultsInfo getResultsInfo() {
        return resultsInfo;
    }

    public void setResultsInfo(ResultsInfo resultsInfo) {
        this.resultsInfo = resultsInfo;
    }

    public ArrayList<ResultsList> getResultsLists() {
        return resultsLists;
    }

    public void setResultsLists(ArrayList<ResultsList> resultsLists) {
        this.resultsLists = resultsLists;
    }

    public TaxSearchInfo(JSONObject jsonObject){
        try{
            JSONObject json = jsonObject.getJSONObject("ResultsInfo");
            resultsInfo = new ResultsInfo(json);
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            resultsLists = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("ResultsList");
            if(json == null){
                JSONArray resultsarray = jsonObject.getJSONArray("ResultsList");
                for (int i=0; i<resultsarray.length(); i++){
                    JSONObject jObj = (JSONObject) resultsarray.get(i);
                    ResultsList data = new ResultsList(jObj);
                    resultsLists.add(data);
                }
            }else{
                ResultsList data = new ResultsList(json);
                resultsLists.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }



    @Override
    public int describeContents() {
        return 0;
    }

    public TaxSearchInfo(Parcel in){
        resultsInfo = ParcelableUtils.readParcelable(in, ResultsInfo.class.getClassLoader());
        resultsLists = ParcelableUtils.readParcelableList(in, ResultsList.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, resultsLists, flags);
        ParcelableUtils.write(dest, resultsInfo, flags);
    }

    public static final Creator<TaxSearchInfo> CREATOR = new Creator<TaxSearchInfo>() {
        @Override
        public TaxSearchInfo createFromParcel(Parcel source) {
            return new TaxSearchInfo(source);
        }

        @Override
        public TaxSearchInfo[] newArray(int size) {
            return new TaxSearchInfo[size];
        }
    };
}
