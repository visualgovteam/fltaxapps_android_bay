package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 27/09/15.
 */
public class TaxBillStatusList implements Parcelable {


    String BillStatus;


    public String getBillStatus() {
        return BillStatus;
    }

    public void setBillStatus(String billStatus) {
        BillStatus = billStatus;
    }

    public TaxBillStatusList(JSONObject jsonObject){

        try{
            BillStatus = jsonObject.getString("BillStatus");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }


    public TaxBillStatusList(Parcel in){
        BillStatus = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, BillStatus);
    }

    public static final Creator<TaxBillStatusList> CREATOR = new Creator<TaxBillStatusList>() {
        @Override
        public TaxBillStatusList createFromParcel(Parcel source) {
            return new TaxBillStatusList(source);
        }

        @Override
        public TaxBillStatusList[] newArray(int size) {
            return new TaxBillStatusList[size];
        }
    };
}
