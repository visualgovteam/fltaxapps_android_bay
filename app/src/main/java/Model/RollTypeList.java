package Model;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class RollTypeList {

    ArrayList<RollType> mRollTypeList;

    public ArrayList<RollType> getmRollTypeList() {
        return mRollTypeList;
    }

    public void setmRollTypeList(ArrayList<RollType> mRollTypeList) {
        this.mRollTypeList = mRollTypeList;
    }

    public RollTypeList(JSONObject jsonObject){

        try {
            mRollTypeList  = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("RollTypeList");
            if (json == null) {
                JSONArray rollarray = jsonObject.getJSONArray("RollTypeList");
                for(int i=0; i<rollarray.length(); i++){
                    JSONObject jObj = (JSONObject) rollarray.get(i);
                    RollType data = new RollType(jObj);
                    mRollTypeList.add(data);
                }
            }else {
                RollType data = new RollType(json);
                mRollTypeList.add(data);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
