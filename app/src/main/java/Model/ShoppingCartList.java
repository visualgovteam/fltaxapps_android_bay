package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
public class ShoppingCartList implements Parcelable {


    String GrandTotalAmount;
    String InvoiceKey;
    String ItemsCount;
    String TaxShoppingCartItems;
    String TotalAmount;
    String TotalFeeAmount;

    public String getGrandTotalAmount() {
        return GrandTotalAmount;
    }

    public void setGrandTotalAmount(String grandTotalAmount) {
        GrandTotalAmount = grandTotalAmount;
    }

    public String getInvoiceKey() {
        return InvoiceKey;
    }

    public void setInvoiceKey(String invoiceKey) {
        InvoiceKey = invoiceKey;
    }

    public String getItemsCount() {
        return ItemsCount;
    }

    public void setItemsCount(String itemsCount) {
        ItemsCount = itemsCount;
    }

    public String getTaxShoppingCartItems() {
        return TaxShoppingCartItems;
    }

    public void setTaxShoppingCartItems(String taxShoppingCartItems) {
        TaxShoppingCartItems = taxShoppingCartItems;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getTotalFeeAmount() {
        return TotalFeeAmount;
    }

    public void setTotalFeeAmount(String totalFeeAmount) {
        TotalFeeAmount = totalFeeAmount;
    }

    public ShoppingCartList(){

    }

    public ShoppingCartList(JSONObject jsonObject){

        try{
            GrandTotalAmount = jsonObject.getString("GrandTotalAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            InvoiceKey = jsonObject.getString("InvoiceKey");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            ItemsCount = jsonObject.getString("ItemsCount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxShoppingCartItems = jsonObject.getString("TaxShoppingCartItems");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TotalAmount = jsonObject.getString("TotalAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TotalFeeAmount = jsonObject.getString("TotalFeeAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public ShoppingCartList(Parcel in){
        GrandTotalAmount = ParcelableUtils.readString(in);
        InvoiceKey = ParcelableUtils.readString(in);
        ItemsCount = ParcelableUtils.readString(in);
        TaxShoppingCartItems = ParcelableUtils.readString(in);
        TotalAmount = ParcelableUtils.readString(in);
        TotalFeeAmount = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, GrandTotalAmount);
        ParcelableUtils.write(dest, InvoiceKey);
        ParcelableUtils.write(dest, ItemsCount);
        ParcelableUtils.write(dest, TaxShoppingCartItems);
        ParcelableUtils.write(dest, TotalAmount);
        ParcelableUtils.write(dest, TotalFeeAmount);
    }

    public static final Creator<ShoppingCartList> CREATOR = new Creator<ShoppingCartList>() {
        @Override
        public ShoppingCartList createFromParcel(Parcel source) {
            return new ShoppingCartList(source);
        }

        @Override
        public ShoppingCartList[] newArray(int size) {
            return new ShoppingCartList[size];
        }
    };
}
