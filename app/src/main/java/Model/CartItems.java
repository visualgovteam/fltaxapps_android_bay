package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
public class CartItems implements Parcelable {

    String FeeAmount;
    String ItemAmount;
    String ItemID;
    String RollType;
    String TaxBillNumber;
    String TaxProductType;
    String TaxYear;
    String VGProductCode;

    public String getFeeAmount() {
        return FeeAmount;
    }

    public void setFeeAmount(String feeAmount) {
        FeeAmount = feeAmount;
    }

    public String getItemAmount() {
        return ItemAmount;
    }

    public void setItemAmount(String itemAmount) {
        ItemAmount = itemAmount;
    }

    public String getItemID() {
        return ItemID;
    }

    public void setItemID(String itemID) {
        ItemID = itemID;
    }

    public String getRollType() {
        return RollType;
    }

    public void setRollType(String rollType) {
        RollType = rollType;
    }

    public String getTaxBillNumber() {
        return TaxBillNumber;
    }

    public void setTaxBillNumber(String taxBillNumber) {
        TaxBillNumber = taxBillNumber;
    }

    public String getTaxProductType() {
        return TaxProductType;
    }

    public void setTaxProductType(String taxProductType) {
        TaxProductType = taxProductType;
    }

    public String getTaxYear() {
        return TaxYear;
    }

    public void setTaxYear(String taxYear) {
        TaxYear = taxYear;
    }

    public String getVGProductCode() {
        return VGProductCode;
    }

    public void setVGProductCode(String VGProductCode) {
        this.VGProductCode = VGProductCode;
    }

    public CartItems(JSONObject jsonObject){

        try{
            FeeAmount = jsonObject.getString("FeeAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            ItemAmount = jsonObject.getString("ItemAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            ItemID = jsonObject.getString("ItemID");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            RollType = jsonObject.getString("RollType");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxBillNumber = jsonObject.getString("TaxBillNumber");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxProductType = jsonObject.getString("TaxProductType");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxYear = jsonObject.getString("TaxYear");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            VGProductCode = jsonObject.getString("VGProductCode");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }



    @Override
    public int describeContents() {
        return 0;
    }

    public CartItems(Parcel in){
        FeeAmount = ParcelableUtils.readString(in);
        ItemAmount  = ParcelableUtils.readString(in);
        ItemID = ParcelableUtils.readString(in);
        RollType = ParcelableUtils.readString(in);
        TaxBillNumber = ParcelableUtils.readString(in);
        TaxProductType = ParcelableUtils.readString(in);
        TaxYear = ParcelableUtils.readString(in);
        VGProductCode = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, FeeAmount);
        ParcelableUtils.write(dest, ItemAmount);
        ParcelableUtils.write(dest, ItemID);
        ParcelableUtils.write(dest, RollType);
        ParcelableUtils.write(dest, TaxBillNumber);
        ParcelableUtils.write(dest, TaxProductType);
        ParcelableUtils.write(dest, TaxYear);
        ParcelableUtils.write(dest, VGProductCode);

    }

    public static final Creator<CartItems> CREATOR = new Creator<CartItems>() {
        @Override
        public CartItems createFromParcel(Parcel source) {
            return new CartItems(source);
        }

        @Override
        public CartItems[] newArray(int size) {
            return new CartItems[size];
        }
    };
}
