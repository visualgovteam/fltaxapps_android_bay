package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class DelinquentList implements Parcelable {

    String TaxBill;
    String TaxOutstanding;
    String TaxPenalties;
    String TaxReference;
    String TaxRoll;
    String TaxTotal;
    String TaxYear;

    public String getTaxBill() {
        return TaxBill;
    }

    public void setTaxBill(String taxBill) {
        TaxBill = taxBill;
    }

    public String getTaxOutstanding() {
        return TaxOutstanding;
    }

    public void setTaxOutstanding(String taxOutstanding) {
        TaxOutstanding = taxOutstanding;
    }

    public String getTaxPenalties() {
        return TaxPenalties;
    }

    public void setTaxPenalties(String taxPenalties) {
        TaxPenalties = taxPenalties;
    }

    public String getTaxReference() {
        return TaxReference;
    }

    public void setTaxReference(String taxReference) {
        TaxReference = taxReference;
    }

    public String getTaxRoll() {
        return TaxRoll;
    }

    public void setTaxRoll(String taxRoll) {
        TaxRoll = taxRoll;
    }

    public String getTaxTotal() {
        return TaxTotal;
    }

    public void setTaxTotal(String taxTotal) {
        TaxTotal = taxTotal;
    }

    public String getTaxYear() {
        return TaxYear;
    }

    public void setTaxYear(String taxYear) {
        TaxYear = taxYear;
    }

    public DelinquentList(JSONObject jsonObject){
        try{
            TaxBill = jsonObject.getString("TaxBill");
            if(TaxBill.equals("null")){
                TaxBill = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxOutstanding = jsonObject.getString("TaxOutstanding");
            if(TaxOutstanding.equals("null")){
                TaxOutstanding = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxPenalties = jsonObject.getString("TaxPenalties");
            if(TaxPenalties.equals("null")){
                TaxPenalties = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxReference = jsonObject.getString("TaxReference");
            if(TaxReference.equals("null")){
                TaxReference = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxRoll = jsonObject.getString("TaxRoll");
            if(TaxRoll.equals("null")){
                TaxRoll = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxTotal = jsonObject.getString("TaxTotal");
            if(TaxTotal.equals("null")){
                TaxTotal = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxYear = jsonObject.getString("TaxYear");
            if(TaxYear.equals("null")){
                TaxYear = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public DelinquentList(Parcel in){
        TaxBill = ParcelableUtils.readString(in);
        TaxOutstanding = ParcelableUtils.readString(in);
        TaxPenalties = ParcelableUtils.readString(in);
        TaxReference = ParcelableUtils.readString(in);
        TaxRoll = ParcelableUtils.readString(in);
        TaxTotal = ParcelableUtils.readString(in);
        TaxYear = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, TaxBill);
        ParcelableUtils.write(dest, TaxOutstanding);
        ParcelableUtils.write(dest, TaxPenalties);
        ParcelableUtils.write(dest, TaxReference);
        ParcelableUtils.write(dest, TaxRoll);
        ParcelableUtils.write(dest, TaxTotal);
        ParcelableUtils.write(dest, TaxYear);
    }

    public static final Creator<DelinquentList> CREATOR = new Creator<DelinquentList>() {
        @Override
        public DelinquentList createFromParcel(Parcel source) {
            return new DelinquentList(source);
        }

        @Override
        public DelinquentList[] newArray(int size) {
            return new DelinquentList[size];
        }
    };
}
