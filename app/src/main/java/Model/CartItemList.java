package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
public class CartItemList implements Parcelable{

    ArrayList<CartItems> cartItemsList;

    ShoppingCartList shoppingCartList;

    boolean hasInstallment;

    int installmentCount;

    public ArrayList<CartItems> getCartItemsList() {
        return cartItemsList;
    }

    public void setCartItemsList(ArrayList<CartItems> cartItemsList) {
        this.cartItemsList = cartItemsList;
    }

    public ShoppingCartList getShoppingCartList() {
        return shoppingCartList;
    }

    public void setShoppingCartList(ShoppingCartList shoppingCartList) {
        this.shoppingCartList = shoppingCartList;
    }

    public boolean isHasInstallment() {
        return hasInstallment;
    }

    public void setHasInstallment(boolean hasInstallment) {
        this.hasInstallment = hasInstallment;
    }

    public int getInstallmentCount() {
        return installmentCount;
    }

    public void setInstallmentCount(int installmentCount) {
        this.installmentCount = installmentCount;
    }

    public CartItemList(JSONObject jsonObject){
        try{
            cartItemsList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("CartItems");
            if(json == null){
                JSONArray cartarray = jsonObject.getJSONArray("CartItems");
                for (int i=0; i<cartarray.length(); i++){
                    JSONObject jObj = (JSONObject) cartarray.get(i);
                    CartItems data = new CartItems(jObj);
                    cartItemsList.add(data);
                }
            }else{
                CartItems data = new CartItems(json);
                cartItemsList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject json = jsonObject.getJSONObject("ShoppingCartList");
            shoppingCartList = new ShoppingCartList(json);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public CartItemList(Parcel in){
        cartItemsList = ParcelableUtils.readParcelableList(in, CartItems.class.getClassLoader());
        shoppingCartList = ParcelableUtils.readParcelable(in, ShoppingCartList.class.getClassLoader());
        hasInstallment = ParcelableUtils.readBoolean(in);
        installmentCount = ParcelableUtils.readInt(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, cartItemsList, flags);
        ParcelableUtils.write(dest, shoppingCartList, flags);
        ParcelableUtils.write(dest, hasInstallment);
        ParcelableUtils.write(dest, installmentCount);
    }

    public static final Creator<CartItemList> CREATOR = new Creator<CartItemList>() {
        @Override
        public CartItemList createFromParcel(Parcel source) {
            return new CartItemList(source);
        }

        @Override
        public CartItemList[] newArray(int size) {
            return new CartItemList[size];
        }
    };
}
