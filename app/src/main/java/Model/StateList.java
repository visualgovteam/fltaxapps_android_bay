package Model;

/**
 * Created by SaraschandraaM on 16/09/15.
 */
public class StateList {

    String StateName;

    String StateCode;


    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }
}
