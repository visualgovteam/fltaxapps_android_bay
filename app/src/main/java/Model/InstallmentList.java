package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SaraschandraaM on 27/08/15.
 */
public class InstallmentList implements Parcelable {

    String AmountCollected;
    String AmtCollected;
    String DueAmts1;
    String DueLiteral1;
    String InstallAmt;
    String InstallmentNumber;
    String LastPmDate;
    String Name;
    String PropertyNO;
    String ReceiptNo;
    String RollType;
    String TaxBillNo;
    String TaxYear;
    String TOTALSCollections;
    String Trn;

    public String getAmountCollected() {
        return AmountCollected;
    }

    public void setAmountCollected(String amountCollected) {
        AmountCollected = amountCollected;
    }

    public String getAmtCollected() {
        return AmtCollected;
    }

    public void setAmtCollected(String amtCollected) {
        AmtCollected = amtCollected;
    }

    public String getDueAmts1() {
        return DueAmts1;
    }

    public void setDueAmts1(String dueAmts1) {
        DueAmts1 = dueAmts1;
    }

    public String getDueLiteral1() {
        return DueLiteral1;
    }

    public void setDueLiteral1(String dueLiteral1) {
        DueLiteral1 = dueLiteral1;
    }

    public String getInstallAmt() {
        return InstallAmt;
    }

    public void setInstallAmt(String installAmt) {
        InstallAmt = installAmt;
    }

    public String getInstallmentNumber() {
        return InstallmentNumber;
    }

    public void setInstallmentNumber(String installmentNumber) {
        InstallmentNumber = installmentNumber;
    }

    public String getLastPmDate() {
        return LastPmDate;
    }

    public void setLastPmDate(String lastPmDate) {
        LastPmDate = lastPmDate;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPropertyNO() {
        return PropertyNO;
    }

    public void setPropertyNO(String propertyNO) {
        PropertyNO = propertyNO;
    }

    public String getReceiptNo() {
        return ReceiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        ReceiptNo = receiptNo;
    }

    public String getRollType() {
        return RollType;
    }

    public void setRollType(String rollType) {
        RollType = rollType;
    }

    public String getTaxBillNo() {
        return TaxBillNo;
    }

    public void setTaxBillNo(String taxBillNo) {
        TaxBillNo = taxBillNo;
    }

    public String getTaxYear() {
        return TaxYear;
    }

    public void setTaxYear(String taxYear) {
        TaxYear = taxYear;
    }

    public String getTOTALSCollections() {
        return TOTALSCollections;
    }

    public void setTOTALSCollections(String TOTALSCollections) {
        this.TOTALSCollections = TOTALSCollections;
    }

    public String getTrn() {
        return Trn;
    }

    public void setTrn(String trn) {
        Trn = trn;
    }

    public InstallmentList(JSONObject jsonObject){

        try{
            AmountCollected = jsonObject.getString("AmountCollected") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            AmtCollected = jsonObject.getString("AmtCollected") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            DueAmts1 = jsonObject.getString("DueAmts1") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            DueLiteral1 = jsonObject.getString("DueLiteral1") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            InstallAmt = jsonObject.getString("InstallAmt") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            InstallmentNumber = jsonObject.getString("InstallmentNumber") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            LastPmDate = jsonObject.getString("LastPmDate") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            Name = jsonObject.getString("Name") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            PropertyNO = jsonObject.getString("PropertyNO") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            ReceiptNo = jsonObject.getString("ReceiptNo") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            RollType = jsonObject.getString("RollType") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            TaxBillNo = jsonObject.getString("TaxBillNo") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            TaxYear = jsonObject.getString("TaxYear") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            TOTALSCollections = jsonObject.getString("TOTALSCollections") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }

        try{
            Trn = jsonObject.getString("Trn") ;
        }catch (JSONException exception){
            exception.printStackTrace();
        }


    }



    @Override
    public int describeContents() {
        return 0;
    }

    public InstallmentList(Parcel in){

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    public static final Creator<InstallmentList> CREATOR = new Creator<InstallmentList>() {
        @Override
        public InstallmentList createFromParcel(Parcel source) {
            return new InstallmentList(source);
        }

        @Override
        public InstallmentList[] newArray(int size) {
            return new InstallmentList[size];
        }
    };
}
