package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 18/08/15.
 */
public class ResultsInfo implements Parcelable {

    String Rows;
    String Skip;
    String TotalRowCount;


    public String getRows() {
        return Rows;
    }

    public void setRows(String rows) {
        Rows = rows;
    }

    public String getSkip() {
        return Skip;
    }

    public void setSkip(String skip) {
        Skip = skip;
    }

    public String getTotalRowCount() {
        return TotalRowCount;
    }

    public void setTotalRowCount(String totalRowCount) {
        TotalRowCount = totalRowCount;
    }

    public ResultsInfo(JSONObject jsonObject){

        try{
            Rows = jsonObject.getString("Rows");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            Skip = jsonObject.getString("Skip");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TotalRowCount = jsonObject.getString("TotalRowCount");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public ResultsInfo(Parcel in){
        Rows = ParcelableUtils.readString(in);
        Skip = ParcelableUtils.readString(in);
        TotalRowCount = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, Rows);
        ParcelableUtils.write(dest, Skip);
        ParcelableUtils.write(dest, TotalRowCount);
    }

    public static final Creator<ResultsInfo> CREATOR = new Creator<ResultsInfo>() {
        @Override
        public ResultsInfo createFromParcel(Parcel source) {
            return new ResultsInfo(source);
        }

        @Override
        public ResultsInfo[] newArray(int size) {
            return new ResultsInfo[size];
        }
    };
}
