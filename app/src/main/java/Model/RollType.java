package Model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class RollType {

    String TaxRollCharacter;
    String TaxRollDescription;

    public String getTaxRollCharacter() {
        return TaxRollCharacter;
    }

    public void setTaxRollCharacter(String taxRollCharacter) {
        TaxRollCharacter = taxRollCharacter;
    }

    public String getTaxRollDescription() {
        return TaxRollDescription;
    }

    public void setTaxRollDescription(String taxRollDescription) {
        TaxRollDescription = taxRollDescription;
    }

    public RollType(JSONObject jsonObject){

        try{
            TaxRollCharacter = jsonObject.getString("TaxRollCharacter");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxRollDescription = jsonObject.getString("TaxRollDescription");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
