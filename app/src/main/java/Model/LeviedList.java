package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class LeviedList implements Parcelable {

    String LeviedAdjustment;
    String LeviedDescription;
    String LeviedOriginal;

    public String getLeviedAdjustment() {
        return LeviedAdjustment;
    }

    public void setLeviedAdjustment(String leviedAdjustment) {
        LeviedAdjustment = leviedAdjustment;
    }

    public String getLeviedDescription() {
        return LeviedDescription;
    }

    public void setLeviedDescription(String leviedDescription) {
        LeviedDescription = leviedDescription;
    }

    public String getLeviedOriginal() {
        return LeviedOriginal;
    }

    public void setLeviedOriginal(String leviedOriginal) {
        LeviedOriginal = leviedOriginal;
    }

    public LeviedList(JSONObject jsonObject){

        try{
            LeviedAdjustment = jsonObject.getString("LeviedAdjustment");
            if(LeviedAdjustment.equals("null")){
                LeviedAdjustment = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            LeviedDescription = jsonObject.getString("LeviedDescription");
            if(LeviedDescription.equals("null")){
                LeviedDescription = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            LeviedOriginal = jsonObject.getString("LeviedOriginal");
            if(LeviedOriginal.equals("null")){
                LeviedOriginal = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public LeviedList (Parcel in){
        LeviedAdjustment = ParcelableUtils.readString(in);
        LeviedDescription = ParcelableUtils.readString(in);
        LeviedOriginal = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, LeviedAdjustment);
        ParcelableUtils.write(dest, LeviedDescription);
        ParcelableUtils.write(dest, LeviedOriginal);
    }

    public static final Creator<LeviedList> CREATOR = new Creator<LeviedList>() {
        @Override
        public LeviedList createFromParcel(Parcel source) {
            return new LeviedList(source);
        }

        @Override
        public LeviedList[] newArray(int size) {
            return new LeviedList[size];
        }
    };
}
