package Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class YearList {

    ArrayList<Year> mYearList;

    public ArrayList<Year> getmYearList() {
        return mYearList;
    }

    public void setmYearList(ArrayList<Year> mYearList) {
        this.mYearList = mYearList;
    }

    public YearList(JSONObject jsonObject) {
        try {
            mYearList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("YearList");
            if(json == null){
                JSONArray yearArray = jsonObject.getJSONArray("YearList");
                for(int i=0; i<yearArray.length(); i++){
                    JSONObject jObj = (JSONObject) yearArray.get(i);
                    Year data = new Year(jObj);
                    mYearList.add(data);
                }
            }else{
                Year data = new Year(json);
                mYearList.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
