package Model;

import android.os.Parcel;
import android.os.Parcelable;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class Details implements Parcelable {

    String key;
    String value;

    public Details(){

    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, key);
        ParcelableUtils.write(dest, value);
    }

    public Details(Parcel in){
        key = ParcelableUtils.readString(in);
        value = ParcelableUtils.readString(in);
    }

    public static final Creator<Details> CREATOR = new Creator<Details>() {
        @Override
        public Details createFromParcel(Parcel source) {
            return new Details(source);
        }

        @Override
        public Details[] newArray(int size) {
            return new Details[size];
        }
    };
}
