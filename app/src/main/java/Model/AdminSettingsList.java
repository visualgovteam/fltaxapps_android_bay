package Model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class AdminSettingsList {

    String CheckEnabled;
    String CheckMessage;
    String CreditCardEnabled;
    String CreditCardMessage;
    String DelinquencyPayEnabled;
    String DisplayPaidDelinquentBill;
    String DisplayProtestBill;
    String InstallmentPayEnabled;
    String IVREnabled;
    String MaxCartCapacity;
    String MobileCheckEnabled;
    String MobileCreditCardEnabled;
    String SiteEnabled;
    String SiteMessage;

    public String getCheckEnabled() {
        return CheckEnabled;
    }

    public void setCheckEnabled(String checkEnabled) {
        CheckEnabled = checkEnabled;
    }

    public String getCheckMessage() {
        return CheckMessage;
    }

    public void setCheckMessage(String checkMessage) {
        CheckMessage = checkMessage;
    }

    public String getCreditCardEnabled() {
        return CreditCardEnabled;
    }

    public void setCreditCardEnabled(String creditCardEnabled) {
        CreditCardEnabled = creditCardEnabled;
    }

    public String getCreditCardMessage() {
        return CreditCardMessage;
    }

    public void setCreditCardMessage(String creditCardMessage) {
        CreditCardMessage = creditCardMessage;
    }

    public String getDelinquencyPayEnabled() {
        return DelinquencyPayEnabled;
    }

    public void setDelinquencyPayEnabled(String delinquencyPayEnabled) {
        DelinquencyPayEnabled = delinquencyPayEnabled;
    }

    public String getDisplayPaidDelinquentBill() {
        return DisplayPaidDelinquentBill;
    }

    public void setDisplayPaidDelinquentBill(String displayPaidDelinquentBill) {
        DisplayPaidDelinquentBill = displayPaidDelinquentBill;
    }

    public String getDisplayProtestBill() {
        return DisplayProtestBill;
    }

    public void setDisplayProtestBill(String displayProtestBill) {
        DisplayProtestBill = displayProtestBill;
    }

    public String getInstallmentPayEnabled() {
        return InstallmentPayEnabled;
    }

    public void setInstallmentPayEnabled(String installmentPayEnabled) {
        InstallmentPayEnabled = installmentPayEnabled;
    }

    public String getIVREnabled() {
        return IVREnabled;
    }

    public void setIVREnabled(String IVREnabled) {
        this.IVREnabled = IVREnabled;
    }

    public String getMaxCartCapacity() {
        return MaxCartCapacity;
    }

    public void setMaxCartCapacity(String maxCartCapacity) {
        MaxCartCapacity = maxCartCapacity;
    }

    public String getMobileCheckEnabled() {
        return MobileCheckEnabled;
    }

    public void setMobileCheckEnabled(String mobileCheckEnabled) {
        MobileCheckEnabled = mobileCheckEnabled;
    }

    public String getMobileCreditCardEnabled() {
        return MobileCreditCardEnabled;
    }

    public void setMobileCreditCardEnabled(String mobileCreditCardEnabled) {
        MobileCreditCardEnabled = mobileCreditCardEnabled;
    }

    public String getSiteEnabled() {
        return SiteEnabled;
    }

    public void setSiteEnabled(String siteEnabled) {
        SiteEnabled = siteEnabled;
    }

    public String getSiteMessage() {
        return SiteMessage;
    }

    public void setSiteMessage(String siteMessage) {
        SiteMessage = siteMessage;
    }

    public AdminSettingsList(JSONObject jsonObject){


        try{
            CheckEnabled = jsonObject.getString("CheckEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            CheckMessage = jsonObject.getString("CheckMessage");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            CreditCardEnabled = jsonObject.getString("CreditCardEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            CreditCardMessage = jsonObject.getString("CreditCardMessage");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            DelinquencyPayEnabled = jsonObject.getString("DelinquencyPayEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            DisplayPaidDelinquentBill = jsonObject.getString("DisplayPaidDelinquentBill");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            DisplayProtestBill = jsonObject.getString("DisplayProtestBill");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            InstallmentPayEnabled = jsonObject.getString("InstallmentPayEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            IVREnabled = jsonObject.getString("IVREnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            MaxCartCapacity = jsonObject.getString("MaxCartCapacity");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            MobileCheckEnabled = jsonObject.getString("MobileCheckEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            MobileCreditCardEnabled = jsonObject.getString("MobileCreditCardEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            SiteEnabled = jsonObject.getString("SiteEnabled");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            SiteMessage = jsonObject.getString("SiteMessage");
        }catch (JSONException e){
            e.printStackTrace();
        }


    }
}
