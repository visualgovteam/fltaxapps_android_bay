package Model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class Year {

    String SNo;
    String TaxYear;

    public String getSNo() {
        return SNo;
    }

    public void setSNo(String SNo) {
        this.SNo = SNo;
    }

    public String getTaxYear() {
        return TaxYear;
    }

    public void setTaxYear(String taxYear) {
        TaxYear = taxYear;
    }

    public Year(JSONObject jsonObject){
        try{
            SNo = jsonObject.getString("SNo");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxYear = jsonObject.getString("TaxYear");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
