package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class NonAdValorem implements Parcelable {

    String PlaceHolder;
    String TaxAmount;
    String TaxingAuthority;

    public String getPlaceHolder() {
        return PlaceHolder;
    }

    public void setPlaceHolder(String placeHolder) {
        PlaceHolder = placeHolder;
    }

    public String getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        TaxAmount = taxAmount;
    }

    public String getTaxingAuthority() {
        return TaxingAuthority;
    }

    public void setTaxingAuthority(String taxingAuthority) {
        TaxingAuthority = taxingAuthority;
    }

    public NonAdValorem(){

    }

    public NonAdValorem(JSONObject jsonObject){

        try{
            PlaceHolder = jsonObject.getString("PlaceHolder");
            if(PlaceHolder.equals("null")){
                PlaceHolder = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxAmount = jsonObject.getString("TaxAmount");
            if(TaxAmount.equals("null")){
                TaxAmount = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TaxingAuthority = jsonObject.getString("TaxingAuthority");
            if(TaxingAuthority.equals("null")){
                TaxingAuthority = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public NonAdValorem(Parcel in){
        PlaceHolder = ParcelableUtils.readString(in);
        TaxAmount = ParcelableUtils.readString(in);
        TaxingAuthority = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, PlaceHolder);
        ParcelableUtils.write(dest, TaxAmount);
        ParcelableUtils.write(dest, TaxingAuthority);
    }

    public static final Creator<NonAdValorem> CREATOR = new Creator<NonAdValorem>() {
        @Override
        public NonAdValorem createFromParcel(Parcel source) {
            return new NonAdValorem(source);
        }

        @Override
        public NonAdValorem[] newArray(int size) {
            return new NonAdValorem[size];
        }
    };
}
