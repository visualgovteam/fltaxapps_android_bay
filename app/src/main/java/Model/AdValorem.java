package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class AdValorem implements Parcelable {

    String MillageRate;
    String TaxAmount;
    String TaxingAuthority;

    public String getMillageRate() {
        return MillageRate;
    }

    public void setMillageRate(String millageRate) {
        MillageRate = millageRate;
    }

    public String getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(String taxAmount) {
        TaxAmount = taxAmount;
    }

    public String getTaxingAuthority() {
        return TaxingAuthority;
    }

    public void setTaxingAuthority(String taxingAuthority) {
        TaxingAuthority = taxingAuthority;
    }

    public AdValorem(){

    }

    public AdValorem(JSONObject jsonObject) {

        try {
            MillageRate = jsonObject.getString("MillageRate");
            if (MillageRate.equals("null")) {
                MillageRate = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            TaxAmount = jsonObject.getString("TaxAmount");
            if (TaxAmount.equals("null")) {
                TaxAmount = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            TaxingAuthority = jsonObject.getString("TaxingAuthority");
            if (TaxingAuthority.equals("null")) {
                TaxingAuthority = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public AdValorem(Parcel in) {
        MillageRate = ParcelableUtils.readString(in);
        TaxAmount = ParcelableUtils.readString(in);
        TaxingAuthority = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, MillageRate);
        ParcelableUtils.write(dest, TaxAmount);
        ParcelableUtils.write(dest, TaxingAuthority);
    }

    public static final Creator<AdValorem> CREATOR = new Creator<AdValorem>() {
        @Override
        public AdValorem createFromParcel(Parcel source) {
            return new AdValorem(source);
        }

        @Override
        public AdValorem[] newArray(int size) {
            return new AdValorem[size];
        }
    };
}
