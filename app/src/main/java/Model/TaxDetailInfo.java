package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class TaxDetailInfo implements Parcelable {

    ArrayList<LeviedList> mLeviedList;
    TaxMiscList taxMiscList;
    ArrayList<DueList> mDueList;
    ArrayList<DelinquentList> mDelinquentList;
    ArrayList<AdValorem> mAdValoremList;
    ArrayList<NonAdValorem> mNonAdValoremList;
    DetailsList mDetailsList;
    ArrayList<InstallmentList> mInstallmentList;
    TaxBillStatusList taxBillStatusList;

    public ArrayList<LeviedList> getmLeviedList() {
        return mLeviedList;
    }

    public void setmLeviedList(ArrayList<LeviedList> mLeviedList) {
        this.mLeviedList = mLeviedList;
    }

    public DetailsList getmDetailsList() {
        return mDetailsList;
    }

    public void setmDetailsList(DetailsList mDetailsList) {
        this.mDetailsList = mDetailsList;
    }

    public TaxMiscList getTaxMiscList() {
        return taxMiscList;
    }

    public void setTaxMiscList(TaxMiscList taxMiscList) {
        this.taxMiscList = taxMiscList;
    }

    public ArrayList<DueList> getmDueList() {
        return mDueList;
    }

    public void setmDueList(ArrayList<DueList> mDueList) {
        this.mDueList = mDueList;
    }

    public ArrayList<DelinquentList> getmDelinquentList() {
        return mDelinquentList;
    }

    public void setmDelinquentList(ArrayList<DelinquentList> mDelinquentList) {
        this.mDelinquentList = mDelinquentList;
    }

    public ArrayList<AdValorem> getmAdValoremList() {
        return mAdValoremList;
    }

    public void setmAdValoremList(ArrayList<AdValorem> mAdValoremList) {
        this.mAdValoremList = mAdValoremList;
    }

    public ArrayList<NonAdValorem> getmNonAdValoremList() {
        return mNonAdValoremList;
    }

    public void setmNonAdValoremList(ArrayList<NonAdValorem> mNonAdValoremList) {
        this.mNonAdValoremList = mNonAdValoremList;
    }

    public ArrayList<InstallmentList> getmInstallmentList() {
        return mInstallmentList;
    }

    public void setmInstallmentList(ArrayList<InstallmentList> mInstallmentList) {
        this.mInstallmentList = mInstallmentList;
    }

    public TaxBillStatusList getTaxBillStatusList() {
        return taxBillStatusList;
    }

    public void setTaxBillStatusList(TaxBillStatusList taxBillStatusList) {
        this.taxBillStatusList = taxBillStatusList;
    }

    public TaxDetailInfo(JSONObject jsonObject){


        try{
            mLeviedList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("LeviedList");
            if(json == null){
                JSONArray leviedarray = jsonObject.getJSONArray("LeviedList");
                for (int i=0; i<leviedarray.length(); i++){
                    JSONObject jObj = (JSONObject) leviedarray.get(i);
                    LeviedList data = new LeviedList(jObj);
                    mLeviedList.add(data);
                }
            }else{
                LeviedList data = new LeviedList(json);
                mLeviedList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            mDueList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("DueList");
            if(json == null){
                JSONArray duearray = jsonObject.getJSONArray("DueList");
                for (int i=0; i<duearray.length(); i++){
                    JSONObject jObj = (JSONObject) duearray.get(i);
                    DueList data = new DueList(jObj);
                    mDueList.add(data);
                }
            }else{
                DueList data = new DueList(json);
                mDueList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            mDelinquentList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("DelinquentList");
            if(json == null){
                JSONArray deligarray = jsonObject.getJSONArray("DelinquentList");
                for (int i=0; i<deligarray.length(); i++){
                    JSONObject jObj = (JSONObject) deligarray.get(i);
                    DelinquentList data = new DelinquentList(jObj);
                    mDelinquentList.add(data);
                }
            }else{
                DelinquentList data = new DelinquentList(json);
                mDelinquentList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            mAdValoremList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("AdValorem");
            if(json == null){
                JSONArray adValoremarray = jsonObject.getJSONArray("AdValorem");
                for (int i=0; i<adValoremarray.length(); i++){
                    JSONObject jObj = (JSONObject) adValoremarray.get(i);
                    AdValorem data = new AdValorem(jObj);
                    mAdValoremList.add(data);
                }
            }else{
                AdValorem data = new AdValorem(json);
                mAdValoremList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            mNonAdValoremList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("NonAdValorem");
            if(json == null){
                JSONArray nonAdvalarray = jsonObject.getJSONArray("NonAdValorem");
                for (int i=0; i<nonAdvalarray.length(); i++){
                    JSONObject jObj = (JSONObject) nonAdvalarray.get(i);
                    NonAdValorem data = new NonAdValorem(jObj);
                    mNonAdValoremList.add(data);
                }
            }else{
                NonAdValorem data = new NonAdValorem(json);
                mNonAdValoremList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            mInstallmentList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("InstallmentList");
            if(json == null){
                JSONArray installarray = jsonObject.getJSONArray("InstallmentList");
                for (int i=0; i<installarray.length(); i++){
                    JSONObject jObj = (JSONObject) installarray.get(i);
                    InstallmentList data = new InstallmentList(jObj);
                    mInstallmentList.add(data);
                }
            }else{
                InstallmentList data = new InstallmentList(json);
                mInstallmentList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject json = jsonObject.getJSONObject("DetailsList");
            mDetailsList = new DetailsList(json);
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject json = jsonObject.getJSONObject("TaxMiscList");
            taxMiscList = new TaxMiscList(json);
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            JSONObject json = jsonObject.getJSONObject("TaxBillStatusList");
            taxBillStatusList = new TaxBillStatusList(json);
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public TaxDetailInfo(Parcel in){
        mLeviedList = ParcelableUtils.readParcelableList(in, LeviedList.class.getClassLoader());
        mAdValoremList = ParcelableUtils.readParcelableList(in, AdValorem.class.getClassLoader());
        mNonAdValoremList = ParcelableUtils.readParcelableList(in, NonAdValorem.class.getClassLoader());
        mDelinquentList = ParcelableUtils.readParcelableList(in, DelinquentList.class.getClassLoader());
        mDueList = ParcelableUtils.readParcelableList(in, DueList.class.getClassLoader());
        mDetailsList = ParcelableUtils.readParcelable(in, DetailsList.class.getClassLoader());
        taxMiscList = ParcelableUtils.readParcelable(in, TaxMiscList.class.getClassLoader());
        taxBillStatusList = ParcelableUtils.readParcelable(in, TaxBillStatusList.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, mLeviedList, flags);
        ParcelableUtils.write(dest, mAdValoremList, flags);
        ParcelableUtils.write(dest, mNonAdValoremList, flags);
        ParcelableUtils.write(dest, mDelinquentList, flags);
        ParcelableUtils.write(dest, mDueList, flags);
        ParcelableUtils.write(dest, mDetailsList, flags);
        ParcelableUtils.write(dest, taxMiscList, flags);
        ParcelableUtils.write(dest, taxBillStatusList, flags);
    }

    public static final Creator<TaxDetailInfo> CREATOR = new Creator<TaxDetailInfo>() {
        @Override
        public TaxDetailInfo createFromParcel(Parcel source) {
            return new TaxDetailInfo(source);
        }

        @Override
        public TaxDetailInfo[] newArray(int size) {
            return new TaxDetailInfo[size];
        }
    };
}
