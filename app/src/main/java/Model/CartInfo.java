package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 14/09/15.
 */
public class CartInfo implements Parcelable {


    ShoppingCartList shoppingCartList;
    ArrayList<CartItems> CartItemsList;


    public CartInfo(JSONObject jsonObject){


        try{
            JSONObject json = jsonObject.getJSONObject("ShoppingCartList");
            shoppingCartList = new ShoppingCartList(json);
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            CartItemsList = new ArrayList<>() ;
            JSONObject json = jsonObject.optJSONObject("CartItems");
            if(json == null){
                JSONArray cartArray = jsonObject.getJSONArray("CartItems");
                for (int i=0; i<cartArray.length(); i++){
                    JSONObject jObj = (JSONObject) cartArray.get(i);
                    CartItems data = new CartItems(jObj);
                    CartItemsList.add(data);
                }
            }else{
                CartItems data = new CartItems(json);
                CartItemsList.add(data);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    public ShoppingCartList getShoppingCartList() {
        return shoppingCartList;
    }

    public void setShoppingCartList(ShoppingCartList shoppingCartList) {
        this.shoppingCartList = shoppingCartList;
    }

    public ArrayList<CartItems> getCartItemsList() {
        return CartItemsList;
    }

    public void setCartItemsList(ArrayList<CartItems> cartItemsList) {
        CartItemsList = cartItemsList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public CartInfo(Parcel in){
        shoppingCartList = ParcelableUtils.readParcelable(in, ShoppingCartList.class.getClassLoader());
        CartItemsList = ParcelableUtils.readParcelableList(in, CartItems.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, shoppingCartList, flags);
        ParcelableUtils.write(dest, CartItemsList, flags);

    }

    public static final Creator<CartInfo> CREATOR = new Creator<CartInfo>() {
        @Override
        public CartInfo createFromParcel(Parcel source) {
            return new CartInfo(source);
        }

        @Override
        public CartInfo[] newArray(int size) {
            return new CartInfo[size];
        }
    };
}
