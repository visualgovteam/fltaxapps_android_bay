package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class TaxMiscList implements Parcelable{

    String IsCertificate;

    public String getIsCertificate() {
        return IsCertificate;
    }

    public void setIsCertificate(String isCertificate) {
        IsCertificate = isCertificate;
    }

    public static Creator<TaxMiscList> getCREATOR() {
        return CREATOR;
    }

    public TaxMiscList(JSONObject jsonObject){
        try{
            IsCertificate = jsonObject.getString("IsCertificate");
            if(IsCertificate.equals("null")){
                IsCertificate = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public TaxMiscList(Parcel in){
        IsCertificate = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, IsCertificate);
    }

    public static final Creator<TaxMiscList> CREATOR = new Creator<TaxMiscList>() {
        @Override
        public TaxMiscList createFromParcel(Parcel source) {
            return new TaxMiscList(source);
        }

        @Override
        public TaxMiscList[] newArray(int size) {
            return new TaxMiscList[size];
        }
    };
}
