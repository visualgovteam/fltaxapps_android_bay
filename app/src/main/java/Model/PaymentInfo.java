package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 24/09/15.
 */
public class PaymentInfo implements Parcelable {

    String CartStatus;
    String InvoiceKey;
    String Message;
    String PaymentStatus;
    String ResponseCode;
    String TotalAmount;
    String TotalFeeAmount;
    String UpdateDate;

    public String getCartStatus() {
        return CartStatus;
    }

    public void setCartStatus(String cartStatus) {
        CartStatus = cartStatus;
    }

    public String getInvoiceKey() {
        return InvoiceKey;
    }

    public void setInvoiceKey(String invoiceKey) {
        InvoiceKey = invoiceKey;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    public String getTotalFeeAmount() {
        return TotalFeeAmount;
    }

    public void setTotalFeeAmount(String totalFeeAmount) {
        TotalFeeAmount = totalFeeAmount;
    }

    public String getUpdateDate() {
        return UpdateDate;
    }

    public void setUpdateDate(String updateDate) {
        UpdateDate = updateDate;
    }

    public PaymentInfo(JSONObject jsonObject){

        try{
            CartStatus = jsonObject.getString("CartStatus");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            InvoiceKey = jsonObject.getString("InvoiceKey");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            Message = jsonObject.getString("Message");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            PaymentStatus = jsonObject.getString("PaymentStatus");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            ResponseCode = jsonObject.getString("ResponseCode");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TotalAmount = jsonObject.getString("TotalAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            TotalFeeAmount = jsonObject.getString("TotalFeeAmount");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            UpdateDate = jsonObject.getString("UpdateDate");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public PaymentInfo(Parcel in){
        CartStatus = ParcelableUtils.readString(in);
        InvoiceKey = ParcelableUtils.readString(in);
        Message = ParcelableUtils.readString(in);
        PaymentStatus = ParcelableUtils.readString(in);
        ResponseCode = ParcelableUtils.readString(in);
        TotalAmount = ParcelableUtils.readString(in);
        TotalFeeAmount = ParcelableUtils.readString(in);
        UpdateDate = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, CartStatus);
        ParcelableUtils.write(dest, InvoiceKey);
        ParcelableUtils.write(dest, Message);
        ParcelableUtils.write(dest, PaymentStatus);
        ParcelableUtils.write(dest, ResponseCode);
        ParcelableUtils.write(dest, TotalAmount);
        ParcelableUtils.write(dest, TotalFeeAmount);
        ParcelableUtils.write(dest, UpdateDate);
    }

    public static final Creator<PaymentInfo> CREATOR = new Creator<PaymentInfo>() {
        @Override
        public PaymentInfo createFromParcel(Parcel source) {
            return new PaymentInfo(source);
        }

        @Override
        public PaymentInfo[] newArray(int size) {
            return new PaymentInfo[size];
        }
    };

}
