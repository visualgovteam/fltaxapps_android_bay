package Model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONException;
import org.json.JSONObject;

import utils.ParcelableUtils;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
public class DueList implements Parcelable {

    String DueAmount;
    String DueDate;
    String DueLiteral;

    public String getDueAmount() {
        return DueAmount;
    }

    public void setDueAmount(String dueAmount) {
        DueAmount = dueAmount;
    }

    public String getDueDate() {
        return DueDate;
    }

    public void setDueDate(String dueDate) {
        DueDate = dueDate;
    }

    public String getDueLiteral() {
        return DueLiteral;
    }

    public void setDueLiteral(String dueLiteral) {
        DueLiteral = dueLiteral;
    }

    public DueList(JSONObject jsonObject){
        try{
            DueAmount = jsonObject.getString("DueAmount");
            if(DueAmount.equals("null")){
                DueAmount = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            DueDate = jsonObject.getString("DueDate");
            if(DueDate.equals("null")){
                DueDate = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            DueLiteral = jsonObject.getString("DueLiteral");
            if(DueLiteral.equals("null")){
                DueLiteral = "";
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public DueList(Parcel in){
        DueAmount = ParcelableUtils.readString(in);
        DueDate = ParcelableUtils.readString(in);
        DueLiteral = ParcelableUtils.readString(in);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelableUtils.write(dest, DueAmount);
        ParcelableUtils.write(dest, DueDate);
        ParcelableUtils.write(dest, DueLiteral);
    }

    public static final Creator<DueList> CREATOR = new Creator<DueList>() {
        @Override
        public DueList createFromParcel(Parcel source) {
            return new DueList(source);
        }

        @Override
        public DueList[] newArray(int size) {
            return new DueList[size];
        }
    };
}
