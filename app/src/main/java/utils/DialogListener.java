package utils;

/**
 * Created by SaraschandraaM on 26/09/15.
 */
public interface DialogListener {

    public void onDialogShow();

    public void onDialogDismissed(String button);
}
