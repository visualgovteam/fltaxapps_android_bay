package utils;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SaraschandraaM on 18/08/15.
 */
public class HTTPHandler {

    public static HTTPHandler httpHandler = null;

    Context context;

    public HTTPHandler(Context context) {
        this.context = context;
    }

    public static HTTPHandler defaultHandler(Context context) {
        if (httpHandler == null) {
            httpHandler = new HTTPHandler(context);
        }
        return httpHandler;
    }

    public String doPost(String url, List<NameValuePair> params) {

        String json = "";
        InputStream is = null;

        try {

            HttpClient httpClient = new DefaultHttpClient();

            if (params == null)
                params = new ArrayList<NameValuePair>();

            Log.i("Url", url);
            for (NameValuePair nvp : params) {
                Log.i("Param", nvp.getName() + "=" + nvp.getValue());
            }
            HttpPost httpPost = new HttpPost(url);

            httpPost.setEntity(new UrlEncodedFormEntity(params));

            UrlEncodedFormEntity in = new UrlEncodedFormEntity(params);
            InputStreamReader isr = new InputStreamReader(in.getContent());
            BufferedReader reader = new BufferedReader(isr);

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            return null;
        }
        return json;
    }

    public String doGet(String url, List<NameValuePair> params) {

        String json = "";
        InputStream is = null;

        try {

            HttpClient httpClient = new DefaultHttpClient();

//            Log.i("URL", url);

            if (params == null)
                params = new ArrayList<NameValuePair>();

            for (NameValuePair nvp : params) {
//                Log.i("Param", nvp.getName() + "=" + nvp.getValue());
            }

            if (params != null) {
                String paramString = URLEncodedUtils.format(params, "utf-8");
                url += "?" + paramString;
            }

            Log.i("REQUEST URL", url);

            HttpGet httpGet = new HttpGet(url);
            HttpResponse httpResponse = httpClient.execute(httpGet);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            json = sb.toString();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
            return null;
        }

        return json;

    }
}
