package utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.CheckBox;

import com.visualgov.baytaxCollector.R;
import com.visualgov.baytaxCollector.SearchFragmentActivity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;

import Model.AdminSettingsList;
import Model.CartItems;
import Model.RollType;
import Model.ShoppingCartList;
import Model.Year;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
public class Config {

    public static String VGPRODID = "5";

    public static String URL_GETADMINSETTING = "http://mobiledata.visualgov.com/FLTaxAPI/AdminSetting/AdminSettingsInfo";
    public static String URL_GETROLLTYPE = "http://mobiledata.visualgov.com/FLTaxAPI/AdminSetting/RollTypeInfo";
    public static String URL_GETYEARSEARCHURL = "http://mobiledata.visualgov.com/FLTaxAPI/AdminSetting/RollYearInfo";
    public static String URL_TAXSEARCH = "http://mobiledata.visualgov.com/FLTaxAPI/TaxSearch/SearchInfo";
    public static String URL_TAXDETAIL = "http://mobiledata.visualgov.com/FLTaxAPI/TaxDetail/PropInfo";
    public static String URL_ADDTOCART = "http://mobiledata.visualgov.com/FLTaxAPI/TaxPayment/AddToCart";
    public static String URL_REMOVEFROMCART = "http://mobiledata.visualgov.com/FLTaxAPI/TaxPayment/RemoveItemFromCart";
    public static String URL_SUBMITCREDITCARD = "http://mobiledata.visualgov.com/FLTaxAPI/TaxPayment/SubmitPaymentCredit";
    public static String URL_SUBMITECHECK = "http://mobiledata.visualgov.com/FLTaxAPI/TaxPayment/SubmitPaymentCheck";
    public static String URL_GETCARTINFO = "http://mobiledata.visualgov.com/FLTaxAPI/TaxPayment/GetCartInfo";


    public static boolean isFromQR = false;
    public static boolean isCreditCard = false;
    public static boolean isCreditCardAllowed = false;
    public static boolean isECheckAllowed = false;
    public static boolean isInstalmentPayment = false;
    public static boolean isDeliquentPayment = false;
    public static boolean isPayNowClicked = false;
    public static boolean isSiteEnabled = false;
    public static AdminSettingsList adminSettingsList;
    public static String InvoiceKey = "";
    public static ArrayList<Year> yearList = new ArrayList<>();
    public static ArrayList<RollType> rollTypeList = new ArrayList<>();
    public static ArrayList<CartItems> cartItems = new ArrayList<>();
    public static ShoppingCartList shoppingCartItems = new ShoppingCartList();
    public static int installmentCount = 0;


    public static boolean isTablet(Context context) {

        return context.getResources().getBoolean(R.bool.isTablet);

    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showNetworkAlert(final Context context) {
        AlertDialog.Builder mNetworkAlert = new AlertDialog.Builder(context);
        mNetworkAlert.setTitle(context.getResources().getString(R.string.alert_networktitle));
        mNetworkAlert.setMessage(context.getResources().getString(R.string.alert_nonetwork));
        mNetworkAlert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ;
            }
        });
    }

    public static String formatCurrency(String data) {
        String amount = "";
        data = data.replaceAll("\\$", "");
        data = data.replaceAll("\\,", "");
        if (!data.equals("") || data != null || !data.equals("null")) {
            if (data.equals("0") || data.equals("0.00")) {
                data = "$0.00";
            } else {
                double voltax = Double.parseDouble(data);
                DecimalFormat formatter = new DecimalFormat("#,###,###.00");
                amount = formatter.format(voltax);
                data = "$" + amount;
            }
        }
        return data;
    }


}
