package utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.visualgov.baytaxCollector.R;

/**
 * Created by SaraschandraaM on 22/08/15.
 */
public class WSLoadingDialog {

    Dialog wsLoadingDialog;

    public WSLoadingDialog(Context context) {
        wsLoadingDialog = new Dialog(context, R.style.AppTheme_Base);
        View loadingView = LayoutInflater.from(context).inflate(R.layout.layout_loading_dialog, null);
        wsLoadingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        wsLoadingDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        wsLoadingDialog.setContentView(loadingView);
        ImageView progressbar = (ImageView) loadingView.findViewById(R.id.iv_progressbar);
        final AnimationDrawable progressbaranim = (AnimationDrawable) context.getResources().getDrawable(
                R.drawable.progress_anim);
        progressbar.setBackgroundDrawable(progressbaranim);
        progressbar.post(new Runnable() {

            @Override
            public void run() {
                if (progressbaranim != null)
                    progressbaranim.start();

            }
        });
    }

    public  void showWSLoadingDialog(){
        wsLoadingDialog.show();
    }

    public void hideWSLoadingDialog(){
        wsLoadingDialog.dismiss();
    }
}
