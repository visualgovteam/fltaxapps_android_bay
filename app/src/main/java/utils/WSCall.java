package utils;

import android.content.Context;

import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
public class WSCall {

    Context context;
    WebServiceUtil webServiceUtil;
    public WSCall(Context context){
        this.context = context;
    }


    public void getAdminSettingsInfo(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void getRollTypeDetails(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void getRollYearDetails(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void getTaxSearchInfo(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }


    public void getTaxDetailInfo(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void setAddToCart(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void removeItemFromCart(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void payCreditCard(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }

    public void getCartInfo(String URL, List<NameValuePair> params, WSListener listener){
        webServiceUtil = new WebServiceUtil(context, URL, params, listener);
        webServiceUtil.execute();
    }
}
