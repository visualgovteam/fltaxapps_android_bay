package utils;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visualgov.baytaxCollector.R;

/**
 * Created by SaraschandraaM on 26/09/15.
 */
public class DialogView {

    Context context;
    String title;
    String message;
    String posbtn;
    String negbtn;
    DialogListener dialogListener;
    Dialog mDialog;
    View mDialogView;

    public DialogView (Context context, String title, String message, String posbtn, String negbtn, final DialogListener dialogListener){
        this.context = context;
        this.title = title;
        this.message = message;
        this.posbtn = posbtn;
        this.negbtn = negbtn;
        this.dialogListener = dialogListener;

        mDialog = new Dialog(context, R.style.AppTheme_Base);
        mDialogView = LayoutInflater.from(context).inflate(R.layout.layout_dialog, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setCancelable(false);
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setContentView(mDialogView);


        TextView tvTitle = (TextView) mDialogView.findViewById(R.id.tv_dialog_title);
        TextView tvMessage = (TextView) mDialogView.findViewById(R.id.tv_dialog_message);
        TextView tvPosButton = (TextView) mDialogView.findViewById(R.id.tv_dialog_posbtn);
        TextView tvNegButton = (TextView) mDialogView.findViewById(R.id.tv_dialog_negbtn);

        LinearLayout btnPos = (LinearLayout) mDialogView.findViewById(R.id.ll_posbtn);
        LinearLayout btnNeg = (LinearLayout) mDialogView.findViewById(R.id.ll_negbtn);

        if(negbtn == null){
            btnNeg.setVisibility(View.GONE);
        }else{
            btnNeg.setVisibility(View.VISIBLE);
            tvNegButton.setText(negbtn);
        }

        tvTitle.setText(title);
        tvMessage.setText(Html.fromHtml(message));
        tvPosButton.setText(posbtn);

        btnPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                dialogListener.onDialogDismissed("Positive");
            }
        });

        btnNeg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                dialogListener.onDialogDismissed("Negative");
            }
        });

        mDialog.show();
        dialogListener.onDialogShow();
    }
}
