package utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.visualgov.baytaxCollector.SearchFragmentActivity;

import org.apache.http.NameValuePair;

import java.util.List;

/**
 * Created by SaraschandraaM on 18/08/15.
 */
public class WebServiceUtil extends AsyncTask<String, Void, String> {

    HTTPHandler handler;
    String url;
    List<NameValuePair> param;
    Context context;
    WSListener listener;
    Exception exception;
    String response = null;

    public WebServiceUtil(Context context, String url, List<NameValuePair> params, WSListener listener) {
        this.url = url;
        this.param = params;
        this.context = context;
        this.listener = listener;
    }


    @Override
    protected String doInBackground(String... params) {
        handler = new HTTPHandler(context);

        try {
            response = handler.doGet(url, param);
            System.out.println("Response:" + response);
            return response;
        } catch (Exception e) {
            Log.e("WS Exception", e.getMessage());
            exception = e;
        }
        return response;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (listener != null) {
            listener.onRequestSent();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (s == null || s.isEmpty()) {
            DialogView dialogView = new DialogView(context, "Server Unavailable", "The Service is currently unavailable please try again later", "OK", null, new DialogListener() {
                @Override
                public void onDialogShow() {

                }

                @Override
                public void onDialogDismissed(String button) {
                    if(SearchFragmentActivity.defaultInstance() != null){
                        SearchFragmentActivity.defaultInstance().homebuttonPressed();
                    }
                }
            });
        } else {
            if (listener != null) {
                listener.onRequestReceived(s, exception);
            }
        }
    }
}
