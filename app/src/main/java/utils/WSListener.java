package utils;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
public interface WSListener {

    public void onRequestSent();

    public void onRequestReceived(String response, Exception exception);
}
