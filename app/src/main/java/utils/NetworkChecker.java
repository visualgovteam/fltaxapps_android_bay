package utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by SaraschandraaM on 07/09/15.
 */
public class NetworkChecker extends BroadcastReceiver {


    public static int DISCONNECTED = 0;
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    @Override
    public void onReceive(Context context, Intent intent) {
        boolean status = false;
        String network = "";
        int conn = 0;
        System.out.println("connection receiver");
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI){
                conn = TYPE_WIFI;
            }

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE){
                conn = TYPE_MOBILE;
            }

        }

        if (conn == NetworkChecker.TYPE_WIFI) {
            status = true;
            network = "CONNECTED";
        } else if (conn == NetworkChecker.TYPE_MOBILE) {
            status = true;
            network = "CONNECTED";
        } else if (conn == NetworkChecker.DISCONNECTED) {
            status = false;
            network = "DISCONNECTED";
        }
        System.out.println("connection receiver status: " + status);
        Log.i("NETWORK STATUS", network);

        Intent networkIntent = new Intent("NetworkStatus");
        networkIntent.putExtra("IsNetAvailable", status);
        LocalBroadcastManager.getInstance(context).sendBroadcast(networkIntent);
    }
}
