package com.visualgov.baytaxCollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.CartItems;
import Model.PaymentInfo;
import Model.ShoppingCartList;
import Model.StateList;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
@EFragment(R.layout.fragment_paymentecheck)
public class PaymentECheckFragment extends Fragment implements View.OnClickListener {

    @ViewById(R.id.tv_ecart_taxamt)
    TextView mTvCartAmount;

    @ViewById(R.id.tv_ecart_convfee)
    TextView mTvConvAmount;

    @ViewById(R.id.tv_ecart_totalamt)
    TextView mTvTotalAmount;

    @ViewById(R.id.et_ecart_firstname)
    EditText mEtFirstName;

    @ViewById(R.id.et_ecart_lastname)
    EditText mEtLastName;

    @ViewById(R.id.et_ecart_streetaddr1)
    EditText mEtStreetAddress1;

    @ViewById(R.id.et_ecart_streetaddr2)
    EditText mEtStreetAddress2;

    @ViewById(R.id.et_ecart_city)
    EditText mEtCity;

    @ViewById(R.id.et_ecart_state)
    EditText mEtState;

    @ViewById(R.id.et_ecart_zip)
    EditText mEtZip;

    @ViewById(R.id.et_ecart_phoneno)
    EditText mEtPhoneno;

    @ViewById(R.id.et_ecart_emaild)
    EditText mEtEmailId;

    @ViewById(R.id.et_bankname)
    EditText mEtBankName;

    @ViewById(R.id.et_eaccno)
    EditText mEtAccno;

    @ViewById(R.id.et_routingno)
    EditText mEtRoutingNo;

    @ViewById(R.id.et_chkno)
    EditText mEtCheckNo;


    @ViewById(R.id.chk_terms)
    CheckBox mChkTerms;


    Bundle arguments;
    ArrayList<CartItems> cartItemsList;
    ShoppingCartList shoppingCartList;

    String sFirstName, sLastName, sStreetAddr1, sStreetAddr2 = "", sCity, sState, sZip, sPhoneno, sEmailID,
            sBankName, sAccNo, sRoutingNo, sCheckNo, sTaxAmount, sConvFee, sTotalAmount, mInvoiceKey, sStateCode;

    boolean isTermsChecked = false, isStateSelected = false;
    int stateSelectedPos;
    ArrayList<StateList> stateList;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            cartItemsList = arguments.getParcelableArrayList("CartItemslist");
            shoppingCartList = arguments.getParcelable("ShoppingCartItem");
            mInvoiceKey = arguments.getString("InvoiceKey");
            sTaxAmount = Config.formatCurrency(shoppingCartList.getTotalAmount());
            sConvFee = Config.formatCurrency(shoppingCartList.getTotalFeeAmount());
            sTotalAmount = Config.formatCurrency(shoppingCartList.getGrandTotalAmount());
            mTvCartAmount.setText(sTaxAmount);
            mTvConvAmount.setText(sConvFee);
            mTvTotalAmount.setText(sTotalAmount);

            stateList = new ArrayList<>();
            String[] statename = getActivity().getResources().getStringArray(R.array.state_names);
            String[] statecode = getActivity().getResources().getStringArray(R.array.state_codes);
            for (int i = 0; i < statename.length; i++) {
                StateList state = new StateList();
                state.setStateCode(statecode[i]);
                state.setStateName(statename[i]);
                stateList.add(state);
            }
        }
        registerForContextMenu(mEtState);
        mEtState.setOnClickListener(this);
        mChkTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "I understand my payment of " + "<b>" + sTaxAmount + "</b>" + " will be charged along with a convenience fee of " + "<b>" + sConvFee + "</b>" + " by the service provider  <b>KM Solutions</b>.<br/><br/> If you agree with the above charges select I Agree";
                DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
                    @Override
                    public void onDialogShow() {

                    }

                    @Override
                    public void onDialogDismissed(String button) {
                        if (button.equals("Positive")) {
                            isTermsChecked = true;
                            mChkTerms.setChecked(true);
                        } else {
                            isTermsChecked = false;
                            mChkTerms.setChecked(false);
                        }
                    }
                });
            }
        });
    }

    @Click(R.id.tv_cart_terms)
    void onTermsConditionsClicked() {
        String message = "I understand my payment of " + "<b>" + sTaxAmount + "</b>" + " will be charged along with a convenience fee of " + "<b>" + sConvFee + "</b>" + " by the service provider  <b>KM Solutions</b>.<br/><br/>If you agree with the above charges select I Agree";
        DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
            @Override
            public void onDialogShow() {

            }

            @Override
            public void onDialogDismissed(String button) {
                if (button.equals("Positive")) {
                    isTermsChecked = true;
                    mChkTerms.setChecked(true);
                } else {
                    isTermsChecked = false;
                    mChkTerms.setChecked(false);
                }
            }
        });
    }

    @Click(R.id.btn_ecartaddr_paynow)
    void onPayNowClicked() {
        sFirstName = mEtFirstName.getText().toString();
        sLastName = mEtLastName.getText().toString();
        sStreetAddr1 = mEtStreetAddress1.getText().toString();
        sStreetAddr2 = mEtStreetAddress2.getText().toString();
        sCity = mEtCity.getText().toString();
        sState = mEtState.getText().toString();
        sZip = mEtZip.getText().toString();
        sPhoneno = mEtPhoneno.getText().toString();
        sEmailID = mEtEmailId.getText().toString();
        sBankName = mEtBankName.getText().toString();
        sAccNo = mEtAccno.getText().toString();
        sRoutingNo = mEtRoutingNo.getText().toString();
        sCheckNo = mEtCheckNo.getText().toString();

        if (sState.isEmpty() || sCity.isEmpty() || sStreetAddr1.isEmpty() || sZip.isEmpty() || sPhoneno.isEmpty() || sEmailID.isEmpty() ||
                sBankName.isEmpty() || sAccNo.isEmpty() || sCheckNo.isEmpty() || sRoutingNo.isEmpty()
                || sFirstName.isEmpty() || sLastName.isEmpty()) {
            showEmptyAlert("Please enter all the values");
        } else if (!isTermsChecked) {
            showEmptyAlert("Please click on the agreement to proceed with the payment");
        } else {
            final WSLoadingDialog loadingDialog = new WSLoadingDialog(getActivity());
            WSCall wsCall = new WSCall(getActivity());
            List<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
            list.add(new BasicNameValuePair("Address1", sStreetAddr1));
            list.add(new BasicNameValuePair("Address2", sStreetAddr2));
            list.add(new BasicNameValuePair("City", sCity));
            list.add(new BasicNameValuePair("State", sState));
            list.add(new BasicNameValuePair("Zip", sZip));
            list.add(new BasicNameValuePair("FirstName", sFirstName));
            list.add(new BasicNameValuePair("LastName", sLastName));
            list.add(new BasicNameValuePair("PhoneNumber", sPhoneno));
            list.add(new BasicNameValuePair("Email", sEmailID));
            list.add(new BasicNameValuePair("BankName", sBankName));
            list.add(new BasicNameValuePair("AccountNumber", sAccNo));
            list.add(new BasicNameValuePair("RoutingNumber", sRoutingNo));
            list.add(new BasicNameValuePair("CheckNumber", sCheckNo));
            list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));

            wsCall.payCreditCard(Config.URL_SUBMITECHECK, list, new WSListener() {
                @Override
                public void onRequestSent() {
                    loadingDialog.showWSLoadingDialog();
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    loadingDialog.hideWSLoadingDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject json = jsonObject.getJSONObject("FLTax");
                        JSONObject resultjson = json.getJSONObject("PaymentInfo");
                        PaymentInfo data = new PaymentInfo(resultjson);
                        Bundle args = new Bundle();
                        args.putString("email", sEmailID);
                        args.putString("invoicekey", Config.InvoiceKey);
                        args.putParcelable("PaymentInfo", data);
                        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.PAYMENT_SUCCESS, args);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


        }
    }


    BroadcastReceiver mTermsConditionsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                isTermsChecked = intent.getBooleanExtra("TermsConditions", false);
                mChkTerms.setChecked(isTermsChecked);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_echeck));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mTermsConditionsReceiver, new IntentFilter("Terms"));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTermsConditionsReceiver);
    }

    private void showEmptyAlert(String message) {
        DialogView dialogView = new DialogView(getActivity(), "Information", message, "Ok", null, new DialogListener() {
            @Override
            public void onDialogShow() {

            }

            @Override
            public void onDialogDismissed(String button) {

            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        LayoutInflater headerInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);


        if (v.getId() == R.id.et_ecart_state) {
            title.setText(getActivity().getResources().getString(R.string.screen_state));
            menu.setHeaderView(header);
            for (int i = 0; i < stateList.size(); i++) {
                menu.add(3, i, Menu.NONE, stateList.get(i).getStateName());
                menu.setGroupCheckable(3, true, true);
            }
            if (isStateSelected) {
                MenuItem menuItem = menu.getItem(stateSelectedPos);
                menuItem.setChecked(true);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = item.getItemId();

        if (item.getGroupId() == 3) {
            sState = stateList.get(position).getStateName();
            mEtState.setText(sState);
            sStateCode = stateList.get(position).getStateCode();
            isStateSelected = true;
            stateSelectedPos = position;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.et_ecart_state) {
            this.getActivity().openContextMenu(v);
        }
    }
}
