package com.visualgov.baytaxCollector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by SaraschandraaM on 27/08/15.
 */
@EFragment(R.layout.fragment_paymentaddress)
public class PaymentAddressFragment extends Fragment {


    @AfterViews
    void onViewLoaded(){

    }

    @Click(R.id.btn_cartaddr_paynow)
    void onCartAddrPayNowClicked(){
        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.PAYMENT_CREDIT, null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
