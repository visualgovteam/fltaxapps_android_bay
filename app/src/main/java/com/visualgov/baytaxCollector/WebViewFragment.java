package com.visualgov.baytaxCollector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by SaraschandraaM on 29/09/15.
 */

public class WebViewFragment extends Fragment {


    WebView pdfview;

    Bundle arguments;
    String LinkTo;
    View mPDFView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        mPDFView = inflater.inflate(R.layout.fragment_webview, container, false);
        pdfview = (WebView) mPDFView.findViewById(R.id.webview_pdf);
        arguments = getArguments();
        if (arguments != null) {
            LinkTo = arguments.getString("Path");
        }
        pdfview = new WebView(getActivity());
        pdfview.getSettings().setJavaScriptEnabled(true);
        pdfview.getSettings().setLoadWithOverviewMode(true);
        pdfview.getSettings().setUseWideViewPort(true);
        pdfview.loadUrl("https://docs.google.com/gview?embedded=true&url=" + LinkTo);
        return mPDFView;
    }
}
