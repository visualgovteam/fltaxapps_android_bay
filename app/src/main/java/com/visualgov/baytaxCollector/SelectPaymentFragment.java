package com.visualgov.baytaxCollector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.AdminSettingsList;
import Model.CartInfo;
import Model.CartItems;
import Model.ShoppingCartList;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 27/08/15.
 */
@EFragment(R.layout.fragment_selectpayment)
public class SelectPaymentFragment extends Fragment {


    Bundle arguments;
    ArrayList<CartItems> cartItemsList;
    ShoppingCartList shoppingCartList;
    String mInvoiceKey;
    WSCall wsCall;
    WSLoadingDialog wsLoadingDialog;

    @ViewById(R.id.ll_creditcard)
    LinearLayout mCreditCardLayout;

    @ViewById(R.id.ll_echeck)
    LinearLayout mECheckLayout;

    @ViewById(R.id.ll_googlewallet)
    LinearLayout mGoogleWalletLayout;

    @ViewById(R.id.tv_echeck_unavail)
    TextView mTvECheckunavailable;

    @ViewById(R.id.tv_credit_unavail)
    TextView mTvCreditCardunavailable;

    @ViewById(R.id.tv_googlewallet_unavail)
    TextView mTvGoogleWalletunavailable;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        mInvoiceKey = arguments.getString("InvoiceKey");
        cartItemsList = new ArrayList<>();
        shoppingCartList = new ShoppingCartList();
        wsCall = new WSCall(getActivity());
        wsLoadingDialog = new WSLoadingDialog(getActivity());

        if (!Config.isECheckAllowed || !Config.isSiteEnabled) {
            mTvECheckunavailable.setVisibility(View.VISIBLE);
            mECheckLayout.setClickable(false);
        } else {
            mTvECheckunavailable.setVisibility(View.GONE);
            mECheckLayout.setClickable(true);
        }

        if (!Config.isCreditCardAllowed || !Config.isSiteEnabled) {
            mTvCreditCardunavailable.setVisibility(View.VISIBLE);
            mTvGoogleWalletunavailable.setVisibility(View.VISIBLE);
            mCreditCardLayout.setClickable(false);
            mGoogleWalletLayout.setClickable(false);
        } else {
            mTvCreditCardunavailable.setVisibility(View.GONE);
            mTvGoogleWalletunavailable.setVisibility(View.GONE);
            mCreditCardLayout.setClickable(true);
            mGoogleWalletLayout.setClickable(true);
        }


    }

    @Click(R.id.ll_creditcard)
    void onCreditCardClicked() {
        Config.isCreditCard = true;
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("MOPType", "CREDIT"));
        wsCall.getCartInfo(Config.URL_GETCARTINFO, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject json = jsonObject.getJSONObject("FLTax");
                        CartInfo data = new CartInfo(json);
                        cartItemsList = data.getCartItemsList();
                        shoppingCartList = data.getShoppingCartList();
                        final Bundle args = new Bundle();
                        args.putString("InvoiceKey", Config.InvoiceKey);
                        args.putParcelable("ShoppingCartItem", shoppingCartList);
                        args.putParcelableArrayList("CartItemslist", cartItemsList);
                        String totalamount = Config.formatCurrency(shoppingCartList.getTotalAmount());
                        String convamount = Config.formatCurrency(shoppingCartList.getTotalFeeAmount());
                        String message = "I understand my payment of " + "<b>" + totalamount + "</b>" + " will be charged along with a convenience fee of " + "<b>" + convamount + "</b>" + " by the service provider  <b>Visual Gov Solutions</b>.<br/> <br/> If you agree with the above charges select I Agree";
                        DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
                            @Override
                            public void onDialogShow() {

                            }

                            @Override
                            public void onDialogDismissed(String button) {
                                if (button.equals("Positive")) {
                                    SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.PAYMENT_CREDIT, args);
                                }
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Click(R.id.ll_echeck)
    void onECheckClicked() {
        Config.isCreditCard = false;
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("MOPType", "CHECK"));
        wsCall.getCartInfo(Config.URL_GETCARTINFO, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject json = jsonObject.getJSONObject("FLTax");
                        CartInfo data = new CartInfo(json);
                        cartItemsList = data.getCartItemsList();
                        shoppingCartList = data.getShoppingCartList();
                        final Bundle args = new Bundle();
                        args.putString("InvoiceKey", Config.InvoiceKey);
                        args.putParcelable("ShoppingCartItem", shoppingCartList);
                        args.putParcelableArrayList("CartItemslist", cartItemsList);
                        String totalamount = Config.formatCurrency(shoppingCartList.getTotalAmount());
                        String convamount = Config.formatCurrency(shoppingCartList.getTotalFeeAmount());
                        String message = "I understand my payment of " + "<b>" + totalamount + "</b>" + " will be charged along with a convenience fee of " + "<b>" + convamount + "</b>" + " by the service provider <b> KM Solutions </b>.<br/> <br/> If you agree with the above charges select I Agree";
                        DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
                            @Override
                            public void onDialogShow() {

                            }

                            @Override
                            public void onDialogDismissed(String button) {
                                if (button.equals("Positive")) {
                                    SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.PAYMENT_ECHECK, args);
                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Click(R.id.ll_googlewallet)
    void onGoogleWallet(){
        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_googlepay), Toast.LENGTH_SHORT).show();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.GONE);
        SearchFragmentActivity.defaultInstance().ll_search.setVisibility(View.GONE);
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_paymode));
    }

    @Override
    public void onPause() {
        super.onPause();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_cartlist));
    }
}
