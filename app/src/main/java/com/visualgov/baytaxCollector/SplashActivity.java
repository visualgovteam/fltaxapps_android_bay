package com.visualgov.baytaxCollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.AdminSettingsList;
import Model.RollTypeList;
import Model.YearList;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @ViewById(R.id.iv_splashtitle)
    ImageView ivLoginSplashImage;

    @ViewById(R.id.iv_splash)
    ImageView ivSplashImage;

    @ViewById(R.id.rl_splash)
    RelativeLayout splashLayout;

    @ViewById(R.id.rl_login)
    RelativeLayout loginLayout;

    @ViewById(R.id.btn_searchdetails)
    RelativeLayout rlSearchDetails;

    Handler splashHandler;

    WSCall wsCall;
    List<NameValuePair> list;

    WSLoadingDialog wsLoadingDialog;
    Intent splashIntent;
    public boolean isTablet;
    Animation animation;
    String sSiteMessage, sSiteEnabled;
    boolean isNetWorkAvailable, isAppInit = false, isSiteEnabled;

    @Click(R.id.btn_searchqrcode)
    void onSearchQRClicked() {
        Config.isFromQR = true;
        splashIntent = new Intent(SplashActivity.this, SearchFragmentActivity_.class);
        splashIntent.putExtra("SearchType", 1);
        startActivity(splashIntent);
    }

    @Click(R.id.btn_searchdetails)
    void onSearchDetailsClicked() {
        Config.isFromQR = false;
        list = new ArrayList<>();
        list.add(new BasicNameValuePair("VGProdId", Config.VGPRODID));

        wsCall.getAdminSettingsInfo(Config.URL_GETADMINSETTING, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                if (response != null) {
                    try {
                        JSONObject jObject = new JSONObject(response);
                        JSONObject json = jObject.getJSONObject("FLTaxSettings");
                        JSONObject jsonObject = json.getJSONObject("AdminSettingsList");
                        Config.adminSettingsList = new AdminSettingsList(jsonObject);
                        if (Config.adminSettingsList.getCreditCardEnabled().equals("True")) {
                            Config.isCreditCardAllowed = true;
                        } else if (Config.adminSettingsList.getCreditCardEnabled().equals("False")) {
                            Config.isCreditCardAllowed = false;
                        } else {
                            Config.isCreditCardAllowed = false;
                        }

                        sSiteMessage = Config.adminSettingsList.getSiteMessage();
                        sSiteEnabled = Config.adminSettingsList.getSiteEnabled();

                        if (sSiteEnabled.equals("False")) {
                            isSiteEnabled = false;
                        } else if (sSiteEnabled.equals("True")) {
                            isSiteEnabled = true;
                        }


                        if (Config.adminSettingsList.getCheckEnabled().equals("True")) {
                            Config.isECheckAllowed = true;
                        } else if (Config.adminSettingsList.getCheckEnabled().equals("False")) {
                            Config.isECheckAllowed = false;
                        } else {
                            Config.isECheckAllowed = false;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (isSiteEnabled) {
                        getRollType();
                    } else {
                        wsLoadingDialog.hideWSLoadingDialog();
                        DialogView dialogView = new DialogView(SplashActivity.this, "Server Unavailable", "The Service is currently unavailable please try again later", "OK", null, new DialogListener() {
                            @Override
                            public void onDialogShow() {

                            }

                            @Override
                            public void onDialogDismissed(String button) {

                            }
                        });
                    }
                }
            }
        });

    }

    private void getRollType() {
        wsCall.getRollTypeDetails(Config.URL_GETROLLTYPE, list, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                if (response != null) {
                    try {
                        JSONObject jObject = new JSONObject(response);
                        JSONObject json = jObject.getJSONObject("FLTaxSettings");
                        RollTypeList rollTypeList = new RollTypeList(json);
                        Config.rollTypeList = rollTypeList.getmRollTypeList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    getRollYear();
                }
            }
        });
    }


    private void getRollYear() {
        wsCall.getRollYearDetails(Config.URL_GETYEARSEARCHURL, list, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                if (response != null) {
                    try {
                        JSONObject jObject = new JSONObject(response);
                        JSONObject json = jObject.getJSONObject("FLTaxSettings");
                        YearList yearList = new YearList(json);
                        Config.yearList = yearList.getmYearList();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    wsLoadingDialog.hideWSLoadingDialog();
                    splashIntent = new Intent(SplashActivity.this, SearchFragmentActivity_.class);
                    splashIntent.putExtra("SearchType", 0);
                    startActivity(splashIntent);
                }
            }
        });
    }


    @AfterViews
    void onViewLoaded() {
        if (isNetWorkAvailable) {
            wsCall = new WSCall(SplashActivity.this);
            wsLoadingDialog = new WSLoadingDialog(SplashActivity.this);
            loginLayout.setVisibility(View.GONE);
            splashLayout.setVisibility(View.VISIBLE);
            splashHandler = new Handler();
            splashHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    animateSplash();
                }
            }, 1000);
        }
    }


    private void animateSplash() {
        animation = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.image_anim);
        animation.reset();
        animation.setFillAfter(true);
        ivSplashImage.clearAnimation();
        ivSplashImage.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashLayout.setVisibility(View.GONE);
                loginLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void showNetworkAlert() {
        AlertDialog.Builder mNetworkAlert = new AlertDialog.Builder(SplashActivity.this);
        mNetworkAlert.setCancelable(false);
        mNetworkAlert.setTitle(getResources().getString(R.string.alert_networktitle));
        mNetworkAlert.setMessage(getResources().getString(R.string.alert_nonetwork));
        mNetworkAlert.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        mNetworkAlert.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTablet = Config.isTablet(SplashActivity.this);
        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        isNetWorkAvailable = Config.isNetworkAvailable(SplashActivity.this);
        if (!isNetWorkAvailable) {
            showNetworkAlert();
        } else if (!isAppInit) {
            isAppInit = true;
            onViewLoaded();
        }
    }
}
