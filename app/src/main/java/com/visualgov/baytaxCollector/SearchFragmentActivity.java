package com.visualgov.baytaxCollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.CartItemList;
import utils.Config;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
@EActivity(R.layout.activity_search)
public class SearchFragmentActivity extends AppCompatActivity {


    public static enum Fragments {
        SEARCH_SCREEN, SEARCH_RESULT, TAX_DETAIL, DIALOG_FRAG, QR_SCAN, CART_INFO, SELECT_PAYMENT, PAYMENT_ADDRESS, PAYMENT_CREDIT, PAYMENT_ECHECK, PAYMENT_SUCCESS,
        MAP_VIEW, PDF_VIEW
    }


    public static SearchFragmentActivity rootInstance;

    public static SearchFragmentActivity defaultInstance() {
        return rootInstance;
    }

    public boolean isTablet;

    @ViewById(R.id.tl_apptoolbar)
    Toolbar mAppToolbar;

    @ViewById(R.id.tv_toolbarheader)
    public TextView mToolbarTitle;

    @ViewById(R.id.fl_fragmentHolder)
    FrameLayout fragmentHolder;

    @ViewById(R.id.ll_closebtn)
    LinearLayout ll_btn_close;

    @ViewById(R.id.ll_cartbtn)
    public RelativeLayout ll_cart_btn;

    @ViewById(R.id.ll_homebutton)
    LinearLayout ll_homebutton;

    @ViewById(R.id.tv_photo_count)
    TextView mTvCartList;

    @ViewById(R.id.ll_search)
    public LinearLayout ll_search;

    @ViewById(R.id.ll_rootLayout)
    LinearLayout rootLayout;

    FragmentTransaction fragmentTransaction;

    SearchScreenFragment searchScreenFragment;
    SearchResultFragment searchResultFragment;
    TaxDetailFragment taxDetailFragment;
    DialogFragment dialogFragment;
    QRScanFragment qrScanFragment;
    CartInfoFragment cartInfoFragment;
    SelectPaymentFragment selectPaymentFragment;
    PaymentAddressFragment paymentAddressFragment;
    PaymentCreditCardFragment paymentCreditCardFragment;
    PaymentECheckFragment paymentECheckFragment;
    PaymentSuccessFragment paymentSuccessFragment;
    MapViewFragment mapViewFragment;
    WebViewFragment webViewFragment;

    public ArrayList<Fragments> fragmentsList;
    public Fragments viewingFragment;

    int screentype;
    AlertDialog.Builder mNetworkAlert, mServerUnAvailAlert;
    boolean isNetworkAvailable;
    public int mToolbarHeight, mToolbarWidth;


    @AfterViews
    void onViewLoaded() {
        setSupportActionBar(mAppToolbar);
        fragmentsList = new ArrayList<>();
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        screentype = getIntent().getExtras().getInt("SearchType");
        if (screentype == 0) {
            changeFragment(Fragments.SEARCH_SCREEN, null);
        } else if (screentype == 1) {
            changeFragment(Fragments.QR_SCAN, null);
        }
        ViewTreeObserver vto = mAppToolbar.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mAppToolbar.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                mToolbarHeight = mAppToolbar.getMeasuredHeight();

            }
        });

        ViewTreeObserver cartbutton = ll_cart_btn.getViewTreeObserver();
        cartbutton.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ll_cart_btn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                mToolbarWidth = ll_cart_btn.getMeasuredWidth();

            }
        });
    }

    BroadcastReceiver cartdataReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Config.cartItems != null && Config.cartItems.size() > 0) {
                mTvCartList.setVisibility(View.VISIBLE);
                mTvCartList.setText("" + Config.cartItems.size());
            } else {
                mTvCartList.setVisibility(View.INVISIBLE);
                onBackPressed();
            }
        }
    };

    BroadcastReceiver networkReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            isNetworkAvailable = intent.getBooleanExtra("IsNetAvailable", false);
            if (!isNetworkAvailable) {
                showNetworkAlert();
            }
        }
    };


    @Click(R.id.ll_closebtn)
    void onCloseBtnClicked() {
        onBackPressed();
    }

    @Click(R.id.ll_cartbtn)
    void onCartIconClicked() {
        if (viewingFragment == Fragments.TAX_DETAIL && Config.cartItems != null & Config.cartItems.size() > 0) {


            WSCall wsCall = new WSCall(SearchFragmentActivity.defaultInstance());
            final WSLoadingDialog loadingDialog = new WSLoadingDialog(SearchFragmentActivity.defaultInstance());
            List<NameValuePair> list = new ArrayList<>();
            if (!Config.InvoiceKey.equals("")) {
                list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
            }
            list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));

            wsCall.setAddToCart(Config.URL_GETCARTINFO, list, new WSListener() {
                @Override
                public void onRequestSent() {
                    loadingDialog.showWSLoadingDialog();
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    loadingDialog.hideWSLoadingDialog();
                    if (response != null) {
                        try {
                            JSONObject result = new JSONObject(response);
                            JSONObject json = result.getJSONObject("FLTax");
                            CartItemList cartItemList = new CartItemList(json);
                            Config.cartItems = cartItemList.getCartItemsList();
                            Config.shoppingCartItems = cartItemList.getShoppingCartList();
                            Config.InvoiceKey = cartItemList.getShoppingCartList().getInvoiceKey();


                            Bundle args = new Bundle();
                            args.putParcelableArrayList("CartItemslist", Config.cartItems);
                            args.putParcelable("ShoppingCartItem", Config.shoppingCartItems);
                            args.putString("InvoiceKey", Config.InvoiceKey);
                            SearchFragmentActivity.defaultInstance().changeFragment(Fragments.CART_INFO, args);
//                            Intent cartIntent = new Intent("CartNotif");
//                            LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    @Click(R.id.ll_homebutton)
    void onHomeButtonClicked() {
        homebuttonPressed();
    }


    public void changeFragment(Fragments fragments, Bundle args) {

        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (fragmentsList != null) {
            fragmentsList.add(fragments);
        }
        if (fragmentsList.size() != 0) {
            viewingFragment = fragmentsList.get(fragmentsList.size() - 1);
        }
        switch (fragments) {
            case SEARCH_SCREEN:
                searchScreenFragment = new SearchScreenFragment_();
                try {
                    searchScreenFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mToolbarTitle.setText(getResources().getString(R.string.screen_searchbydetails));
                fragmentTransaction.replace(R.id.fl_fragmentHolder, searchScreenFragment, "searchScreenFragment");
                fragmentTransaction.commit();
                break;

            case SEARCH_RESULT:
                searchResultFragment = new SearchResultFragment_();
                try {
                    searchResultFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mToolbarTitle.setText(getResources().getString(R.string.screen_propertydetails));
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentHolder, searchResultFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case TAX_DETAIL:
                taxDetailFragment = new TaxDetailFragment_();
                try {
                    taxDetailFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                if (Config.isFromQR) {
//                    fragmentTransaction.replace(R.id.fl_fragmentHolder, taxDetailFragment, "taxDetailFragment");
//                } else {
//                    fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("searchResultFragment"));
//                    fragmentTransaction.add(R.id.fl_fragmentHolder, taxDetailFragment, "taxDetailFragment");
//                }
                fragmentTransaction.replace(R.id.fl_fragmentHolder, taxDetailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case DIALOG_FRAG:
                dialogFragment = new DialogFragment_();
                try {
                    dialogFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                ll_btn_close.setVisibility(View.VISIBLE);
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("taxDetailFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, dialogFragment, "dialogFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, dialogFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case QR_SCAN:
                qrScanFragment = new QRScanFragment();
                try {
                    qrScanFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                mToolbarTitle.setText(getResources().getString(R.string.screen_searchbyqr));
                fragmentTransaction.replace(R.id.fl_fragmentHolder, qrScanFragment, "qrScanFragment");
                fragmentTransaction.commit();
                break;

            case CART_INFO:
                cartInfoFragment = new CartInfoFragment_();
                try {
                    cartInfoFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("taxDetailFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, cartInfoFragment, "cartInfoFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, cartInfoFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case SELECT_PAYMENT:
                selectPaymentFragment = new SelectPaymentFragment_();
                try {
                    selectPaymentFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("cartInfoFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, selectPaymentFragment, "selectPaymentFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, selectPaymentFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PAYMENT_ADDRESS:
                paymentAddressFragment = new PaymentAddressFragment_();
                try {
                    selectPaymentFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("selectPaymentFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, paymentAddressFragment, "paymentAddressFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, paymentAddressFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PAYMENT_CREDIT:
                paymentCreditCardFragment = new PaymentCreditCardFragment_();
                try {
                    paymentCreditCardFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("selectPaymentFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, paymentCreditCardFragment, "paymentCreditCardFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, paymentCreditCardFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PAYMENT_ECHECK:
                paymentECheckFragment = new PaymentECheckFragment_();
                try {
                    paymentECheckFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("selectPaymentFragment"));
//                fragmentTransaction.add(R.id.fl_fragmentHolder, paymentECheckFragment, "paymentECheckFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, paymentECheckFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PAYMENT_SUCCESS:
                paymentSuccessFragment = new PaymentSuccessFragment_();
                try {
                    paymentSuccessFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                ll_homebutton.setVisibility(View.VISIBLE);
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
//                if(Config.isCreditCard) {
//                    fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("paymentCreditCardFragment"));
//                }else{
//                    fragmentTransaction.hide(getSupportFragmentManager().findFragmentByTag("paymentECheckFragment"));
//                }
//                fragmentTransaction.add(R.id.fl_fragmentHolder, paymentSuccessFragment, "paymentSuccessFragment");
                fragmentTransaction.replace(R.id.fl_fragmentHolder, paymentSuccessFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;


            case MAP_VIEW:
                mapViewFragment = new MapViewFragment_();
                try {
                    mapViewFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentHolder, mapViewFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case PDF_VIEW:
                webViewFragment = new WebViewFragment();
                try {
                    webViewFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentHolder, webViewFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootInstance = this;
        isTablet = Config.isTablet(SearchFragmentActivity.this);
        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        LocalBroadcastManager.getInstance(SearchFragmentActivity.this).registerReceiver(cartdataReceiver, new IntentFilter("CartNotif"));
        LocalBroadcastManager.getInstance(SearchFragmentActivity.this).registerReceiver(networkReceiver, new IntentFilter("NetworkStatus"));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(SearchFragmentActivity.this).unregisterReceiver(cartdataReceiver);
        LocalBroadcastManager.getInstance(SearchFragmentActivity.this).unregisterReceiver(networkReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNetworkAlert = new AlertDialog.Builder(SearchFragmentActivity.this);
        mServerUnAvailAlert = new AlertDialog.Builder(SearchFragmentActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        if (viewingFragment == Fragments.DIALOG_FRAG) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ll_btn_close.setVisibility(View.GONE);
        }
        if (fragmentsList.size() > 1) {

            if (viewingFragment == Fragments.PAYMENT_SUCCESS) {
                homebuttonPressed();
            } else if (viewingFragment == Fragments.TAX_DETAIL && Config.isFromQR) {
                finish();
            } else {
                if (fragmentsList.size() != 0) {
                    viewingFragment = fragmentsList.get(fragmentsList.size() - 2);
                }
                fragmentsList.remove(fragmentsList.size() - 1);
                super.onBackPressed();
            }
        } else {
            finish();
        }
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();

    }

    public void homebuttonPressed() {
        finish();
    }

    public void hideKeyBoard() {
        if (getCurrentFocus() != null && ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).isAcceptingText()) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void showNetworkAlert() {
        mNetworkAlert.setCancelable(false);
        mNetworkAlert.setTitle(getResources().getString(R.string.alert_networktitle));
        mNetworkAlert.setMessage(getResources().getString(R.string.alert_nonetwork));
        mNetworkAlert.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SearchFragmentActivity.this, SplashActivity_.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
            }
        });
        mNetworkAlert.show();
    }

    public void showServerBusyAlert() {
        mServerUnAvailAlert.setCancelable(false);
        mServerUnAvailAlert.setTitle(getResources().getString(R.string.alert_app));
        mServerUnAvailAlert.setMessage(getResources().getString(R.string.alert_nonetwork));
        mServerUnAvailAlert.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(SearchFragmentActivity.this, SplashActivity_.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("EXIT", true);
                startActivity(intent);
                finish();
            }
        });
        mServerUnAvailAlert.show();
    }
}
