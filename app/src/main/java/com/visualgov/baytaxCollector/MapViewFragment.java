package com.visualgov.baytaxCollector;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import utils.GeoCodingLocation;


/**
 * Created by SaraschandraaM on 01/09/15.
 */
@EFragment(R.layout.fragment_mapview)
public class MapViewFragment extends Fragment {

    GoogleMap mGoogleMap;
    String slocationAddress;
    Bundle arguments;
    LatLng latLng;

    double Lat = 30.308552;
    double Long = -85.670733;
    Handler mapHandler;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            slocationAddress = arguments.getString("Location");
        }
        mapHandler = new Handler();
        mGoogleMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.mapView)).getMap();
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Lat, Long), 7.0f));
                mapHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showMarker();
                    }
                }, 300);
            }
        });
    }


    private void showMarker() {
        GeoCodingLocation locationAddress = new GeoCodingLocation();
        locationAddress.getAddressFromLocation(slocationAddress, getActivity(), new GeocoderHandler());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_mapview));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private class GeocoderHandler extends Handler {
        double lat, longitude;

        @Override
        public void handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    slocationAddress = bundle.getString("address");
                    lat = bundle.getDouble("lat");
                    longitude = bundle.getDouble("long");
                    break;
                default:
                    slocationAddress = null;
            }
            final LatLng CIU = new LatLng(lat, longitude);
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longitude), 9.0f));
            mGoogleMap.addMarker(new MarkerOptions().position(CIU));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        MapFragment mapFragment = (MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.mapView);
        if (mapFragment != null)
            getActivity().getFragmentManager().beginTransaction().remove(mapFragment).commit();
    }
}
