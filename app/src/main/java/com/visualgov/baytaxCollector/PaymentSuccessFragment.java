package com.visualgov.baytaxCollector;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.telephony.SmsManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import Model.PaymentInfo;
import Model.ShoppingCartList;
import utils.Config;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
@EFragment(R.layout.fragment_paymentsuccess)
public class PaymentSuccessFragment extends Fragment {


    private static String FILE = Environment.getExternalStorageDirectory() + "/BayTaxCollector.pdf";
    WSLoadingDialog wsLoadingDialog;
    Handler pdfGenerateHandler;
    PaymentInfo paymentInfo;

    File mFile;


    @Click(R.id.btn_payment_email)
    void onEmailButtonClicked() {
        if (paymentInfo.getPaymentStatus().equals("Approved")) {
            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("application/pdf");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Tax-Bill Invoice");
            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Payment Receipt for Property Tax Payment made from a mobile app");
            emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + sFilePath));
            startActivity(Intent.createChooser(emailIntent, "Send E-Mail"));
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_email), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.btn_payment_text)
    void onSMSButtonClicked() {
        if (paymentInfo.getPaymentStatus().equals("Approved")) {
            showPhoneNumberPopupDialog();
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_sms), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.btn_payment_pdf)
    void onPDFClicked() {
        if (paymentInfo.getPaymentStatus().equals("Approved")) {
            File file = new File(sFilePath);
            Intent target = new Intent(Intent.ACTION_VIEW);
            target.setDataAndType(Uri.fromFile(file), "application/pdf");
            target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);

            Intent intent = Intent.createChooser(target, "Open File");
            try {
                startActivity(intent);
            } catch (ActivityNotFoundException e) {
            }
        }else{
            Toast.makeText(getActivity(), "PDF is not available for Declined Payments", Toast.LENGTH_SHORT).show();
        }
    }

    @ViewById(R.id.tv_updatedate)
    TextView mTvUpdateDate;

    @ViewById(R.id.tv_success_invoicekey)
    TextView mTvInvoiceKey;

    @ViewById(R.id.tv_success_totalamount)
    TextView mTvTotalAmount;

    @ViewById(R.id.tv_success_totalfeeamount)
    TextView mTvTotalFeeAmount;

    @ViewById(R.id.tv_paymentmessage)
    TextView mTvPaymentMessage;

    @ViewById(R.id.tv_emailConfirmation)
    TextView mTvEmailConfirmation;

    @ViewById(R.id.tv_confirmtitle)
    TextView mTvConfirmTitle;

    @ViewById(R.id.vw_left)
    View leftView;

    @ViewById(R.id.vw_right)
    View rightView;

    Bundle arguments;

    String sTotalAmount, sTotalFeeAmount, sInvoicekey, sUpdateTime, EmailID, sPhoneNumber, sFilePath, sPaymentMessage;
    boolean isPaymentSuccess;
    double amount, totalamount, feeamount;

    @AfterViews
    void onViewLoaded() {
        wsLoadingDialog = new WSLoadingDialog(getActivity());
        arguments = getArguments();
        EmailID = arguments.getString("email");
        paymentInfo = arguments.getParcelable("PaymentInfo");

        feeamount = Double.parseDouble(paymentInfo.getTotalFeeAmount());
        amount = Double.parseDouble(paymentInfo.getTotalAmount());
        totalamount = feeamount + amount;

        sTotalAmount = "" + totalamount;

        sTotalAmount = Config.formatCurrency(sTotalAmount);
        sTotalFeeAmount = Config.formatCurrency(paymentInfo.getTotalFeeAmount());
        sInvoicekey = paymentInfo.getInvoiceKey();
        sUpdateTime = paymentInfo.getUpdateDate();

        mTvUpdateDate.setText(sUpdateTime);
        mTvTotalAmount.setText(sTotalAmount);
        mTvTotalFeeAmount.setText(sTotalFeeAmount);


        if (!paymentInfo.getPaymentStatus().equals("Approved")) {
            mTvEmailConfirmation.setVisibility(View.GONE);
            sPaymentMessage = "Your Payment was declined due to " + paymentInfo.getMessage();
            mTvPaymentMessage.setText(sPaymentMessage);
            mTvPaymentMessage.setTextColor(getActivity().getResources().getColor(R.color.light_red));
            leftView.setVisibility(View.GONE);
            rightView.setVisibility(View.GONE);
            mTvConfirmTitle.setVisibility(View.GONE);
            mTvInvoiceKey.setVisibility(View.GONE);
            isPaymentSuccess = false;
        } else {
            wsLoadingDialog.showWSLoadingDialog();
            sPaymentMessage = "Your Payment was processed successfully";
            mTvPaymentMessage.setTextColor(getActivity().getResources().getColor(R.color.color_green));
            mTvInvoiceKey.setText(sInvoicekey);
            isPaymentSuccess = true;
            pdfGenerateHandler = new Handler();
            pdfGenerateHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    wsLoadingDialog.hideWSLoadingDialog();
                    Log.i("PDF GENERATED:", "Done");
                    newpdfgenerate();
                }
            }, 500);
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_success));
    }

    @Override
    public void onPause() {
        super.onPause();
        Config.cartItems = new ArrayList<>();
        Config.shoppingCartItems = new ShoppingCartList();
        Config.InvoiceKey = "";

    }

    @SuppressLint("SimpleDateFormat")
    private void getCurrentDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat pf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFromat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat timeFromatParam = new SimpleDateFormat("HH:mm");

        sUpdateTime = df.format(c.getTime());

    }

    private String getBodyMessage() {
        String message = "<B>" + getActivity().getResources().getString(R.string.app_name) + "</B>" + ", <br/> <br/>";
        if (isPaymentSuccess) {
            message += "InvoiceKey : " + sInvoicekey + ", <br/>";
        } else {
            message += "Fee : " + sTotalFeeAmount + ", <br/>" +
                    "TotalAmount : " + sTotalAmount + ", <br/>" +
                    "Date : " + sUpdateTime;
        }
        return message;
    }

    private void showPhoneNumberPopupDialog() {
        final Dialog mDialog = new Dialog(getActivity(), R.style.AppTheme_Base);
        View mDialogView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_phonenumber, null);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        mDialog.setContentView(mDialogView);

        final EditText phonenumber = (EditText) mDialogView.findViewById(R.id.et_phonenumber);

        TextView tvTitle = (TextView) mDialogView.findViewById(R.id.tv_dialog_title);
        TextView tvPosButton = (TextView) mDialogView.findViewById(R.id.tv_dialog_posbtn);

        LinearLayout btnPos = (LinearLayout) mDialogView.findViewById(R.id.ll_posbtn);

        tvPosButton.setText("Send");

        btnPos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sPhoneNumber = phonenumber.getText().toString();
                if (!sPhoneNumber.isEmpty()) {
                    mDialog.dismiss();
                    sendSMS();
                }
            }
        });

        mDialog.show();
    }

    private void sendSMS() {
        SmsManager sm = SmsManager.getDefault();
        String message = Html.fromHtml(getBodyMessage()).toString();
        ArrayList<String> parts = sm.divideMessage(message);
        sm.sendMultipartTextMessage(sPhoneNumber, null, parts, null, null);
        SearchFragmentActivity.defaultInstance().hideKeyBoard();
        Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT).show();
    }


    private void shownotification(final String filepath) {
        File file = new File(filepath);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = file.getName().substring(file.getName().indexOf(".") + 1);
        String type = mime.getMimeTypeFromExtension(ext);
        Intent openFile = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));
        openFile.setDataAndType(Uri.fromFile(file), type);
        openFile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent p = PendingIntent.getActivity(getActivity(), 0, openFile, 0);
        NotificationManager mNotifyManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity());
        mBuilder.setContentTitle("PaymentReceipt")
                .setContentText("Payment Received Saved in your device")
                .setSmallIcon(R.mipmap.icon_launcher)
                .setContentIntent(p);
        mNotifyManager.notify(1, mBuilder.build());
    }


    private void newpdfgenerate() {
        try {


            String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/BayTaxCollector";

            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            Log.d("PDFCreator", "PDF Path: " + path);

            String PDFName = "TaxPaymentReceipt.pdf";


            mFile = new File(dir, PDFName);
            FileOutputStream fOut = new FileOutputStream(mFile);

            Document document = new Document(PageSize.A4, 50, 50, 50, 50);
            PdfWriter.getInstance(document, fOut);
            document.open();


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_pdfheader);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

            Image image = Image.getInstance(stream.toByteArray());
            image.scaleAbsolute(300f, 150f);
            image.setAlignment(Element.ALIGN_TOP | Element.ALIGN_CENTER);
            document.add(image);


            Font paraFont = new Font(Font.FontFamily.COURIER);
            paraFont.setSize(25f);

            document.top(100f);

            if (isPaymentSuccess) {
                Font successFont = new Font(Font.FontFamily.COURIER);
                successFont.setSize(25f);
                successFont.setColor(22, 164, 11);

                Paragraph paymentmessage = new Paragraph(sPaymentMessage);
                paymentmessage.setAlignment(Element.ALIGN_CENTER);
                paymentmessage.setFont(successFont);
                document.add(paymentmessage);


                document.top(100f);

                Paragraph p1 = new Paragraph("Confirmation#         :       " + sInvoicekey);
                p1.setAlignment(Element.ALIGN_CENTER);
                p1.setFont(paraFont);
                document.add(p1);
            } else {
                Font failurefont = new Font(Font.FontFamily.COURIER);
                failurefont.setSize(25f);
                failurefont.setColor(213, 55, 19);

                Paragraph paymentmessage = new Paragraph(sPaymentMessage);
                paymentmessage.setAlignment(Element.ALIGN_CENTER);
                paymentmessage.setFont(failurefont);
                document.add(paymentmessage);

                document.top(100f);
            }

            Paragraph p3 = new Paragraph("Fee     :       " + sTotalFeeAmount);
            p3.setAlignment(Element.ALIGN_CENTER);
            p3.setFont(paraFont);
            document.add(p3);

            Paragraph p2 = new Paragraph("Total Amount        :       " + sTotalAmount);
            p2.setAlignment(Element.ALIGN_CENTER);
            p2.setFont(paraFont);
            document.add(p2);


            Paragraph p4 = new Paragraph(" Date         :       " + sUpdateTime);
            p4.setAlignment(Element.ALIGN_CENTER);
            p4.setFont(paraFont);
            document.add(p4);


            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            Bitmap bitmap1 = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.ic_pdffooter);
            bitmap1.compress(Bitmap.CompressFormat.JPEG, 100, stream1);

            Image image2 = Image.getInstance(stream1.toByteArray());
            image2.scaleAbsolute(175f, 100f);
            image2.setAlignment(Element.ALIGN_BOTTOM | Element.ALIGN_RIGHT);
            document.add(image2);


            document.close();
            shownotification(path + "/" + PDFName);
            sFilePath = path + "/" + PDFName;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
