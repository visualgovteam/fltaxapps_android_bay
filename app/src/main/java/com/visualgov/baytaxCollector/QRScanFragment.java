package com.visualgov.baytaxCollector;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.TaxDetailInfo;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 24/08/15.
 */

public class QRScanFragment extends Fragment {

    View qrView;

    WSCall wsCall;
    WSLoadingDialog wsLoadingDialog;
    boolean isDifferentCounty = false;

    private CodeScanner mCodeScanner;
    private static final int REQUEST_CAMERA = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        qrView = inflater.inflate(R.layout.fragment_qrscan, container, false);
        CodeScannerView scannerView = qrView.findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(getActivity(), scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean isValidQR = isQRCodeValid(result.getText().toString());
                        if (isValidQR) {
                            processQRScan(result.getText().toString());
                        } else {
                            Toast.makeText(getActivity(), "The QR Code is not in correct format", Toast.LENGTH_SHORT).show();
                            mCodeScanner.releaseResources();
                            mCodeScanner.startPreview();
                        }
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
        return qrView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT < 23) {
            mCodeScanner.startPreview();
        } else {
            loadPermissions(Manifest.permission.CAMERA, REQUEST_CAMERA);
        }

    }

    @Override
    public void onPause() {
        if (mCodeScanner != null) {
            mCodeScanner.releaseResources();
        }
        super.onPause();
    }

    private boolean isQRCodeValid(String result) {
        boolean valid = false;
        if (result != null && !result.equals("")) {
            String[] value = result.split("\\|");
            if (value.length == 4) {
                if (value[0].equals(utils.Config.VGPRODID)) {
                    valid = true;
                } else {
                    valid = false;
                    isDifferentCounty = true;
                }
            } else {
                valid = false;
            }
        }
        return valid;
    }


    private void processQRScan(String result) {
        String[] value = result.split("\\|");
        String rolltype = value[1];
        String billno = value[2];
        String taxyear = value[3];
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("VGProdID", utils.Config.VGPRODID));
        list.add(new BasicNameValuePair("RollType", rolltype));
        list.add(new BasicNameValuePair("TaxBillNo", billno));
        list.add(new BasicNameValuePair("TaxYear", taxyear));
        wsCall = new WSCall(getActivity());
        wsCall.getTaxDetailInfo(utils.Config.URL_TAXDETAIL, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject json = jsonObject.getJSONObject("FLTax");
                        TaxDetailInfo detailInfo = new TaxDetailInfo(json);
                        Bundle args = new Bundle();
                        args.putParcelable("TaxDetail", detailInfo);
                        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.TAX_DETAIL, args);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        mCodeScanner.releaseResources();
                        mCodeScanner.startPreview();
                    }
                } else {
                    mCodeScanner.releaseResources();
                    mCodeScanner.startPreview();
                }
            }
        });

    }

    private void loadPermissions(String perm, int requestCode) {
        if (ContextCompat.checkSelfPermission(getActivity(), perm) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), perm)) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{perm}, requestCode);
            }
        } else {
            mCodeScanner.startPreview();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA: {
                if (grantResults.length > 0) {                                                      /** If request is cancelled, the result arrays are empty. **/
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        mCodeScanner.startPreview();
                    }
                } else {
                    /** no granted **/
                }
                return;
            }
        }

    }

}
