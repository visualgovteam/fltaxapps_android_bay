package com.visualgov.baytaxCollector;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import Model.AdValorem;
import Model.CartItemList;
import Model.DelinquentList;
import Model.Details;
import Model.DueList;
import Model.InstallmentList;
import Model.LeviedList;
import Model.NonAdValorem;
import Model.TaxDetailInfo;
import Model.Values;
import utils.Config;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 20/08/15.
 */
@EFragment(R.layout.fragment_taxdetail)
public class TaxDetailFragment extends Fragment {

    Bundle arguments;
    TaxDetailInfo taxDetailInfo;

    @ViewById(R.id.tv_taxdet_billno)
    TextView mBillNo;

    @ViewById(R.id.tv_taxdet_addr)
    TextView mTvAddr;

    @ViewById(R.id.tv_taxdet_ownername)
    TextView mTvOwnerName;

    @ViewById(R.id.tv_taxdet_prtyno)
    TextView mTvPrptyNo;

    @ViewById(R.id.tv_taxdet_combtax)
    TextView mTvCombTax;

    @ViewById(R.id.tv_taxdet_discount)
    TextView mTvDiscount;

    @ViewById(R.id.tv_taxdet_unpaidbal)
    TextView mTvUnpaid;

    @ViewById(R.id.tv_taxdet_advolamt)
    TextView mTvAdVol;

    @ViewById(R.id.tv_taxdet_noadvolamt)
    TextView mTvNonAdVol;

    @ViewById(R.id.tv_taxdet_delqlist)
    TextView mTvDelqAmt;

    @ViewById(R.id.tv_paystatus)
    TextView mTvPayStatus;

    @ViewById(R.id.tv_taxdet_salesdate)
    TextView mTvSalesDate;

    @ViewById(R.id.tv_taxdet_certno)
    TextView mTvCertificateno;

    @ViewById(R.id.tv_taxdet_certholder)
    TextView mTvCertificateHolder;

    @ViewById(R.id.tv_taxdet_intrate)
    TextView mTvInterestRate;

    @ViewById(R.id.tv_taxdet_orgamt)
    TextView mTvOriginalRate;

    @ViewById(R.id.tv_taxdet_intamt)
    TextView mTvInterestAmount;

    @ViewById(R.id.tv_taxdet_fees)
    TextView mTvFees;

    @ViewById(R.id.tv_taxdet_cerunpaidbal)
    TextView mTvCerUnpaidBal;

    @ViewById(R.id.tv_taxdet_prtyaddr)
    TextView mTvPropertyAddress;

    @ViewById(R.id.tv_taxdetail_message)
    TextView mTvNoMessage;

    @ViewById(R.id.tv_click)
    TextView mTvClicktoNavigate;

    @ViewById(R.id.tv_taxdet_exemptions)
    TextView mTvExemptions;

    @ViewById(R.id.tv_taxdet_exempt)
    TextView mTvNonCertificateExemptions;

    @ViewById(R.id.ll_deliquent)
    LinearLayout ll_delqlist;

    @ViewById(R.id.ll_taxdet_exempt)
    LinearLayout NonCertificateExemptLayout;

    @ViewById(R.id.iv_taxdet_advolmore)
    ImageView ivAdVolmore;

    @ViewById(R.id.iv_taxdet_noadvalmore)
    ImageView ivNonAdVolmore;

    @ViewById(R.id.ll_paymentlayout)
    LinearLayout paymentLayout;

    @ViewById(R.id.sc_certificateview)
    ScrollView certificateLayout;

    @ViewById(R.id.ll_installments)
    LinearLayout installmentListLayout;

    @ViewById(R.id.ll_detaislayout)
    LinearLayout mDetailsLayout;

    @ViewById(R.id.ll_notavailablelayout)
    LinearLayout mNonAvailableLayout;

    @ViewById(R.id.iv_map)
    ImageView mMapIcon;


    String sAddrLine1, sAddrLine2, sAddrLine3, sAddrLine4, sTaxRollType, sTaxBill, sTaxYear, sPropertyNo, sOwnerName, sCombTaxAmt, sDiscount, sUnPaidBal, sAdVolAmt,
            sNonAdVol, sIsPaid, sDelqAmt, sAdVolTaxAuth, sAdVolMileage, sNonAdVolTaxAuth, sDeedAcred, sLegal1, sLegal2, sLegal3, sLegal4,
            sRealValue, sCoNetTaxValue, sDist, sExemptValue, sGrossTaxValue, sLastpayment, sAmountCollected, sReceiptNumber, sCertificate, sTaxableValue,
            sSalesDate, sCertificateno, sCertificateHolder, sIntRate, sOrgAmt, sIntBal, sIntCollect, sFeesBal, sFeeCollections, sCertUnPaidBal, sAddress, sPriorYear, sHasInstallments,
            sAmountToPay, sHasDelinquentHistory, sPropertyAddr, sPropertyCity, sLocationAddress, sBillStatus, sNotAvailableMessage, sExemptions, sHasAmountCollected, sHasDisplaymentBox;

    double payableamount;

    boolean isPaid, isCertificate = false, isPriorYear, hasInstallments, hasDelinquentHistory, hasAmountCollected, hasPaymentDisplayBox;

    ArrayList<Details> detailsList;
    ArrayList<LeviedList> mLeviedList;
    ArrayList<DueList> mDueList;
    ArrayList<AdValorem> mAdValoremList;
    ArrayList<NonAdValorem> mNoAdValoremList;
    ArrayList<DelinquentList> mDelingquentList;
    ArrayList<String> mLegalList;
    ArrayList<Values> mValuesList;
    ArrayList<Values> mReceiptList;
    ArrayList<InstallmentList> mInstallmentList;
    Values data;
    Bundle args;
    WSLoadingDialog loadingDialog;


    @AfterViews
    void onViewLoaded() {
        detailsList = new ArrayList<>();
        mLeviedList = new ArrayList<>();
        mDueList = new ArrayList<>();
        mAdValoremList = new ArrayList<>();
        mNoAdValoremList = new ArrayList<>();
        mDelingquentList = new ArrayList<>();
        mValuesList = new ArrayList<>();
        mLegalList = new ArrayList<>();
        mReceiptList = new ArrayList<>();
        mInstallmentList = new ArrayList<>();
        ll_delqlist.setVisibility(View.GONE);
        installmentListLayout.setVisibility(View.GONE);
        arguments = getArguments();
        if (arguments != null) {
            taxDetailInfo = arguments.getParcelable("TaxDetail");
            detailsList = taxDetailInfo.getmDetailsList().getDetailsList();
            mLeviedList = taxDetailInfo.getmLeviedList();
            mDueList = taxDetailInfo.getmDueList();
            mAdValoremList = taxDetailInfo.getmAdValoremList();
            mNoAdValoremList = taxDetailInfo.getmNonAdValoremList();
            mDelingquentList = taxDetailInfo.getmDelinquentList();
            sCertificate = taxDetailInfo.getTaxMiscList().getIsCertificate();
            mInstallmentList = taxDetailInfo.getmInstallmentList();
            sBillStatus = taxDetailInfo.getTaxBillStatusList().getBillStatus();
            if (sCertificate.equals("True")) {
                isCertificate = true;
            } else if (sCertificate.equals("False")) {
                isCertificate = false;
            }
        }
        if (isCertificate) {
            certificateLayout.setVisibility(View.VISIBLE);
            paymentLayout.setVisibility(View.GONE);
        } else {
            certificateLayout.setVisibility(View.GONE);
            paymentLayout.setVisibility(View.VISIBLE);
        }
        ViewTreeObserver cartbutton = SearchFragmentActivity.defaultInstance().ll_cart_btn.getViewTreeObserver();
        cartbutton.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                SearchFragmentActivity.defaultInstance().ll_cart_btn.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                SearchFragmentActivity.defaultInstance().mToolbarWidth = SearchFragmentActivity.defaultInstance().ll_cart_btn.getMeasuredWidth();

            }
        });
        getDetailsInfo();
        mTvClicktoNavigate.setPaintFlags(mTvClicktoNavigate.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Config.isPayNowClicked = false;
    }

    @Click(R.id.tv_click)
    void onReturntoSearchClicked() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.popBackStack();
    }

    @Click(R.id.iv_taxdet_advolmore)
    void onAdVolmoreClicked() {
        if (mAdValoremList.size() == 0) {
            AdValorem data = new AdValorem();
            data.setTaxAmount(sAdVolAmt);
            data.setTaxingAuthority(sAdVolTaxAuth);
            data.setMillageRate(sAdVolMileage);
            mAdValoremList.add(data);
        }
        args = new Bundle();
        args.putInt("ScreenType", 1);
        args.putParcelableArrayList("AdVolValue", mAdValoremList);
        args.putString("AdVolTotal", sAdVolAmt);
        args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_advol));
        args.putString("TaxBill", sTaxBill);
        args.putString("TaxYear", sTaxYear);
        args.putString("TaxRollType", sTaxRollType);
        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
    }

    @Click(R.id.iv_taxdet_noadvalmore)
    void onNonAdVolmoreClicked() {
        if (mNoAdValoremList.size() == 0) {
            NonAdValorem data = new NonAdValorem();
            data.setTaxingAuthority(sNonAdVolTaxAuth);
            data.setTaxAmount(sNonAdVol);
            mNoAdValoremList.add(data);
        }
        args = new Bundle();
        args.putInt("ScreenType", 2);
        args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_nonadvol));
        args.putParcelableArrayList("NonAdVolValue", mNoAdValoremList);
        args.putString("NonAdVolTotal", sNonAdVol);
        args.putString("TaxBill", sTaxBill);
        args.putString("TaxYear", sTaxYear);
        args.putString("TaxRollType", sTaxRollType);
        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
    }

    @Click(R.id.iv_taxdet_duepay)
    void onDuePayClicked() {
        if (mDueList != null && mDueList.size() > 0 && hasPaymentDisplayBox) {
            args = new Bundle();
            args.putInt("ScreenType", 3);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_due));
            args.putParcelableArrayList("DueList", mDueList);
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        }else{
            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_noduelist), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.iv_taxdet_levlist)
    void onLevListClicked() {
        if (mLeviedList != null && mLeviedList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 4);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_levlist));
            args.putParcelableArrayList("LeviedList", mLeviedList);
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        }
    }

    @Click(R.id.btn_taxdet_legal)
    void onLegalClicked() {
        if (mLegalList != null && mLegalList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 5);
            args.putStringArrayList("LegalList", mLegalList);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_legals));
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.btn_taxdet_values)
    void onValuesClicked() {
        if (mValuesList != null && mValuesList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 6);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_values));
            args.putParcelableArrayList("ValuesList", mValuesList);
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.btn_taxdet_receipt)
    void onReceiptClicked() {
        if (mReceiptList != null && mReceiptList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 7);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_receipt));
            args.putParcelableArrayList("ReceiptList", mReceiptList);
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.iv_taxdet_delqlist)
    void onDeliqlistClicked() {
        if (mDelingquentList != null && mDelingquentList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 8);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_deliq));
            args.putParcelableArrayList("DeliqList", mDelingquentList);
            args.putString("DeliqLisTotal", sDelqAmt);
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.iv_taxdet_installist)
    void onInstallListClicked() {
        if (mInstallmentList != null && mInstallmentList.size() > 0) {
            args = new Bundle();
            args.putInt("ScreenType", 9);
            args.putString("ScreenTitle", getActivity().getResources().getString(R.string.screen_install));
            args.putString("TaxBill", sTaxBill);
            args.putString("TaxYear", sTaxYear);
            args.putString("TaxRollType", sTaxRollType);
            args.putParcelableArrayList("InstallList", mInstallmentList);
            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.DIALOG_FRAG, args);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.alert_nodata), Toast.LENGTH_SHORT).show();
        }
    }

    @Click(R.id.tv_paystatus)
    void onPaymentClicked() {
        if (!isCertificate && !isPaid) {
            WSCall wsCall = new WSCall(getActivity());
            loadingDialog = new WSLoadingDialog(getActivity());
            List<NameValuePair> list = new ArrayList<>();
            if (!Config.InvoiceKey.equals("")) {
                list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
            }
            list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
            list.add(new BasicNameValuePair("RollType", sTaxRollType));
            list.add(new BasicNameValuePair("TaxBillNo", sTaxBill));
            list.add(new BasicNameValuePair("TaxYear", sTaxYear));
            list.add(new BasicNameValuePair("TaxAmount", sUnPaidBal));

            wsCall.setAddToCart(Config.URL_ADDTOCART, list, new WSListener() {
                @Override
                public void onRequestSent() {
                    loadingDialog.showWSLoadingDialog();
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    loadingDialog.hideWSLoadingDialog();
                    if (response != null) {
                        try {
                            JSONObject result = new JSONObject(response);
                            JSONObject json = result.getJSONObject("FLTax");
                            CartItemList cartItemList = new CartItemList(json);
                            Config.cartItems = cartItemList.getCartItemsList();
                            Config.shoppingCartItems = cartItemList.getShoppingCartList();
                            Config.InvoiceKey = cartItemList.getShoppingCartList().getInvoiceKey();
                            Intent cartIntent = new Intent("CartNotif");
                            LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                            showToast();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    @Click(R.id.iv_map)
    void onMapClicked() {
        if (!sPropertyAddr.isEmpty()) {
            sLocationAddress = sPropertyAddr + sPropertyCity;
        } else if (!sAddress.isEmpty()) {
            sLocationAddress = sAddress;
        }
        Bundle args = new Bundle();
        args.putString("Location", sLocationAddress);
        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.MAP_VIEW, args);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void getDetailsInfo() {
        for (int i = 0; i < detailsList.size(); i++) {
            Details data = new Details();
            data = detailsList.get(i);
            switch (data.getKey()) {
                case "TaxRollType":
                    sTaxRollType = data.getValue();
                    break;

                case "TaxBillNumber":
                    sTaxBill = data.getValue();
                    break;

                case "TaxYear":
                    sTaxYear = data.getValue();
                    break;

                case "AddressLine1":
                    sAddrLine1 = data.getValue();
                    break;

                case "AddressLine2":
                    sAddrLine2 = data.getValue();
                    break;

                case "AddressLine3":
                    sAddrLine3 = data.getValue();
                    if (sAddrLine3 == null || sAddrLine3.equals("null")) {
                        sAddrLine3 = "";
                    }
                    break;

                case "AddressLine4":
                    sAddrLine4 = data.getValue();
                    if (sAddrLine4 == null || sAddrLine4.equals("null")) {
                        sAddrLine4 = "";
                    }
                    break;

                case "Name":
                    sOwnerName = data.getValue();
                    break;

                case "PropertyNumber":
                    sPropertyNo = data.getValue();
                    break;

                case "CombinedTaxTotal":
                    sCombTaxAmt = data.getValue();
                    sCombTaxAmt = Config.formatCurrency(sCombTaxAmt);
                    break;
                case "Discount":
                    sDiscount = data.getValue();
                    sDiscount = Config.formatCurrency(sDiscount);
                    break;
                case "AmountToDisplay":
                    sUnPaidBal = data.getValue();
                    sAmountToPay = data.getValue();
                    payableamount = Double.parseDouble(sAmountToPay);
                    sUnPaidBal = Config.formatCurrency(sUnPaidBal);
                    break;
                case "AdvaloremTotal":
                    sAdVolAmt = data.getValue();
                    sAdVolAmt = Config.formatCurrency(sAdVolAmt);
                    break;
                case "NonAdValoremTotal":
                    sNonAdVol = data.getValue();
                    sNonAdVol = Config.formatCurrency(sNonAdVol);
                    break;
                case "IsPaid":
                    sIsPaid = data.getValue();
                    if (sIsPaid.equals("True")) {
                        isPaid = true;
                    } else if (sIsPaid.equals("False")) {
                        isPaid = false;
                    }
                    break;

                case "AdValorem":
                    sAdVolTaxAuth = data.getValue();
                    break;

                case "MillageRate":
                    sAdVolMileage = data.getValue();
                    break;

                case "NonAdValorem":
                    sNonAdVolTaxAuth = data.getValue();
                    break;

                case "DeedAcred":
                    sDeedAcred = data.getValue();
                    break;

                case "Legal1":
                    sLegal1 = data.getValue();
                    if(sLegal1.contains("Â")) {
                        sLegal1 = sLegal1.replace("Â", "");
                    }
                    break;

                case "Legal2":
                    sLegal2 = data.getValue();
                    if(sLegal2.contains("Â")) {
                        sLegal2 = sLegal2.replace("Â", "");
                    }
                    break;

                case "Legal3":
                    sLegal3 = data.getValue().toString();
                    if(sLegal3.contains("Â")) {
                        sLegal3 = sLegal3.replace("Â", "");
                    }
                    break;

                case "Legal4":
                    sLegal4 = data.getValue();
                    if(sLegal4.contains("Â")) {
                        sLegal4 = sLegal4.replace("Â", "");
                    }
                    break;

                case "CoNetTaxValue":
                    sCoNetTaxValue = data.getValue();
                    sCoNetTaxValue = Config.formatCurrency(sCoNetTaxValue);
                    break;

                case "TaxableValue":
                    sTaxableValue = data.getValue();
                    sTaxableValue = Config.formatCurrency(sTaxableValue);
                    break;

                case "GrossTaxValue":
                    sGrossTaxValue = data.getValue();
                    sGrossTaxValue = Config.formatCurrency(sGrossTaxValue);
                    break;

                case "CoExemptValue":
                    sExemptValue = data.getValue();
                    sExemptValue = Config.formatCurrency(sExemptValue);
                    break;

                case "DistCode":
                    sDist = data.getValue();
                    break;

                case "RealValue":
                    sRealValue = data.getValue();
                    sRealValue = Config.formatCurrency(sRealValue);
                    break;

                case "LastPaymentDate":
                    sLastpayment = data.getValue();
                    break;

                case "AmountCollected":
                    sAmountCollected = data.getValue();
                    sAmountCollected = Config.formatCurrency(sAmountCollected);
                    break;

                case "ReceiptNumber":
                    sReceiptNumber = data.getValue();
                    break;

                case "SalesDate":
                    sSalesDate = data.getValue();
                    break;

                case "CertificateNumber":
                    sCertificateno = data.getValue();
                    break;

                case "BidderID":
                    sCertificateHolder = data.getValue();
                    break;

                case "BidRate":
                    sIntRate = data.getValue();
                    break;

                case "CertificateOriginalAmount":
                    sOrgAmt = data.getValue();
                    sOrgAmt = Config.formatCurrency(sOrgAmt);
                    break;

                case "InterestBalance":
                    sIntBal = data.getValue();
                    sIntBal = Config.formatCurrency(sIntBal);
                    break;

                case "InterestCollections":
                    sIntCollect = data.getValue();
                    sIntCollect = Config.formatCurrency(sIntCollect);
                    break;

                case "RedemptionFeeBalance":
                    sFeesBal = data.getValue();
                    sFeesBal = Config.formatCurrency(sFeesBal);
                    break;

                case "RedemptionFeeCollections":
                    sFeeCollections = data.getValue();
                    sFeeCollections = Config.formatCurrency(sFeeCollections);
                    break;

                case "TotalCertificateBalance":
                    sCertUnPaidBal = data.getValue();
                    sCertUnPaidBal = Config.formatCurrency(sCertUnPaidBal);
                    break;

                case "HasInstallments":
                    sHasInstallments = data.getValue();
                    if (sHasInstallments.equals("True")) {
                        hasInstallments = true;
                    } else if (sHasInstallments.equals("False")) {
                        hasInstallments = false;
                    }
                    break;

                case "IsPriorYear":
                    sPriorYear = data.getValue();
                    if (sPriorYear.equals("True")) {
                        isPriorYear = true;
                    } else if (sPriorYear.equals("False")) {
                        isPriorYear = false;
                    }
                    break;

                case "HasDelinquentHistory":
                    sHasDelinquentHistory = data.getValue();
                    if (sHasDelinquentHistory.equals("True")) {
                        hasDelinquentHistory = true;
                    } else if (sHasDelinquentHistory.equals("False")) {
                        hasDelinquentHistory = false;
                    }
                    break;

                case "PropertyStreet":
                    sPropertyAddr = data.getValue();
                    break;

                case "PropertyCityZip":
                    sPropertyCity = data.getValue();
                    sPropertyCity = sPropertyCity.replaceAll("\\s+", " ");
                    break;

                case "Exemption":
                    sExemptions = data.getValue();
                    break;

                case "HasAmountCollected":
                    sHasAmountCollected = data.getValue();
                    if (sHasAmountCollected.equals("True")) {
                        hasAmountCollected = true;
                    } else if (sHasAmountCollected.equals("False")) {
                        hasAmountCollected = false;
                    }
                    break;

                case "CanDisplayPaymentBox":
                    sHasDisplaymentBox = data.getValue();
                    if (sHasDisplaymentBox.equals("True")) {
                        hasPaymentDisplayBox = true;
                    } else if (sHasDisplaymentBox.equals("False")) {
                        hasPaymentDisplayBox = false;
                    }
                    break;


                default:
                    break;

            }
        }

        if (sBillStatus != null && sBillStatus.equals("TD")) {
            mDetailsLayout.setVisibility(View.GONE);
            mNonAvailableLayout.setVisibility(View.VISIBLE);
            sNotAvailableMessage = "<b> TaxBillNo: " + sTaxRollType + " " + sTaxBill + " " + sTaxYear + "</b> <br/> <br/>" + "This parcel is or has been included on a Tax Deed Application, <br/>" +
                    "please contact the Tax Collector for its current status.";
            mTvNoMessage.setText(Html.fromHtml(sNotAvailableMessage));
        } else {
            mDetailsLayout.setVisibility(View.VISIBLE);
            mNonAvailableLayout.setVisibility(View.GONE);
            setDetails();
        }
    }

    private void setDetails() {
        if (mLeviedList != null && mLeviedList.size() > 0) {
            for (int i = 0; i < mLeviedList.size(); i++) {
                LeviedList data = mLeviedList.get(i);
                if (data.getLeviedDescription().contains("DISCOUNT GIVEN")) {
                    sDiscount = data.getLeviedOriginal();
                    sDiscount = Config.formatCurrency(sDiscount);
                }
            }
        }
        mBillNo.setText("" + sTaxRollType + " " + sTaxBill + " " + sTaxYear);
        if (Config.isTablet(getActivity())) {
            sAddress = sAddrLine1 + " " + sAddrLine2 + " " + sAddrLine3 + " " + sAddrLine4;
        } else {
            sAddress = sAddrLine1 + "\n" + sAddrLine2 + sAddrLine3 + sAddrLine4;
        }
        sLocationAddress = sPropertyAddr + " " + sPropertyCity;
        if (sAddrLine1 != null &&  sAddrLine1.contains("CONFIDENTIAL")) {
            mTvAddr.setText("CONFIDENTIAL");
            mTvOwnerName.setText("CONFIDENTIAL");
            mMapIcon.setVisibility(View.GONE);
        } else {
            mTvAddr.setText(sAddress);
            mTvOwnerName.setText(sOwnerName);
        }
        mTvPropertyAddress.setText(sLocationAddress);
        mTvPrptyNo.setText("" + sTaxRollType + " " + sPropertyNo);
        if (!isCertificate) {
            mTvCombTax.setText(sCombTaxAmt);
            mTvDiscount.setText(sDiscount);
            mTvUnpaid.setText(sUnPaidBal);
            mTvAdVol.setText(sAdVolAmt);
            mTvNonAdVol.setText(sNonAdVol);
            if(!sExemptions.isEmpty() && !sExemptions.equals("null") && sExemptions != null){
                NonCertificateExemptLayout.setVisibility(View.VISIBLE);
                mTvNonCertificateExemptions.setText(sExemptions);
            }else{
                NonCertificateExemptLayout.setVisibility(View.GONE);
            }
        } else {
            mTvSalesDate.setText(sSalesDate);
            mTvCertificateno.setText(sCertificateno);
            mTvCertificateHolder.setText(sCertificateHolder);
            mTvInterestRate.setText(sIntRate);
            mTvOriginalRate.setText(sOrgAmt);
            if (!hasAmountCollected) {
                mTvInterestAmount.setText(sIntBal);
                mTvFees.setText(sFeesBal);
            } else {
                mTvInterestAmount.setText(sIntCollect);
                mTvFees.setText(sFeeCollections);
            }
            mTvCerUnpaidBal.setText(sCertUnPaidBal);
            mTvExemptions.setText(sExemptions);
        }


        if (mDelingquentList != null && mDelingquentList.size() > 0) {
            ll_delqlist.setVisibility(View.VISIBLE);
            double delqamt = 0;
            for (int i = 0; i < mDelingquentList.size(); i++) {
                String amt = mDelingquentList.get(i).getTaxTotal();
                amt = amt.replaceAll("\\$", "");
                amt = amt.replaceAll("\\,", "");
                Double data = Double.parseDouble(amt);
                delqamt = delqamt + data;
                DecimalFormat delqformat = new DecimalFormat("#,###,###.00");
                sDelqAmt = "$" + delqformat.format(delqamt);
                mTvDelqAmt.setText(sDelqAmt);

            }
        } else {
            ll_delqlist.setVisibility(View.GONE);
        }

        if (isCertificate) {
            if (sCertUnPaidBal.equals("$0.00")) {
                mTvPayStatus.setText("CERTIFICATE SOLD PAID");
            } else {
                mTvPayStatus.setText("CERTIFICATE SOLD");
            }
            mTvPayStatus.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_paid));
            mTvPayStatus.setTextColor(getActivity().getResources().getColor(R.color.white));
        } else if (isPaid) {
            mTvPayStatus.setText(getActivity().getResources().getString(R.string.taxdet_paid));
            mTvPayStatus.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_paid));
            mTvPayStatus.setTextColor(getActivity().getResources().getColor(R.color.white));
        } else {
            if ((Config.isCreditCardAllowed || Config.isECheckAllowed) && !isPaid && !isPriorYear && !hasInstallments && payableamount > 0) {
                mTvPayStatus.setText(getActivity().getResources().getString(R.string.taxdet_paynow));
                mTvPayStatus.setBackground(getActivity().getResources().getDrawable(R.drawable.ic_paynow));
                mTvPayStatus.setTextColor(getActivity().getResources().getColor(R.color.white));
            } else {
                mTvPayStatus.setVisibility(View.GONE);
            }

        }

        if (sDeedAcred != null && !sDeedAcred.equals("")) {
            mLegalList.add(sDeedAcred);
        }
        if (sLegal1 != null && !sLegal1.equals("")) {
            mLegalList.add(sLegal1);
        }
        if (sLegal2 != null && !sLegal2.equals("")) {
            mLegalList.add(sLegal2);
        }

        if (sLegal3 != null && !sLegal3.equals("")) {
            mLegalList.add(sLegal3);
        }
        if (sLegal4 != null && !sLegal4.equals("")) {
            mLegalList.add(sLegal4);
        }

        if (sRealValue != null && !sRealValue.equals("")) {
            data = new Values();
            data.setKey("FAIR MKT VALUE");
            data.setData(sRealValue);
            mValuesList.add(data);
        }

        if (sCoNetTaxValue != null && !sCoNetTaxValue.equals("")) {
            data = new Values();
            data.setKey("TAXABLE VALUE");
            data.setData(sCoNetTaxValue);
            mValuesList.add(data);
        }

        if (sTaxableValue != null && !sTaxableValue.equals("")) {
            data = new Values();
            data.setKey("TAXABLE VALUE");
            data.setData(sTaxableValue);
            mValuesList.add(data);
        }

        if (sGrossTaxValue != null && !sGrossTaxValue.equals("")) {
            data = new Values();
            data.setKey("ASSESS");
            data.setData(sGrossTaxValue);
            mValuesList.add(data);
        }

        if (sDist != null && !sDist.equals("")) {
            data = new Values();
            data.setKey("DIST");
            data.setData(sDist);
            mValuesList.add(data);
        }

        if (sExemptValue != null && !sExemptValue.equals("")) {
            data = new Values();
            data.setKey("EXEMPT VALUE");
            data.setData(sExemptValue);
            mValuesList.add(data);
        }

        if (sLastpayment != null && !sLastpayment.equals("")) {
            data = new Values();
            data.setKey("Last Payment");
            data.setData(sLastpayment);
            mReceiptList.add(data);
        }

        if (sAmountCollected != null && !sAmountCollected.equals("")) {
            data = new Values();
            data.setKey("Amount Collected");
            data.setData(sAmountCollected);
            mReceiptList.add(data);
        }


        if (sReceiptNumber != null && !sReceiptNumber.equals("")) {
            data = new Values();
            data.setKey("Receipt Number");
            data.setData(sReceiptNumber);
            mReceiptList.add(data);
        }

        if (sDiscount != null && !sDiscount.equals("")) {
            data = new Values();
            data.setKey("Discount Amount");
            data.setData(sDiscount);
            mReceiptList.add(data);
        }

        if (hasInstallments && mInstallmentList != null && mInstallmentList.size() > 0) {
            installmentListLayout.setVisibility(View.VISIBLE);
        }


    }


    public void showToast() {

        int Y = SearchFragmentActivity.defaultInstance().mToolbarHeight;
        int X = SearchFragmentActivity.defaultInstance().mToolbarWidth;
        int yhalf = Y / 4;
        int yoff;
        if (Config.isTablet(getActivity())) {
            yoff = (3 * dpToPx(Y));
        } else {
            yoff = 2 * dpToPx(Y);
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.cell_cartpopup, null);
        TextView tv = (TextView) layout.findViewById(R.id.tv_cartpopmsg);
        tv.setText("Item is added to cart");
        Toast toast = new Toast(getActivity());
        toast.setGravity(Gravity.RIGHT | Gravity.TOP, X, yhalf);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_taxbill));
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.VISIBLE);
        SearchFragmentActivity.defaultInstance().ll_search.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        super.onPause();
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.GONE);
        SearchFragmentActivity.defaultInstance().ll_search.setVisibility(View.GONE);
        if (Config.isFromQR) {
            SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_searchbyqr));
        } else {
            SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_propertydetails));
        }
    }
}

