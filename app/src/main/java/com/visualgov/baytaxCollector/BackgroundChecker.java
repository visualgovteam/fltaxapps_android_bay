package com.visualgov.baytaxCollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

/**
 * Created by SaraschandraaM on 28/08/15.
 */
public class BackgroundChecker extends BroadcastReceiver {

    Context context;

    @Override
    public void onReceive(final Context context, Intent intent) {

        AlertDialog.Builder AppAlert = new AlertDialog.Builder(context, R.style.AppTheme_Base);
        AppAlert.setMessage("Do you want to Exit the app, All the Current Data will be Lost");
        AppAlert.setPositiveButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AppAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent wakeupIntent = new Intent(context, SearchFragmentActivity_.class);
                context.startActivity(wakeupIntent);
            }
        });

        AppAlert.show();
    }



}
