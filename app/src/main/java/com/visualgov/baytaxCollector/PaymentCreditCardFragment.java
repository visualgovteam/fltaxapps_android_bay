package com.visualgov.baytaxCollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import Model.CartInfo;
import Model.CartItems;
import Model.PaymentInfo;
import Model.ShoppingCartList;
import Model.StateList;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 27/08/15.
 */
@EFragment(R.layout.fragment_paymentaddress)
public class PaymentCreditCardFragment extends Fragment implements View.OnClickListener {


    @ViewById(R.id.tv_cart_taxamt)
    TextView mTvCartAmount;

    @ViewById(R.id.tv_cart_convfee)
    TextView mTvConvAmount;

    @ViewById(R.id.tv_cart_totalamt)
    TextView mTvTotalAmount;

    @ViewById(R.id.et_cart_firstname)
    EditText mEtFirstName;

    @ViewById(R.id.et_cart_lastname)
    EditText mEtLastName;

    @ViewById(R.id.et_cart_streetaddr1)
    EditText mEtStreetAddress1;

    @ViewById(R.id.et_cart_streetaddr2)
    EditText mEtStreetAddress2;

    @ViewById(R.id.et_cart_city)
    EditText mEtCity;

    @ViewById(R.id.et_cart_state)
    EditText mEtState;

    @ViewById(R.id.et_cart_zip)
    EditText mEtZip;

    @ViewById(R.id.et_cart_phoneno)
    EditText mEtPhoneno;

    @ViewById(R.id.et_cart_emaild)
    EditText mEtEmailId;

    @ViewById(R.id.et_cardholdername)
    EditText mEtCardHoldername;

    @ViewById(R.id.et_cardnumber)
    EditText mEtCardNumber;

    @ViewById(R.id.et_cardexpmonth)
    EditText mEtExpMonth;

    @ViewById(R.id.et_cardexpyear)
    EditText mEtExpYear;

    @ViewById(R.id.et_cardcvv)
    EditText mEtCvv;

    @ViewById(R.id.chk_terms)
    CheckBox mChkTerms;

    @ViewById(R.id.tv_cart_terms)
    TextView mTvTermsConditions;


    Bundle arguments;
    ArrayList<CartItems> cartItemsList;
    ShoppingCartList shoppingCartList;
    ArrayList<String> monthList;
    ArrayList<String> yearList;
    ArrayList<StateList> stateList;
    boolean isMonthSelected = false, isYearSelected = false, isStateSelected = false;
    int monthSelectedPos, yearSelectedPos, stateSelectedPos;
    boolean isTermsChecked = false;
    public static final char SPACING_CHAR = '-';
    private static final char space = ' ';

    String sFirstName, sLastName, sStreetAddr1, sStreetAddr2 = "", sCity, sState, sZip, sPhoneno, sEmailID, sCardHoldername, sCardNumber,
            sCardExpMonth, sCardExpYear, sCardCvv, sTaxAmount, sConvFee, sTotalAmount, mInvoiceKey, sStateCode;

    @AfterViews
    void onViewLoaded() {
        mEtCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Remove spacing char
                if (s.length() > 0 && (s.length() % 5) == 0) {
                    final char c = s.charAt(s.length() - 1);
                    if (space == c) {
                        s.delete(s.length() - 1, s.length());
                    }
                }
                // Insert char where needed.


                if (s.length() > 0 && (s.length() % 5) == 0) {
                    char c = s.charAt(s.length() - 1);
                    // Only if its a digit where there should be a space we insert a space
                    if (Character.isDigit(c) && TextUtils.split(s.toString(), String.valueOf(space)).length <= 3) {
                        s.insert(s.length() - 1, String.valueOf(space));
                    }
                }
                sCardNumber = s.toString().replace(" ", "");
            }
        });
        mTvTermsConditions.setPaintFlags(mTvTermsConditions.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvTermsConditions.setText(getActivity().getResources().getString(R.string.cart_termscond));
        arguments = getArguments();
        if (arguments != null) {
            cartItemsList = arguments.getParcelableArrayList("CartItemslist");
            shoppingCartList = arguments.getParcelable("ShoppingCartItem");
            mInvoiceKey = arguments.getString("InvoiceKey");
            sTaxAmount = Config.formatCurrency(shoppingCartList.getTotalAmount());
            sConvFee = Config.formatCurrency(shoppingCartList.getTotalFeeAmount());
            sTotalAmount = Config.formatCurrency(shoppingCartList.getGrandTotalAmount());
            mTvCartAmount.setText(sTaxAmount);
            mTvConvAmount.setText(sConvFee);
            mTvTotalAmount.setText(sTotalAmount);
        }
        loadMonthYear();
        registerForContextMenu(mEtExpMonth);
        registerForContextMenu(mEtExpYear);
        registerForContextMenu(mEtState);
        mEtExpMonth.setOnClickListener(this);
        mEtExpYear.setOnClickListener(this);
        mEtState.setOnClickListener(this);
        mChkTerms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = "I understand my payment of " + "<b>" + sTaxAmount + "</b>" + " will be charged along with a convenience fee of " + "<b>" + sConvFee + "</b>" + " by the service provider  <b>Visual Gov Solutions</b>.<br/><br/> If you agree with the above charges select I Agree";
                DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
                    @Override
                    public void onDialogShow() {

                    }

                    @Override
                    public void onDialogDismissed(String button) {
                        if (button.equals("Positive")) {
                            isTermsChecked = true;
                            mChkTerms.setChecked(true);
                        } else {
                            isTermsChecked = false;
                            mChkTerms.setChecked(false);
                        }
                    }
                });
            }
        });
    }

    @Click(R.id.tv_cart_terms)
    void onTermsConditionsClicked() {
        String message = "I understand my payment of " + "<b>"+sTaxAmount+"</b>" + " will be charged along with a convenience fee of " + "<b>"+sConvFee+"</b>" + " by the service provider  <b>Visual Gov Solutions</b>.<br/><br/> If you agree with the above charges select I Agree";
        DialogView dialogView = new DialogView(getActivity(), "Terms and Conditions", message, "I Agree", "Cancel", new DialogListener() {
            @Override
            public void onDialogShow() {

            }

            @Override
            public void onDialogDismissed(String button) {
                if (button.equals("Positive")) {
                    isTermsChecked = true;
                    mChkTerms.setChecked(true);
                } else {
                    isTermsChecked = false;
                    mChkTerms.setChecked(false);
                }
            }
        });
    }


    @Click(R.id.btn_cartaddr_paynow)
    void onPayNowClicked() {
        sFirstName = mEtFirstName.getText().toString();
        sLastName = mEtLastName.getText().toString();
        sStreetAddr1 = mEtStreetAddress1.getText().toString();
        sStreetAddr2 = mEtStreetAddress2.getText().toString();
        sCity = mEtCity.getText().toString();
        sState = mEtState.getText().toString();
        sZip = mEtZip.getText().toString();
        sPhoneno = mEtPhoneno.getText().toString();
        sEmailID = mEtEmailId.getText().toString();
        sCardHoldername = mEtCardHoldername.getText().toString();
        sCardNumber = mEtCardNumber.getText().toString();
        sCardNumber = sCardNumber.replace(" ", "");
        sCardExpMonth = mEtExpMonth.getText().toString();
        sCardExpYear = mEtExpYear.getText().toString();
        sCardCvv = mEtCvv.getText().toString();

        if (sState.isEmpty() || sCity.isEmpty() || sStreetAddr1.isEmpty() || sZip.isEmpty() || sPhoneno.isEmpty() || sEmailID.isEmpty() ||
                sCardHoldername.isEmpty() || sCardNumber.isEmpty() || sCardExpMonth.isEmpty() || sCardExpYear.isEmpty() || sCardCvv.isEmpty()
                || sFirstName.isEmpty() || sLastName.isEmpty() ) {
            showEmptyAlert("Please enter all the values");
        }else if(!isTermsChecked) {
            showEmptyAlert("Please click on the agreement to proceed with the payment");
        }else {
            final WSLoadingDialog loadingDialog = new WSLoadingDialog(getActivity());
            WSCall wsCall = new WSCall(getActivity());
            List<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
            list.add(new BasicNameValuePair("Address1", sStreetAddr1));
            list.add(new BasicNameValuePair("Address2", sStreetAddr2));
            list.add(new BasicNameValuePair("City", sCity));
            list.add(new BasicNameValuePair("State", sStateCode));
            list.add(new BasicNameValuePair("Zip", sZip));
            list.add(new BasicNameValuePair("FirstName", sFirstName));
            list.add(new BasicNameValuePair("LastName", sLastName));
            list.add(new BasicNameValuePair("PhoneNumber", sPhoneno));
            list.add(new BasicNameValuePair("Email", sEmailID));
            list.add(new BasicNameValuePair("CCNumber", sCardNumber));
            list.add(new BasicNameValuePair("CVV", sCardCvv));
            list.add(new BasicNameValuePair("expYear", sCardExpYear));
            list.add(new BasicNameValuePair("expMonth", sCardExpMonth));
            list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));

            wsCall.payCreditCard(Config.URL_SUBMITCREDITCARD, list, new WSListener() {
                @Override
                public void onRequestSent() {
                    loadingDialog.showWSLoadingDialog();
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    loadingDialog.hideWSLoadingDialog();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONObject json = jsonObject.getJSONObject("FLTax");
                        JSONObject resultjson = json.getJSONObject("PaymentInfo");
                        PaymentInfo data = new PaymentInfo(resultjson);
                        Bundle args = new Bundle();
                        args.putString("email", sEmailID);
                        args.putParcelable("PaymentInfo", data);
                        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.PAYMENT_SUCCESS, args);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });

        }
    }

    BroadcastReceiver mTermsConditionsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                isTermsChecked = intent.getBooleanExtra("TermsConditions", false);
                mChkTerms.setChecked(isTermsChecked);
            }
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_creditcard));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mTermsConditionsReceiver, new IntentFilter("Terms"));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mTermsConditionsReceiver);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        LayoutInflater headerInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);

        if (v.getId() == R.id.et_cardexpmonth) {
            title.setText(getActivity().getResources().getString(R.string.screen_month));
            menu.setHeaderView(header);
            for (int i = 0; i < monthList.size(); i++) {
                menu.add(1, i, Menu.NONE, monthList.get(i));
                menu.setGroupCheckable(1, true, true);
            }
            if (isMonthSelected) {
                MenuItem menuItem = menu.getItem(monthSelectedPos);
                menuItem.setChecked(true);
            }

        }

        if (v.getId() == R.id.et_cardexpyear) {
            title.setText(getActivity().getResources().getString(R.string.screen_year));
            menu.setHeaderView(header);
            for (int i = 0; i < yearList.size(); i++) {
                menu.add(2, i, Menu.NONE, yearList.get(i));
                menu.setGroupCheckable(2, true, true);
            }
            if (isYearSelected) {
                MenuItem menuItem = menu.getItem(yearSelectedPos);
                menuItem.setChecked(true);
            }
        }

        if (v.getId() == R.id.et_cart_state) {
            title.setText(getActivity().getResources().getString(R.string.screen_state));
            menu.setHeaderView(header);
            for (int i = 0; i < stateList.size(); i++) {
                menu.add(3, i, Menu.NONE, stateList.get(i).getStateName());
                menu.setGroupCheckable(3, true, true);
            }
            if (isStateSelected) {
                MenuItem menuItem = menu.getItem(stateSelectedPos);
                menuItem.setChecked(true);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = item.getItemId();
        if (item.getGroupId() == 1) {
            sCardExpMonth = monthList.get(position);
            mEtExpMonth.setText(sCardExpMonth);
            isMonthSelected = true;
            monthSelectedPos = position;
        }

        if (item.getGroupId() == 2) {
            sCardExpYear = yearList.get(position);
            mEtExpYear.setText(sCardExpYear);
            isYearSelected = true;
            yearSelectedPos = position;
        }

        if (item.getGroupId() == 3) {
            sState = stateList.get(position).getStateName();
            mEtState.setText(sState);
            sStateCode = stateList.get(position).getStateCode();
            isStateSelected = true;
            stateSelectedPos = position;
        }
        return super.onContextItemSelected(item);
    }

    private void showEmptyAlert(String message) {
        DialogView dialogView = new DialogView(getActivity(), "Information", message, "OK", null, new DialogListener() {
            @Override
            public void onDialogShow() {

            }

            @Override
            public void onDialogDismissed(String button) {

            }
        });
    }

    private void loadMonthYear() {
        monthList = new ArrayList<>();
        yearList = new ArrayList<>();
        stateList = new ArrayList<>();
        String[] statename = getActivity().getResources().getStringArray(R.array.state_names);
        String[] statecode = getActivity().getResources().getStringArray(R.array.state_codes);
        String data;
        String yeardata;
        for (int i = 1; i <= 12; i++) {
            if (i < 10) {
                data = "0" + i;
                monthList.add(data);
            } else {
                data = "" + i;
                monthList.add(data);
            }
        }

        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        String yearformat = df.format(c.getTime());
        int year = Integer.parseInt(yearformat);

        for (int i = 1; i <= 20; i++) {
            yeardata = "" + year;
            year++;
            yearList.add(yeardata);
        }

        for (int i = 0; i < statename.length; i++) {
            StateList state = new StateList();
            state.setStateCode(statecode[i]);
            state.setStateName(statename[i]);
            stateList.add(state);
        }


    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.et_cardexpmonth) {
            this.getActivity().openContextMenu(v);
        }
        if (v.getId() == R.id.et_cardexpyear) {
            this.getActivity().openContextMenu(v);
        }
        if (v.getId() == R.id.et_cart_state) {
            this.getActivity().openContextMenu(v);
        }
    }

    public class CreditCardTextWatcher implements TextWatcher {

        public static final char SPACING_CHAR = '-'; // Using a Unicode character seems to stuff the logic up.

        @Override
        public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) { }

        @Override
        public void onTextChanged(final CharSequence s, final int start, final int before, final int count) { }

        @Override
        public void afterTextChanged(final Editable s) {
            if (s.length() > 0) {

                // Any changes we make to s in here will cause this method to be run again.  Thus we only make changes where they need to be made,
                // otherwise we'll be in an infinite loop.

                // Delete any spacing characters that are out of place.
                for (int i=s.length()-1; i>=0; --i) {
                    if (s.charAt(i) == SPACING_CHAR  // There is a spacing char at this position ,
                            && (i+1 == s.length()    // And it's either the last digit in the string (bad),
                            || (i+1) % 5 != 0)) {    // Or the position is not meant to contain a spacing char?

                        s.delete(i,i+1);
                    }
                }

                // Insert any spacing characters that are missing.
                for (int i=14; i>=4; i-=5) {
                    if (i < s.length() && s.charAt(i) != SPACING_CHAR) {
                        s.insert(i, String.valueOf(SPACING_CHAR));
                    }
                }
            }
        }
    }
}