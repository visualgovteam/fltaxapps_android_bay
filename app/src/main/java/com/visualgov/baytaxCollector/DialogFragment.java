package com.visualgov.baytaxCollector;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import Adapter.AdValoremAdapter;
import Adapter.DeliquentListAdapter;
import Adapter.DueListAdapter;
import Adapter.InstallmentListAdapter;
import Adapter.LegalListAdapter;
import Adapter.LeviedListAdapter;
import Adapter.NonAdValoremAdapter;
import Adapter.ValuesListAdapter;
import Model.AdValorem;
import Model.CartItemList;
import Model.DelinquentList;
import Model.DueList;
import Model.InstallmentList;
import Model.LeviedList;
import Model.NonAdValorem;
import Model.Values;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 22/08/15.
 */
@EFragment(R.layout.fragment_dialog)
public class DialogFragment extends Fragment implements InstallmentListAdapter.InstallmentClickListener, DeliquentListAdapter.OnDeliquentClickListener {


    @ViewById(R.id.ll_dia_advolarem)
    RelativeLayout ll_advolarem;

    @ViewById(R.id.ll_dia_nonadvolarem)
    RelativeLayout ll_nonadvolarem;

    @ViewById(R.id.ll_advol_totallayout)
    LinearLayout ll_advol_totallayout;

    @ViewById(R.id.ll_nonadvol_totallayout)
    LinearLayout ll_nonadvol_totallayout;

    @ViewById(R.id.ll_noadvol_header)
    LinearLayout ll_nonadvol_header;

    @ViewById(R.id.ll_legallist)
    LinearLayout ll_legallist;

    @ViewById(R.id.ll_installmentLayout)
    LinearLayout ll_installmentList;

    @ViewById(R.id.tv_dia_desc)
    TextView mTvNonDesc;

    @ViewById(R.id.tv_dia_amt)
    TextView mTvNonAmt;

    @ViewById(R.id.tv_ad_auth)
    TextView mTvAdAuth;

    @ViewById(R.id.tv_ad_mileage)
    TextView mTvAdMileage;

    @ViewById(R.id.tv_ad_amt)
    TextView mTvAdAmt;

    @ViewById(R.id.rc_advolaremlist)
    RecyclerView mRcAdvolaremList;

    @ViewById(R.id.rc_nonadvolaremlist)
    RecyclerView mRcNonAdvolaremlist;

    @ViewById(R.id.rc_legallist)
    RecyclerView mRcLegalList;

    @ViewById(R.id.rc_installmentlist)
    RecyclerView mRcInstallmentList;


    @ViewById(R.id.tv_advol_tottaxingAuthority)
    TextView mTvAdVolTotTaxAuth;

    @ViewById(R.id.tv_advol_tottaxamount)
    TextView mTvAdVolTotTaxAmt;

    @ViewById(R.id.tv_nonadvol_tottaxingAuthority)
    TextView mTvNonAdVolTotTaxAuth;

    @ViewById(R.id.tv_nonadvol_tottaxamount)
    TextView mTvNonAdVolTotTaxAmt;

    Bundle arguments;

    int screenType;

    AdValoremAdapter adValoremAdapter;
    NonAdValoremAdapter nonAdValoremAdapter;
    DueListAdapter mDueListAdapter;
    LeviedListAdapter mLeviedListAdapter;
    LegalListAdapter mLegalListAdapter;
    ValuesListAdapter mValuesListAdapter;
    DeliquentListAdapter mDeliquentListAdapter;
    InstallmentListAdapter mInstallmentListAdapter;

    ArrayList<AdValorem> mAdValoremList;
    ArrayList<NonAdValorem> mNonAdValoremList;
    ArrayList<DueList> mDueList;
    ArrayList<LeviedList> mLeviedList;
    ArrayList<String> mLegalList;
    ArrayList<Values> mValuesList;
    ArrayList<Values> mReceiptList;
    ArrayList<DelinquentList> mDelinquentList;
    ArrayList<InstallmentList> mInstallmentList;

    String sScreenTitle, sTotalAdvol, sTotalNonAdvol, sTaxRollType, sTaxYear, sTaxBill, sTaxBillNo, sCurrentDate;

    SimpleDateFormat df;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @AfterViews
    void onViewLoaded() {
        ll_advolarem.setVisibility(View.GONE);
        ll_nonadvolarem.setVisibility(View.GONE);
        ll_legallist.setVisibility(View.GONE);
        ll_advol_totallayout.setVisibility(View.GONE);
        ll_nonadvol_totallayout.setVisibility(View.GONE);
        ll_installmentList.setVisibility(View.GONE);
        arguments = getArguments();
        if (arguments != null) {
            screenType = arguments.getInt("ScreenType");
            sScreenTitle = arguments.getString("ScreenTitle");
            sTaxBill = arguments.getString("TaxBill");
            sTaxRollType = arguments.getString("TaxRollType");
            sTaxYear = arguments.getString("TaxYear");
            SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(sScreenTitle);

        }
        setScreenDetails(screenType);
        Config.isPayNowClicked = false;

    }

    private void setScreenDetails(int screenType) {
        switch (screenType) {
            case 1:
                mAdValoremList = new ArrayList<>();
                mAdValoremList = arguments.getParcelableArrayList("AdVolValue");
                sTotalAdvol = arguments.getString("AdVolTotal");
                ll_advolarem.setVisibility(View.VISIBLE);
                ll_advol_totallayout.setVisibility(View.VISIBLE);
                mTvAdAuth.setText(getActivity().getResources().getString(R.string.diadet_taxingauthority));
                mTvAdMileage.setText(getActivity().getResources().getString(R.string.diadet_mileagerate));
                mTvAdAmt.setText(getActivity().getResources().getString(R.string.diadet_taxamt));
                mRcAdvolaremList.setLayoutManager(new LinearLayoutManager(getActivity()));
                adValoremAdapter = new AdValoremAdapter(getActivity(), mAdValoremList);
                mRcAdvolaremList.setAdapter(adValoremAdapter);
                mTvAdVolTotTaxAmt.setText(sTotalAdvol);
                mTvAdVolTotTaxAuth.setText("TOTAL AD-VALOREM");
                break;

            case 2:
                mNonAdValoremList = new ArrayList<>();
                mNonAdValoremList = arguments.getParcelableArrayList("NonAdVolValue");
                sTotalNonAdvol = arguments.getString("NonAdVolTotal");
                ll_nonadvolarem.setVisibility(View.VISIBLE);
                ll_nonadvol_totallayout.setVisibility(View.VISIBLE);
                mTvNonDesc.setText(getActivity().getResources().getString(R.string.diadet_taxingauthority));
                mTvNonAmt.setText(getActivity().getResources().getString(R.string.diadet_taxamt));
                mRcNonAdvolaremlist.setLayoutManager(new LinearLayoutManager(getActivity()));
                nonAdValoremAdapter = new NonAdValoremAdapter(getActivity(), mNonAdValoremList);
                mRcNonAdvolaremlist.setAdapter(nonAdValoremAdapter);
                mTvNonAdVolTotTaxAuth.setText("TOTAL NON AD-VALOREM");
                mTvNonAdVolTotTaxAmt.setText(sTotalNonAdvol);
                break;

            case 3:
                mDueList = new ArrayList<>();
                mDueList = arguments.getParcelableArrayList("DueList");
                ll_nonadvolarem.setVisibility(View.VISIBLE);
                mTvNonDesc.setText(getActivity().getResources().getString(R.string.diadet_duedate));
                mTvNonAmt.setText(getActivity().getResources().getString(R.string.diadet_dueamt));
                mRcNonAdvolaremlist.setLayoutManager(new LinearLayoutManager(getActivity()));
                mDueListAdapter = new DueListAdapter(getActivity(), mDueList);
                mRcNonAdvolaremlist.setAdapter(mDueListAdapter);
                break;

            case 4:
                mLeviedList = new ArrayList<>();
                mLeviedList = arguments.getParcelableArrayList("LeviedList");
                ll_advolarem.setVisibility(View.VISIBLE);
                ll_advol_totallayout.setVisibility(View.GONE);
                mTvAdAuth.setText(getActivity().getResources().getString(R.string.diadet_levdesc));
                mTvAdMileage.setText(getActivity().getResources().getString(R.string.diadet_levadj));
                mTvAdAmt.setText(getActivity().getResources().getString(R.string.diadet_levOriginal));
                mRcAdvolaremList.setLayoutManager(new LinearLayoutManager(getActivity()));
                mLeviedListAdapter = new LeviedListAdapter(getActivity(), mLeviedList);
                mRcAdvolaremList.setAdapter(mLeviedListAdapter);
                break;

            case 5:
                mLegalList = new ArrayList<>();
                mLegalList = arguments.getStringArrayList("LegalList");
                ll_legallist.setVisibility(View.VISIBLE);
                mRcLegalList.setLayoutManager(new LinearLayoutManager(getActivity()));
                mLegalListAdapter = new LegalListAdapter(getActivity(), mLegalList);
                mRcLegalList.setAdapter(mLegalListAdapter);
                break;

            case 6:
                mValuesList = new ArrayList<>();
                mValuesList = arguments.getParcelableArrayList("ValuesList");
                ll_nonadvolarem.setVisibility(View.VISIBLE);
                ll_nonadvol_header.setVisibility(View.GONE);
                mRcNonAdvolaremlist.setLayoutManager(new LinearLayoutManager(getActivity()));
                mValuesListAdapter = new ValuesListAdapter(getActivity(), mValuesList);
                mRcNonAdvolaremlist.setAdapter(mValuesListAdapter);
                break;

            case 7:
                mReceiptList = new ArrayList<>();
                mReceiptList = arguments.getParcelableArrayList("ReceiptList");
                ll_nonadvolarem.setVisibility(View.VISIBLE);
                ll_nonadvol_header.setVisibility(View.GONE);
                mRcNonAdvolaremlist.setLayoutManager(new LinearLayoutManager(getActivity()));
                mValuesListAdapter = new ValuesListAdapter(getActivity(), mReceiptList);
                mRcNonAdvolaremlist.setAdapter(mValuesListAdapter);
                break;

            case 8:
                mDelinquentList = new ArrayList<>();
                mDelinquentList = arguments.getParcelableArrayList("DeliqList");
                sTotalNonAdvol = arguments.getString("DeliqLisTotal");
                ll_nonadvolarem.setVisibility(View.VISIBLE);
                ll_nonadvol_header.setVisibility(View.GONE);
                mRcNonAdvolaremlist.setLayoutManager(new LinearLayoutManager(getActivity()));
                mDeliquentListAdapter = new DeliquentListAdapter(getActivity(), mDelinquentList);
                mRcNonAdvolaremlist.setAdapter(mDeliquentListAdapter);
                mTvNonAdVolTotTaxAuth.setText("TOTAL DUE");
                mTvNonAdVolTotTaxAmt.setText(sTotalNonAdvol);
                break;

            case 9:
                ll_installmentList.setVisibility(View.VISIBLE);
                mInstallmentList = new ArrayList<>();
                mInstallmentList = arguments.getParcelableArrayList("InstallList");
                mRcInstallmentList.setLayoutManager(new LinearLayoutManager(getActivity()));
                mInstallmentListAdapter = new InstallmentListAdapter(getActivity(), mInstallmentList);
                mInstallmentListAdapter.setInstallmentClickListener(this);
                mRcInstallmentList.setAdapter(mInstallmentListAdapter);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        SearchFragmentActivity.defaultInstance().getSupportActionBar().setHomeButtonEnabled(false);
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.VISIBLE);
        SearchFragmentActivity.defaultInstance().ll_btn_close.setVisibility(View.VISIBLE);

    }

    @Override
    public void onPause() {
        super.onPause();
        SearchFragmentActivity.defaultInstance().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SearchFragmentActivity.defaultInstance().getSupportActionBar().setHomeButtonEnabled(true);
        SearchFragmentActivity.defaultInstance().ll_btn_close.setVisibility(View.GONE);
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_taxbill));
    }

    @Override
    public void onInstallmentClicked(View view, int position) {


        sTaxBillNo = sTaxBill + "-" + mInstallmentList.get(position).getInstallmentNumber();
        int maxCart = 0, minInstal = 0, maxInstal = 0;
        ArrayList<Integer> instalpayno = new ArrayList<>();
        ArrayList<Integer> cartInstalpayno = new ArrayList<>();
        for (int i = 0; i < mInstallmentList.size(); i++) {
            InstallmentList data = mInstallmentList.get(i);
            if (data.getReceiptNo() == null || data.getReceiptNo().equals("null")) {
                int installmentno = Integer.parseInt(data.getInstallmentNumber());
                instalpayno.add(installmentno);
            }

        }

        int year = Integer.parseInt(sTaxYear);
        year = year + 1;
        String checkDate = "08/01/" + year;
        Log.i("CHECK DATE", checkDate);
        getCurrentDateAndTime();

        try {
            Date maxDate = df.parse(checkDate);
            Date currentDate = df.parse(sCurrentDate);
            if (instalpayno.contains(5)) {
                if (currentDate.compareTo(maxDate) < 0) {
                    for (int i = 0; i < Config.cartItems.size(); i++) {
                        if (Config.cartItems.get(i).getTaxBillNumber().contains("-")) {
                            String checkItem = Config.cartItems.get(i).getTaxBillNumber();
                            String[] checkvalue = checkItem.split("\\-");
                            String checkTaxBillno = checkvalue[0];
                            String checkInstalmentno = checkvalue[1];
                            if (checkTaxBillno.equals(sTaxBill)) {
                                int checkInstalno = Integer.parseInt(checkInstalmentno);
                                cartInstalpayno.add(checkInstalno);
                            }
                        }
                    }
                    if (cartInstalpayno != null && cartInstalpayno.size() > 0) {
                        maxCart = Collections.max(cartInstalpayno);
                    }
                    if (instalpayno != null && instalpayno.size() > 0) {
                        minInstal = Collections.min(instalpayno);
                        maxInstal = Collections.max(instalpayno);
                    }
                    int currentInstalno = Integer.parseInt(mInstallmentList.get(position).getInstallmentNumber());

                    if (maxCart != 0 && currentInstalno > maxCart) {
                        addInstalmentToCart(sTaxBillNo, mInstallmentList.get(position).getInstallAmt());
                    } else if (currentInstalno < maxInstal) {
                        addInstalmentToCart(sTaxBillNo, mInstallmentList.get(position).getInstallAmt());
                    } else {
                        Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_addlowerinstal) , Toast.LENGTH_SHORT).show();
                    }
                } else {
                    DialogView dialogView = new DialogView(getActivity(), "ALERT", getActivity().getResources().getString(R.string.alert_contactoffice), "OK", null, new DialogListener() {
                        @Override
                        public void onDialogShow() {

                        }

                        @Override
                        public void onDialogDismissed(String button) {

                        }
                    });
                }
            }else{
                for (int i = 0; i < Config.cartItems.size(); i++) {
                    if (Config.cartItems.get(i).getTaxBillNumber().contains("-")) {
                        String checkItem = Config.cartItems.get(i).getTaxBillNumber();
                        String[] checkvalue = checkItem.split("\\-");
                        String checkTaxBillno = checkvalue[0];
                        String checkInstalmentno = checkvalue[1];
                        if (checkTaxBillno.equals(sTaxBill)) {
                            int checkInstalno = Integer.parseInt(checkInstalmentno);
                            cartInstalpayno.add(checkInstalno);
                        }
                    }
                }
                if (cartInstalpayno != null && cartInstalpayno.size() > 0) {
                    maxCart = Collections.max(cartInstalpayno);
                }
                if (instalpayno != null && instalpayno.size() > 0) {
                    minInstal = Collections.min(instalpayno);
                    maxInstal = Collections.max(instalpayno);
                }
                int currentInstalno = Integer.parseInt(mInstallmentList.get(position).getInstallmentNumber());

                if (maxCart != 0 && currentInstalno > maxCart) {
                    addInstalmentToCart(sTaxBillNo, mInstallmentList.get(position).getInstallAmt());
                } else if (instalpayno.size() > 1 && currentInstalno < maxInstal) {
                    addInstalmentToCart(sTaxBillNo, mInstallmentList.get(position).getInstallAmt());
                } else if (instalpayno.size() <=1) {
                    addInstalmentToCart(sTaxBillNo, mInstallmentList.get(position).getInstallAmt());
                } else {
                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.alert_addlowerinstal), Toast.LENGTH_SHORT).show();
                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public void showToast() {

        int Y = SearchFragmentActivity.defaultInstance().mToolbarHeight;
        int X = SearchFragmentActivity.defaultInstance().mToolbarWidth;
        int yhalf = Y / 4;
        int yoff;
        if (Config.isTablet(getActivity())) {
            yoff = (3 * dpToPx(Y));
        } else {
            yoff = 2 * dpToPx(Y);
        }
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layout = inflater.inflate(R.layout.cell_cartpopup, null);
        TextView tv = (TextView) layout.findViewById(R.id.tv_cartpopmsg);
        tv.setText("Item is added to cart");
        Toast toast = new Toast(getActivity());
        toast.setGravity(Gravity.RIGHT | Gravity.TOP, X, yhalf);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    @Override
    public void onDeliquentListClicked(View view, int position) {
        sTaxBillNo = mDelinquentList.get(position).getTaxBill();
        WSCall wsCall = new WSCall(getActivity());
        final WSLoadingDialog loadingDialog = new WSLoadingDialog(getActivity());
        List<NameValuePair> list = new ArrayList<>();
        if (!Config.InvoiceKey.equals("")) {
            list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        }
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("RollType", sTaxRollType));
        list.add(new BasicNameValuePair("TaxBillNo", sTaxBillNo));
        list.add(new BasicNameValuePair("TaxYear", sTaxYear));
        list.add(new BasicNameValuePair("TaxAmount", mDelinquentList.get(position).getTaxTotal()));

        wsCall.setAddToCart(Config.URL_ADDTOCART, list, new WSListener() {
            @Override
            public void onRequestSent() {
                loadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                loadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject json = result.getJSONObject("FLTax");
                        CartItemList cartItemList = new CartItemList(json);
                        Config.cartItems = cartItemList.getCartItemsList();
                        Config.shoppingCartItems = cartItemList.getShoppingCartList();
                        Config.InvoiceKey = cartItemList.getShoppingCartList().getInvoiceKey();
                        Intent cartIntent = new Intent("CartNotif");
                        LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                        showToast();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void addInstalmentToCart(String taxbillno, String taxamount) {
        WSCall wsCall = new WSCall(getActivity());
        final WSLoadingDialog loadingDialog = new WSLoadingDialog(getActivity());
        List<NameValuePair> list = new ArrayList<>();
        if (!Config.InvoiceKey.equals("")) {
            list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        }
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("RollType", sTaxRollType));
        list.add(new BasicNameValuePair("TaxBillNo", taxbillno));
        list.add(new BasicNameValuePair("TaxYear", sTaxYear));
        list.add(new BasicNameValuePair("TaxAmount", taxamount));

        wsCall.setAddToCart(Config.URL_ADDTOCART, list, new WSListener() {
            @Override
            public void onRequestSent() {
                loadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                loadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject json = result.getJSONObject("FLTax");
                        CartItemList cartItemList = new CartItemList(json);
                        cartItemList.setHasInstallment(true);
                        Config.installmentCount++;
                        Config.cartItems = cartItemList.getCartItemsList();
                        Config.shoppingCartItems = cartItemList.getShoppingCartList();
                        Config.InvoiceKey = cartItemList.getShoppingCartList().getInvoiceKey();
                        Intent cartIntent = new Intent("CartNotif");
                        LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                        showToast();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @SuppressLint("SimpleDateFormat")
    private void getCurrentDateAndTime() {
        Calendar c = Calendar.getInstance();
        df = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat pf = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFromat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat timeFromatParam = new SimpleDateFormat("HH:mm");
        sCurrentDate = df.format(c.getTime());
    }
}
