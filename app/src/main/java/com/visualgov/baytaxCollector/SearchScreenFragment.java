package com.visualgov.baytaxCollector;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Model.TaxSearchInfo;
import utils.Config;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 17/08/15.
 */
@EFragment(R.layout.fragment_searchscreen)
public class SearchScreenFragment extends Fragment implements View.OnClickListener {

    @ViewById(R.id.et_srch_prptyname)
    EditText etSearchPropertyName;

    @ViewById(R.id.et_srch_streetno)
    EditText etSearchStreetNo;

    @ViewById(R.id.et_srch_streetname)
    EditText etSearchStreetName;

    @ViewById(R.id.et_srch_prptyno)
    EditText etPropertyNumber;

    @ViewById(R.id.et_srch_taxbillno)
    EditText etTaxBillNo;

    @ViewById(R.id.et_srch_rolltype)
    EditText etRollType;

    @ViewById(R.id.et_srch_taxyear)
    EditText etTaxYear;

    String propertyName = "", streetno = "", streetname = "", prptyno = "", taxbillno = "", rolltype = "", taxyear = "", rollTypeDesc = "";

    boolean isRollTypeSelected = false, isTaxyearSelected = false;
    int rollTypeSelectedPos, taxyearSelectedPos;
    WSLoadingDialog wsLoadingDialog;
    List<NameValuePair> paramsList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @AfterViews
    void onViewLoaded() {
        registerForContextMenu(etRollType);
        registerForContextMenu(etTaxYear);
        etRollType.setOnClickListener(this);
        etTaxYear.setOnClickListener(this);
        wsLoadingDialog = new WSLoadingDialog(getActivity());
    }


    @Click(R.id.btn_srchdetails)
    void onClick() {
        propertyName = etSearchPropertyName.getText().toString();
        streetno = etSearchStreetNo.getText().toString();
        streetname = etSearchStreetName.getText().toString();
        prptyno = etPropertyNumber.getText().toString();
        taxbillno = etTaxBillNo.getText().toString();

        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("RollType", rolltype));
        list.add(new BasicNameValuePair("OwnerName", propertyName));
        list.add(new BasicNameValuePair("ParcelNumber", prptyno));
        list.add(new BasicNameValuePair("StreetNo", streetno));
        list.add(new BasicNameValuePair("StreetName", streetname));
        list.add(new BasicNameValuePair("TaxBill", taxbillno));
        list.add(new BasicNameValuePair("TaxYear", taxyear));
        list.add(new BasicNameValuePair("Skip", "0"));
        list.add(new BasicNameValuePair("Rows", "50"));


        WSCall wsCall = new WSCall(getActivity());
        wsCall.getTaxSearchInfo(Config.URL_TAXSEARCH, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
                Log.i("SEARCH RESULT WS", "Request Sent");
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                Log.i("SEARCH RESULT WS", "Response Received");
                wsLoadingDialog.hideWSLoadingDialog();

                if (exception != null) {
                    exception.printStackTrace();
                }


                if (response != null) {
                    try {
                        JSONObject jObject = new JSONObject(response);
                        JSONObject json = jObject.getJSONObject("FLTax");
                        TaxSearchInfo result = new TaxSearchInfo(json);
                        Bundle args = new Bundle();
                        args.putString("RollType", rolltype);
                        args.putString("OwnerName", propertyName);
                        args.putString("ParcelNumber", prptyno);
                        args.putString("StreetNo", streetno);
                        args.putString("StreetName", streetname);
                        args.putString("TaxBill", taxbillno);
                        args.putString("TaxYear", taxyear);
                        args.putParcelable("SearchResult", result);
                        SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.SEARCH_RESULT, args);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_searchbydetails));
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        LayoutInflater headerInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);

        // menu.setHeaderView(header);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);


        if (v.getId() == R.id.et_srch_rolltype) {
            title.setText(getActivity().getResources().getString(R.string.screen_rollType));
            menu.setHeaderView(header);
            for (int i = 0; i < Config.rollTypeList.size(); i++) {
                menu.add(1, i, Menu.NONE, Config.rollTypeList.get(i).getTaxRollDescription());
                menu.setGroupCheckable(1, true, true);
            }
            if(isRollTypeSelected){
                MenuItem menuItem = menu.getItem(rollTypeSelectedPos);
                menuItem.setChecked(true);
            }

        }

        if (v.getId() == R.id.et_srch_taxyear){
            title.setText(getActivity().getResources().getString(R.string.screen_taxyear));
            menu.setHeaderView(header);
            for (int i = 0; i < Config.yearList.size(); i++) {
                menu.add(2, i, Menu.NONE, Config.yearList.get(i).getTaxYear());
                menu.setGroupCheckable(2, true, true);
            }
            if(isTaxyearSelected){
                MenuItem menuItem = menu.getItem(taxyearSelectedPos);
                menuItem.setChecked(true);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int position = item.getItemId();
        if (item.getGroupId() == 1) {
            rolltype = Config.rollTypeList.get(position).getTaxRollCharacter();
            rollTypeDesc = Config.rollTypeList.get(position).getTaxRollDescription();
            etRollType.setText(rollTypeDesc);
            isRollTypeSelected = true;
            rollTypeSelectedPos = position;
        }

        if(item.getGroupId() == 2){
            taxyear = Config.yearList.get(position).getTaxYear();
            etTaxYear.setText(taxyear);
            isTaxyearSelected = true;
            taxyearSelectedPos = position;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.et_srch_rolltype) {
            this.getActivity().openContextMenu(v);
        }
        if(v.getId() == R.id.et_srch_taxyear){
            this.getActivity().openContextMenu(v);
        }
    }
}
