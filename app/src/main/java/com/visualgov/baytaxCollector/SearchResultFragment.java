package com.visualgov.baytaxCollector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Adapter.SearchResultAdapter;
import Model.ResultsList;
import Model.TaxDetailInfo;
import Model.TaxSearchInfo;
import utils.Config;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 19/08/15.
 */
@EFragment(R.layout.fragment_searchresult)
public class SearchResultFragment extends Fragment implements SearchResultAdapter.OnSearchResultClickListener {

    @ViewById(R.id.rc_searchresult)
    RecyclerView mRCSearchResult;

    @ViewById(R.id.tv_srchrsult_norecordsfound)
    TextView mTvNoResultsFound;

    Bundle arguments;
    TaxSearchInfo searchResultInfo;

    SearchResultAdapter searchResultAdapter;

    WSLoadingDialog wsLoadingDialog;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;

    LinearLayoutManager mLayoutManager;
    List<NameValuePair> list;
    int currentCount, totalCount, skipcount;
    ArrayList<ResultsList> mResultsList;

    String rolltype,propertyName, prptyno, streetno, streetname, taxbillno, taxyear;


    @AfterViews
    void onViewLoaded(){
        mResultsList = new ArrayList<>();
        wsLoadingDialog = new WSLoadingDialog(getActivity());
        arguments = getArguments();
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRCSearchResult.setLayoutManager(mLayoutManager);
        if(arguments != null) {
            searchResultInfo = arguments.getParcelable("SearchResult");
            currentCount = Integer.parseInt(searchResultInfo.getResultsInfo().getRows());
            totalCount = Integer.parseInt(searchResultInfo.getResultsInfo().getTotalRowCount());
            rolltype = arguments.getString("RollType");
            propertyName= arguments.getString("OwnerName");
            prptyno = arguments.getString("ParcelNumber");
            streetno = arguments.getString("StreetNo");
            streetname = arguments.getString("StreetName");
            taxbillno = arguments.getString("TaxBill");
            taxyear = arguments.getString("TaxYear");
        }
        if(searchResultInfo != null && searchResultInfo.getResultsLists().size() >0){
            mTvNoResultsFound.setVisibility(View.GONE);
            mRCSearchResult.setVisibility(View.VISIBLE);
            mResultsList = searchResultInfo.getResultsLists();
            searchResultAdapter = new SearchResultAdapter(getActivity(), mResultsList);
            searchResultAdapter.setOnSearchResultClickListener(this);
            mRCSearchResult.setAdapter(searchResultAdapter);
        }else{
            mTvNoResultsFound.setVisibility(View.VISIBLE);
            mRCSearchResult.setVisibility(View.GONE);
        }

        mRCSearchResult.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                visibleItemCount = mLayoutManager.getChildCount();
                totalItemCount = mLayoutManager.getItemCount();
                pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

                if (loading) {
                    if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        loading = false;
                        skipcount++;
                        onLoadMoreResult();

                    }
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_propertydetails));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void OnSearchResultClicked(View v, int position) {
        ResultsList searchDetail = searchResultInfo.getResultsLists().get(position);
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
        list.add(new BasicNameValuePair("RollType", searchDetail.getROLLTYPE()));
        list.add(new BasicNameValuePair("TaxBillNo", searchDetail.getTAXBILLNO()));
        list.add(new BasicNameValuePair("TaxYear", searchDetail.getTAXYEAR()));
        WSCall wsCall = new WSCall(getActivity());
        wsCall.getTaxDetailInfo(Config.URL_TAXDETAIL, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject json = jsonObject.getJSONObject("FLTax");
                    TaxDetailInfo detailInfo = new TaxDetailInfo(json);
                    Bundle args = new Bundle();
                    args.putParcelable("TaxDetail", detailInfo);
                    SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.TAX_DETAIL, args);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void onLoadMoreResult(){
        int skip = skipcount * 50;
        Log.i("Count", "Current Count:"+currentCount+"Total Count:"+totalCount);
        if(currentCount < totalCount ) {
            List<NameValuePair> list = new ArrayList<>();
            list.add(new BasicNameValuePair("VGProdID", Config.VGPRODID));
            list.add(new BasicNameValuePair("RollType", rolltype));
            list.add(new BasicNameValuePair("OwnerName", propertyName));
            list.add(new BasicNameValuePair("ParcelNumber", prptyno));
            list.add(new BasicNameValuePair("StreetNo", streetno));
            list.add(new BasicNameValuePair("StreetName", streetname));
            list.add(new BasicNameValuePair("TaxBill", taxbillno));
            list.add(new BasicNameValuePair("TaxYear", taxyear));
            list.add(new BasicNameValuePair("Skip", ""+skip));
            list.add(new BasicNameValuePair("Rows", "50"));
            WSCall wsCall = new WSCall(getActivity());
            wsCall.getTaxSearchInfo(Config.URL_TAXSEARCH, list, new WSListener() {
                @Override
                public void onRequestSent() {
                    wsLoadingDialog.showWSLoadingDialog();
                    Log.i("SEARCH RESULT WS", "Request Sent");
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    Log.i("SEARCH RESULT WS", "Response Received");

                    if (exception != null) {
                        exception.printStackTrace();
                    }


                    if (response != null) {
                        try {

                            JSONObject jObject = new JSONObject(response);
                            JSONObject json = jObject.getJSONObject("FLTax");
                            TaxSearchInfo result = new TaxSearchInfo(json);

                            ArrayList<ResultsList> data = new ArrayList<ResultsList>();
                            data =  result.getResultsLists();
                            for(int i=0; i<data.size(); i++){
                                ResultsList currentObject = data.get(i);
                                mResultsList.add(currentObject);
                            }
                            searchResultAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    wsLoadingDialog.hideWSLoadingDialog();
                    loading = true;

                }
            });
        }
    }


}
