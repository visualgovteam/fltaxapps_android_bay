package com.visualgov.baytaxCollector;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import Adapter.CartListAdapter;
import Model.AdminSettingsList;
import Model.CartItemList;
import Model.CartItems;
import Model.ShoppingCartList;
import utils.Config;
import utils.DialogListener;
import utils.DialogView;
import utils.WSCall;
import utils.WSListener;
import utils.WSLoadingDialog;

/**
 * Created by SaraschandraaM on 27/08/15.
 */
@EFragment(R.layout.fragment_cartinfo)
public class CartInfoFragment extends Fragment implements CartListAdapter.OnCheckedChangeListener {

    @ViewById(R.id.rc_cartdetails)
    RecyclerView mRCCardDetails;

    @ViewById(R.id.tv_cart_totamt)
    TextView mTvCartTotalAmt;

    CartListAdapter cartListAdapter;

    Bundle arguments;

    ArrayList<CartItems> cartItemsList;
    ShoppingCartList shoppingCartList;
    String mInvoiceKey, sSiteMessage, sSiteEnabled;
    boolean isSiteEnabled;
    boolean isPayment;

    List<NameValuePair> list;
    WSLoadingDialog wsLoadingDialog;

    @AfterViews
    void onViewLoaded() {
        mRCCardDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        arguments = getArguments();
        if (!Config.isPayNowClicked) {
            cartItemsList = arguments.getParcelableArrayList("CartItemslist");
            shoppingCartList = arguments.getParcelable("ShoppingCartItem");
            mInvoiceKey = arguments.getString("InvoiceKey");
        }
        setDetails();
    }


    @Click(R.id.btn_cart_paynow)
    void onPayClicked() {
        checkPaymentAllowed();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_cartlist));
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.VISIBLE);
        SearchFragmentActivity.defaultInstance().ll_search.setVisibility(View.GONE);
        setDetails();
    }

    @Override
    public void onPause() {
        super.onPause();
        SearchFragmentActivity.defaultInstance().ll_cart_btn.setVisibility(View.VISIBLE);
        SearchFragmentActivity.defaultInstance().ll_search.setVisibility(View.GONE);
        SearchFragmentActivity.defaultInstance().mToolbarTitle.setText(getActivity().getResources().getString(R.string.screen_taxbill));
    }

    @Override
    public void onCheckedChange(View view, int position) {
        CartItems data = Config.cartItems.get(position);

        String ItemId = data.getItemID();
        final ArrayList<String> instalmentIds = new ArrayList<>();
        if (data.getTaxBillNumber().contains("-")) {
            String[] value = data.getTaxBillNumber().split("\\-");
            String currentInstalmentno = value[1];
            String currentTaxBillno = value[0];
            int currentInstno = Integer.parseInt(currentInstalmentno);
            for (int i = 0; i < Config.cartItems.size(); i++) {
                if (Config.cartItems.get(i).getTaxBillNumber().contains("-")) {
                    String checkItem = Config.cartItems.get(i).getTaxBillNumber();
                    String[] checkvalue = checkItem.split("\\-");
                    String checkInstalmentno = checkvalue[1];
                    String checkTaxBillno = checkvalue[0];
                    if (checkTaxBillno.equals(currentTaxBillno)) {
                        int checkInstalno = Integer.parseInt(checkInstalmentno);
                        if (checkInstalno > currentInstno) {
                            String itemId = Config.cartItems.get(i).getItemID();
                            instalmentIds.add(itemId);
                        }
                    }
                }
            }
        }
        final WSLoadingDialog wsLoadingDialog = new WSLoadingDialog(getActivity());
        WSCall wsCall = new WSCall(getActivity());
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        list.add(new BasicNameValuePair("ItemID", ItemId));
        wsCall.removeItemFromCart(Config.URL_REMOVEFROMCART, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject json = result.getJSONObject("FLTax");
                        CartItemList cartItems = new CartItemList(json);
                        Config.cartItems = cartItems.getCartItemsList();
                        cartItemsList = cartItems.getCartItemsList();
                        Config.shoppingCartItems = cartItems.getShoppingCartList();
                        shoppingCartList = cartItems.getShoppingCartList();
                        Config.InvoiceKey = cartItems.getShoppingCartList().getInvoiceKey();
                        Intent cartIntent = new Intent("CartNotif");
                        LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                        setDetails();
                        if (instalmentIds != null && instalmentIds.size() > 0) {
                            for (int i = 0; i < instalmentIds.size(); i++) {
                                String itemid = instalmentIds.get(i);
                                deleteInstalmentCartItems(itemid);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    private void setDetails() {
        if (cartItemsList != null) {
            cartListAdapter = new CartListAdapter(getActivity(), cartItemsList);
            cartListAdapter.setOnCheckedChangeListener(CartInfoFragment.this);
            mRCCardDetails.setAdapter(cartListAdapter);
            String amount = shoppingCartList.getGrandTotalAmount();
            amount = Config.formatCurrency(amount);
            mTvCartTotalAmt.setText(amount);
        }
    }

    private void deleteInstalmentCartItems(String itemId) {
        final WSLoadingDialog wsLoadingDialog = new WSLoadingDialog(getActivity());
        WSCall wsCall = new WSCall(getActivity());
        List<NameValuePair> list = new ArrayList<>();
        list.add(new BasicNameValuePair("InvoiceKey", Config.InvoiceKey));
        list.add(new BasicNameValuePair("ItemID", itemId));
        wsCall.removeItemFromCart(Config.URL_REMOVEFROMCART, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject json = result.getJSONObject("FLTax");
                        CartItemList cartItems = new CartItemList(json);
                        Config.cartItems = cartItems.getCartItemsList();
                        cartItemsList = cartItems.getCartItemsList();
                        Config.shoppingCartItems = cartItems.getShoppingCartList();
                        shoppingCartList = cartItems.getShoppingCartList();
                        Config.InvoiceKey = cartItems.getShoppingCartList().getInvoiceKey();
                        Intent cartIntent = new Intent("CartNotif");
                        LocalBroadcastManager.getInstance(SearchFragmentActivity.defaultInstance()).sendBroadcast(cartIntent);
                        setDetails();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }


    private void checkPaymentAllowed() {
        list = new ArrayList<>();
        list.add(new BasicNameValuePair("VGProdId", Config.VGPRODID));
        WSCall wsCall = new WSCall(getActivity());
        wsLoadingDialog = new WSLoadingDialog(getActivity());

        wsCall.getAdminSettingsInfo(Config.URL_GETADMINSETTING, list, new WSListener() {
            @Override
            public void onRequestSent() {
                wsLoadingDialog.showWSLoadingDialog();
            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                wsLoadingDialog.hideWSLoadingDialog();
                if (response != null) {
                    try {
                        JSONObject jObject = new JSONObject(response);
                        JSONObject json = jObject.getJSONObject("FLTaxSettings");
                        JSONObject jsonObject = json.getJSONObject("AdminSettingsList");
                        Config.adminSettingsList = new AdminSettingsList(jsonObject);
                        if (Config.adminSettingsList.getCreditCardEnabled().equals("True")) {
                            Config.isCreditCardAllowed = true;
                        } else if (Config.adminSettingsList.getCreditCardEnabled().equals("False")) {
                            Config.isCreditCardAllowed = false;
                        } else {
                            Config.isCreditCardAllowed = false;
                        }

                        sSiteMessage = Config.adminSettingsList.getSiteMessage();
                        sSiteEnabled = Config.adminSettingsList.getSiteEnabled();

                        if (sSiteEnabled.equals("False")) {
                            isSiteEnabled = false;
                        } else if (sSiteEnabled.equals("True")) {
                            isSiteEnabled = true;
                        }

                        Config.isSiteEnabled = isSiteEnabled;


                        if (Config.adminSettingsList.getCheckEnabled().equals("True")) {
                            Config.isECheckAllowed = true;
                        } else if (Config.adminSettingsList.getCheckEnabled().equals("False")) {
                            Config.isECheckAllowed = false;
                        } else {
                            Config.isECheckAllowed = false;
                        }

                        if (cartItemsList != null && cartItemsList.size() > 0) {
                            Config.isPayNowClicked = true;
                            Bundle args = new Bundle();
                            args.putString("InvoiceKey", Config.InvoiceKey);
                            args.putParcelable("ShoppingCartItem", shoppingCartList);
                            args.putParcelableArrayList("CartItemslist", cartItemsList);
                            SearchFragmentActivity.defaultInstance().changeFragment(SearchFragmentActivity.Fragments.SELECT_PAYMENT, args);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }


}

